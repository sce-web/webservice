<?php
require_once('Lib/webservice/nusoap.php');

$server = new soap_server;

$server->configureWSDL('server.AvisarPedidoProcessado', 'urn:server.AvisarPedidoProcessado');
$server->wsdl->schemaTargetNamespace = 'urn:server.AvisarPedidoProcessado';

$server->register('AvisarPedidoProcessado',
    array('nropedido' => 'xsd:string'),
    array('return' => 'xsd:string'),
    'urn:server.AvisarPedidoProcessado',
    'urn:server.AvisarPedidoProcessado#AvisarPedidoProcessado',
    'rpc',
    'encoded',
    'Retorna o resultado'
);


function AvisarPedidoProcessado($nropedido)
{
    require_once("AutoLoader.php");
    return Webservice::AvisaPedidoProcessado($nropedido);
    /*
    try {

        $banco = DB_DATABASE;
        $usuario = DB_USERNAME;
        $senha = DB_PASSWORD;
        $hostname = DB_HOSTNAME;
        $conn = mysql_connect($hostname, $usuario, $senha);
        mysql_select_db($banco) or die("Não foi possível conectar ao banco MySQL");
        if (!$conn) {
            throw new Exception("Não foi possível conectar ao banco MySQL. ");
        }

        $pedido = mysql_query("SELECT * FROM `order` WHERE order_id = " . $nropedido) or die("Erro na consulta de pedido");


        if ($ped = mysql_fetch_assoc($pedido)) {
            $email = $ped["email"];
        }


        mysql_query("UPDATE `order` SET order_status_id = 17 WHERE order_id = " . $nropedido);
        $status = mysql_query("INSERT INTO order_history (order_id, order_status_id, notify, comment, date_added) VALUES (" . $nropedido . ", 17,1,'" . $message . "','" . date('Y-m-d H:i:s') . "')");


        if ($status) {
            return "<?xml version='1.0' encoding='utf-16'?>
					<WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
					  <Status>true</Status>
					  <Retorno>ok</Retorno>
					  <Erro></Erro>
					</WsParam>";
        } else {
            throw new Exception(mysql_error());
        }
    } catch (Exception $e) {
        return "<?xml version='1.0' encoding='utf-16'?>
			<WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
			  <Status>false</Status>
			  <Retorno></Retorno>
			  <Erro>" . $e->getMessage() . "</Erro>
			</WsParam>";

    }
    */
}


$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);

?>
