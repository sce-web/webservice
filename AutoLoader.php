<?php
ini_set("display_errors", true);
error_reporting(E_ALL ^ E_NOTICE);

/**
 * Class AutoLoader
 */
class AutoLoader
{
    static private $classNames = array();

    public static function registerDirectory($dirName)
    {
        $di = new DirectoryIterator($dirName);
        foreach ($di as $file) {
            if ($file->isDir() && !$file->isLink() && !$file->isDot()) {
                self::registerDirectory($file->getPathname());
            } elseif (substr($file->getFilename(), -4) === '.php') {
                $className = substr($file->getFilename(), 0, -4);
                AutoLoader::registerClass($className, $file->getPathname());
            }
        }
    }

    public static function registerClass($className, $fileName)
    {
        AutoLoader::$classNames[$className] = $fileName;
    }

    public static function loadClass($className)
    {
        if (isset(AutoLoader::$classNames[$className])) {
            require_once(AutoLoader::$classNames[$className]);
        }
    }
}

/**
 * Configurations
 */

spl_autoload_register(array("AutoLoader", "loadClass"));

//directory registrations
define("DEFAULT_OPENCART_CONFIG", "../../config.php");


if (file_exists(DEFAULT_OPENCART_CONFIG)) {
    require_once(DEFAULT_OPENCART_CONFIG);
} else {
    require_once("LocalConfig.php");
}

define("WEBSERVICE_ROOT", DIR_ROOT.DIRECTORY_SEPARATOR."sceweb".DIRECTORY_SEPARATOR."webservice".DIRECTORY_SEPARATOR);

require_once("Model/library.php");

AutoLoader::registerDirectory("Model/categoria");
AutoLoader::registerDirectory("Model/pedido");
AutoLoader::registerDirectory("Model/produto");
AutoLoader::registerDirectory("Model");
AutoLoader::registerDirectory("Connection");
AutoLoader::registerDirectory("Lib");

/**
 * Default configurations
 */

Logging::lfile(WEBSERVICE_ROOT . "error.log");
