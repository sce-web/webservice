<?php
require_once('Lib/webservice/nusoap.php');

$server = new soap_server();
$server->configureWSDL('server.GetSession', 'urn:server.GetSession');
$server->wsdl->schemaTargetNamespace = 'urn:server.GetSession';
$server->register('GetSession',
    array(),
    array('return' => 'xsd:string'),
    'urn:server.GetSession',
    'urn:server.GetSession#GetSession',
    'rpc',
    'encoded',
    'Retorna o resultado'
);


function GetSession()
{
    require_once("AutoLoader.php");
    return Webservice::GetSession();
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
