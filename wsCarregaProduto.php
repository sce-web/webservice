<?php
require_once('Lib/webservice/nusoap.php');

$server = new soap_server();
$server->configureWSDL('server.CarregaProduto', 'urn:server.CarregaProduto');
$server->wsdl->schemaTargetNamespace = 'urn:server.CarregaProduto';
$server->register(
    'CarregaProduto',
    array(
        'token' => 'xsd:string',
        'versao' => 'xsd:string',
        'msg' => 'xsd:string',
        'sincronizar' => 'xsd:string'
    ),
    array('return' => 'xsd:string'),
    'urn:server.CarregaProduto',
    'urn:server.CarregaProduto#CarregaProduto',
    'rpc',
    'encoded',
    'Retorna o resultado'
);

$server->register(
    'CarregaProdutoV2',
    array(
        'token' => 'xsd:string',
        'versao' => 'xsd:string',
        'msg' => 'xsd:string',
        'sincronizar' => 'xsd:string'
    ),
    array('return' => 'xsd:string'),
    'urn:server.CarregaProduto',
    'urn:server.CarregaProduto#CarregaProduto',
    'rpc',
    'encoded',
    'Retorna o resultado'
);

function CarregaProduto($token, $versao, $msg, $sincronizar)
{
    require_once("AutoLoader.php");
    return Webservice::CarregaProduto($token, $versao, $msg, $sincronizar);
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
