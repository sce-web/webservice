<?php
require_once('Lib/webservice/nusoap.php');

$server = new soap_server;
$server->configureWSDL('server.ListarPedidoNaoProcessadoPelaSCE', 'urn:server.ListarPedidoNaoProcessadoPelaSCE');
$server->wsdl->schemaTargetNamespace = 'urn:server.ListarPedidoNaoProcessadoPelaSCE';
$server->register('ListarPedidoNaoProcessadoPelaSCE', array(), array('return' => 'xsd:string'),
    'urn:server.ListarPedidoNaoProcessadoPelaSCE',
    'urn:server.ListarPedidoNaoProcessadoPelaSCE#ListarPedidoNaoProcessadoPelaSCE', 'rpc', 'encoded',
    'Retorna o resultado'
);

function ListarPedidoNaoProcessadoPelaSCE()
{
    require_once("AutoLoader.php");
    return Webservice::ListaPedidoNaoProcessadoPelaSce();
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);