<?php
require_once('Lib/webservice/nusoap.php');

$server = new soap_server;

$server->configureWSDL('server.CarregaCliente', 'urn:server.CarregaCliente');
$server->wsdl->schemaTargetNamespace = 'urn:server.CarregaCliente';


$server->register('CarregaCliente',
    array('token' => 'xsd:string', 'versao' => 'xsd:string', 'msg' => 'xsd:string', 'sincronizar' => 'xsd:string'),
    array('return' => 'xsd:string'),
    'urn:server.CarregaCliente',
    'urn:server.CarregaCliente#CarregaCliente',
    'rpc',
    'encoded',
    'Retorna o resultado'
);


function CarregaCliente($token, $versao, $msg, $sincronizar)
{
    require_once("AutoLoader.php");
    return Webservice::CarregaCliente($token, $versao, $msg, $sincronizar);
    /*
    if ($token == "") {
        return "<?xml version='1.0' encoding='utf-16'?>
	<WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
	  <Status>false</Status>
	  <Retorno></Retorno>
	  <Erro>O token estÁ vazio</Erro>
	</WsParam>";
    } else {
        if ($token == md5("6a5443f33950da8dcf871ff128f8bcbe")) {
            if ($versao == "1") {
                try {
                    $banco = DB_DATABASE;
                    $usuario = DB_USERNAME;
                    $senha = DB_PASSWORD;
                    $hostname = DB_HOSTNAME;
                    $conn = mysql_connect($hostname, $usuario, $senha);
                    mysql_select_db($banco) or die("Não foi possível conectar ao banco MySQL");
                    if (!$conn) {
                        throw new Exception("Não foi possível conectar ao banco MySQL. ");


                    }

                    mysql_query("SET AUTOCOMMIT=0");
                    mysql_query("START TRANSACTION");
                    $cliente = explode(chr(254), $msg);
                    $max = count($cliente);
                    for ($i = 0; $i < $max; $i++) {
                        $campo = explode("|", $cliente[$i]);
                        $codigo = $campo[0];
                        $nrocampos = count($campo);

                        $email = explode(";", $campo[$nrocampos - 1]);

                        $msqlclientesantigos = mysql_query("SELECT * FROM customer WHERE empresa_id = '" . $campo[0] . "'");
                        $ativo = false;
                        while ($clienteantigos = mysql_fetch_assoc($msqlclientesantigos)) {
                            for ($j = 0; $j < $nrocampos; $j++) {
                                if ($clienteantigos["email"] == $email[$j]) {
                                    $ativo = true;
                                }

                            }

                            if (!$ativo) {
                                mysql_query("UPDATE customer SET customer_group_id = 1, empresa_id = '' WHERE email = '" . $clienteantigos["email"] . "'");
                            }

                            $ativo = false;

                        }


                        for ($j = 0; $j < $nrocampos; $j++) {
                            $msqlcliente = mysql_query("SELECT * FROM customer WHERE email = '" . $email[$j] . "'");
                            if ($datacliente = mysql_fetch_assoc($msqlcliente)) {
                                if ($sincronizar == "S") {
                                    $tipocliente = 0;

                                    if ($campo[1] == "N") {
                                        $tipocliente = 1;
                                    } else {
                                        if (($campo[1] == "S") && ($campo[2] == "S")) {
                                            $tipocliente = 2;
                                        } else {
                                            $tipocliente = 3;
                                        }

                                    }

                                    if (!mysql_query("UPDATE customer SET customer_group_id = " . $tipocliente . ", empresa_id = '" . $campo[0] . "' WHERE  customer_id = " . $datacliente["customer_id"])) {
                                        throw new Exception("Não alterou o grupo do cliente com e-mail " . $email[$j] . " mysqError: " . mysql_error());
                                    }

                                    if (!mysql_query("UPDATE address SET company_id = '" . $campo[6] . "', tax_id = '" . $campo[4] . "', address_1 = '" . $campo[7] . "', address_2 = '" . $campo[10] . "', city = '" . $campo[11] . "', postcode = '" . $campo[12] . "', numero = '" . $campo[8] . "', complemento = '" . $campo[9] . "', zone_id = (SELECT zone_id FROM zone WHERE code = '" . $campo[13] . "' LIMIT 1), company = '" . $campo[5] . "' WHERE customer_id = " . $datacliente["customer_id"])) {
                                        throw new Exception("Não alterou o endereço do cliente com e-mail " . $email[$j] . " mysqError: " . mysql_error());
                                    }
                                }
                            } else {
                                $tipocliente = 0;

                                if ($campo[1] == "N") {
                                    $tipocliente = 1;
                                } else {
                                    if (($campo[1] == "S") && ($campo[2] == "S")) {
                                        $tipocliente = 2;
                                    } else {
                                        $tipocliente = 3;
                                    }

                                }

                                if ($email[$j] != "") {
                                    if (!mysql_query("INSERT INTO customer (empresa_id, customer_group_id, email, password, status, approved, store_id, date_added) VALUES
										('" . $campo[0] . "', " . $tipocliente . ", '" . $email[$j] . "', '" . md5('mudar123') . "', 1,1,0,now())")
                                    ) {
                                        throw new Exception("Não inseriu o cliente com e-mail " . $email[$j] . " mysqError: " . mysql_error());
                                    }

                                    $mysqlcli = mysql_query("SELECT * FROM customer WHERE email = '" . $email[$j] . "'");

                                    if ($cli = mysql_fetch_assoc($mysqlcli)) {
                                        if (!mysql_query("INSERT INTO address (customer_id, company_id, tax_id, address_1, address_2, city, postcode, numero, complemento, country_id, zone_id, company) VALUES (" . $cli["customer_id"] . ", '" . $campo[6] . "', '" . $campo[4] . "', '" . $campo[7] . "', '" . $campo[10] . "', '" . $campo[11] . "', '" . $campo[12] . "', '" . $campo[8] . "', '" . $campo[9] . "', 30, (SELECT zone_id FROM zone WHERE code = '" . $campo[13] . "' LIMIT 1), '" . $campo[5] . "')")) {
                                            throw new Exception("Não inseriu o endereço do cliente com e-mail " . $email[$j] . " mysqError: " . mysql_error());
                                        }

                                        $mysqladd = mysql_query("SELECT * FROM address ORDER BY address_id DESC LIMIT 1");

                                        if ($address = mysql_fetch_assoc($mysqladd)) {
                                            if (!mysql_query("UPDATE customer SET address_id = " . $address["address_id"] . " WHERE customer_id = " . $cli["customer_id"])) {
                                                throw new Exception("Não inseriu o endereço do cliente com e-mail " . $email[$j] . " mysqError: " . mysql_error());
                                            }
                                        }
                                    }

                                    $msg = "Prezado cliente

										Sua conta foi criada, e você poderá acessar sua conta utilizando seu e-mail: '" . $email[$j] . "'
										e senha: 'mudar123' ao visitar nossa loja, ou através do seguinte link:
										http://www.artnit.com.br/index.php?route=account/login

										Ao acessar sua conta você poderá acessar outros serviços, inclusive visualizar
										compras anteriores e mudar a senha.

										Atenciosamente,
										Artnit";

                                    mail($email[$j], "Artnit - Senha de cadastro", $msg);
                                }


                            }


                        }
                    }

                    mysql_query("COMMIT");
                    return "<?xml version='1.0' encoding='utf-16'?>
					<WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
					  <Status>true</Status>
					  <Retorno></Retorno>
					  <Erro></Erro>
					</WsParam>";
                } catch (Exception $e) {
                    mysql_query("ROLLBACK");
                    return "<?xml version='1.0' encoding='utf-16'?>
		<WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
		  <Status>false</Status>
		  <Retorno></Retorno>
		  <Erro>" . $e->getMessage() . "</Erro>
		</WsParam>";
                }


            }
        } else {
            return "<?xml version='1.0' encoding='utf-16'?>
	<WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
	  <Status>false</Status>
	  <Retorno></Retorno>
	  <Erro>O token está inválido</Erro>
	</WsParam>";

        }
    }
    */
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);

