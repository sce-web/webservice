<?php
/**
 * Constants definitions
 * so the system auto-findings won't break
 */

// HTTP
define("HTTP_SERVER", "");

// HTTPS
define("HTTPS_SERVER", "");

// DIR
define("DIR_ROOT", "");
define("DIR_APPLICATION", "");
define("DIR_SYSTEM", "");
define("DIR_LANGUAGE", "");
define("DIR_TEMPLATE", "");
define("DIR_CONFIG", "");
define("DIR_IMAGE", "");
define("DIR_CACHE", "");
define("DIR_DOWNLOAD", "");
define("DIR_UPLOAD", "");
define("DIR_MODIFICATION", "");
define("DIR_LOGS", "");

// DB
define("DB_DRIVER", "mysqli");

define("DB_HOSTNAME", "127.0.0.1");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "");
define("DB_DATABASE", "webservice_sample");