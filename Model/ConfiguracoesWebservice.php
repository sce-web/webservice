<?php
class ConfiguracoesWebservice
{
    /**
     * @deprecated Não deve ser mais utilizado
     * @var bool
     */
    public $emTeste = false;

    /**
     * ID de linguagem do OpenCart (geralmente 2)
     * @var integer
     */
    protected $language_id;

    /**
     * Token de validação de sessão do webservice
     * @var string
     */
    protected $token;

    /**
     * Em busca de versão de funcionamento do webservice
     * @var string
     */
    protected $versao;

    /**
     * Texto CSV formatado contendo os dados de entrada do webservice (`explodido` em um vetor)
     * @var string
     */
    protected $msg;

    /**
     * Contendo um valor S ou N se a sincronização deve ser feita ou não
     * @var string
     */
    protected $sincronizar;

    /**
     * Contém o status do do XML de retorno (WsParam)
     * @var string
     */
    protected $status = "true";

    /**
     * Contém as informações dos dados (caso exista alguma informação para retornar)
     * do XML de retorno (WsParam)
     * @var
     */
    protected $retorno;

    /**
     * Contém o texto de erro caso o retorno seja falso
     * e tenha acontecido algum erro no processo de integração
     * @var string
     */
    protected $erro;

    /**
     * Objeto de conexão com o banco de dados
     * @var mysqli
     */
    protected $conn;

    /**
     * @throws Exception Em caso de falha na conexão
     */
    public function __construct()
    {
        $this->conn = Connection::open();
    }

    /**
     * @param string $token Hash de verificação (para autenticar se o OpenCart que está tentando ser populado é o mesmo
     * cliente do AutoMagazine)
     * @throws Exception Em caso do token da base de dados e o inserido confrontarem como falso
     */
    public function authenticateToken($token)
    {
        if (md5(Token::value($this->conn)) !== $token) {
            throw new Exception("Token de autenticação inválido.");
        }
    }

    /**
     * @param string $query Uma query SQL
     * @return bool|mysqli_result Um objeto mysqli_result da query executada, ou false em caso de falha
     * @throws Exception Com o texto SQL e qual o erro gerado, em caso de erro na query
     */
    public function query($query)
    {
        $response = $this->conn->query($query);

        if ($this->conn->error) {
            throw new Exception("SQL: {$query} | ERROR: {$this->conn->error}");
        }

        return $response;
    }

    public function queryFetchAll($query) {
        $response = array();

        $queryCall = $this->query($query);

        while ($row = $queryCall->fetch_assoc()) {
            array_push($response, $row);
        }

        return $response;
    }

    //O modo sempre habilitado define que um produto sempre subirá habilitado independente do estoque
    public function sempreHabilitado($nomeWS)
    {
        $sql = "SELECT `value` AS valor FROM web_configuration
            WHERE `option` = 'sobeSempreHabilitado'
            AND   webservice = '{$nomeWS}'
            LIMIT 1
            ";

        $result = $this->query($sql);
        $valor = 'false';
        while ($row = $result->fetch_assoc()) {
            $valor = $row["valor"];
        }
        $valor = $valor === 'true' ? true : false;

        return $valor;
    }

    //Categorias em que os produtos sobem sempre habilitados
    public function categoriasSemControleEstoque($categorias)
    {
        $valor = false;

        $sql = "SELECT `value` AS valor FROM web_configuration
            WHERE `option` = 'categoriasSemControleEstoque'
            AND   webservice = 'wsCarregaProduto'
            LIMIT 1
            ";

        $result = $this->query($sql);
        while ($row = $result->fetch_assoc()) {
            foreach ($categorias as $categoria) {
                if ($categoria == $row["valor"]) {
                    $valor = true;
                }
            }


        }
        $valor = $valor === 'true' ? true : false;

        return $valor;

    }

    //Fabricante em que os produtos sobem sempre habilitados
    public function fabricanteSemControleEstoque($fabricante)
    {

        $sql = "SELECT `value` AS valor FROM web_configuration
            WHERE `option` = 'fabricanteSemControleEstoque'
            AND   webservice = 'wsCarregaProduto'
            LIMIT 1
            ";

        $result = $this->query($sql);
        while ($row = $result->fetch_assoc()) {

            if ($fabricante == $row["valor"]) {
                return true;
            }

        }
        return false;

    }

    /**
     * Retorna o objeto que representa o XML de retorno do Webservice
     * @return SimpleXMLElement Objeto XML de retorno do Webservice
     */
    public static function getWsParam()
    {
        $WsParam = new SimpleXMLElement(
            "<?xml version='1.0' encoding='utf-8'?><WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'></WsParam>"
        );
        $WsParam->Status = '';
        $WsParam->Retorno = '';
        $WsParam->Erro = '';
        return $WsParam;
    }

    /**
     * @return int
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * @param int $language_id
     * @return ConfiguracoesWebservice
     */
    public function setLanguageId($language_id)
    {
        $this->language_id = $language_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return ConfiguracoesWebservice
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getVersao()
    {
        return $this->versao;
    }

    /**
     * @param string $versao
     * @return ConfiguracoesWebservice
     */
    public function setVersao($versao)
    {
        $this->versao = $versao;
        return $this;
    }

    /**
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @param string $msg
     * @return ConfiguracoesWebservice
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;
        return $this;
    }

    /**
     * @return string
     */
    public function getSincronizar()
    {
        return $this->sincronizar;
    }

    /**
     * @param string $sincronizar
     * @return ConfiguracoesWebservice
     */
    public function setSincronizar($sincronizar)
    {
        $this->sincronizar = $sincronizar;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return ConfiguracoesWebservice
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetorno()
    {
        return $this->retorno;
    }

    /**
     * @param mixed $retorno
     * @return ConfiguracoesWebservice
     */
    public function setRetorno($retorno)
    {
        $this->retorno = $retorno;
        return $this;
    }

    /**
     * @return string
     */
    public function getErro()
    {
        return $this->erro;
    }

    /**
     * @param string $erro
     * @return ConfiguracoesWebservice
     */
    public function setErro($erro)
    {
        $this->erro = $erro;
        return $this;
    }

    /**
     * Deprecated methods since 2015-10-27
     */

    //Salva a configuração na tabela configuracoes_webservice
    /**
     * @param string $nomeWS
     * @param string $opcao
     * @param string $valor
     * @deprecated Em tese, não é mais utilizaod
     */
    public function salvaConfig($nomeWS, $opcao, $valor)
    {
        $this->query(
            "UPDATE web_configuration
            SET `value`= '{$valor}'
            WHERE  `option` = '{$opcao}'
            AND `webservice` = '{$nomeWS }';"
        );
        //echo $sql;
    }

    /**
     * @deprecated Não deve ser mais utilizado
     */
    public function conecta()
    {
        $banco = DB_DATABASE;
        $usuario = DB_USERNAME;
        $senha = DB_PASSWORD;
        $hostname = DB_HOSTNAME;

        //echo "Connecting to $banco using username:'$usuario'@password:'$senha' at $hostname";


        $conn = mysql_connect($hostname, $usuario, $senha);
        mysql_select_db($banco) or die("Não foi possível conectar ao banco MySQL");
        if (!$conn) {
            echo "Não foi possível conectar ao banco MySQL. ";
            exit;
        }
    }

    //Transactions
    /**
     * @deprecated Não deve ser mais utilizado
     */
    function begin()
    {
        mysql_query("BEGIN");
    }

    /**
     * @deprecated Não deve ser mais utilizado
     */
    function commit()
    {
        mysql_query("COMMIT");
    }

    /**
     * @deprecated Não deve ser mais utilizado
     */
    function rollback()
    {
        mysql_query("ROLLBACK");
    }

    /**
     * @param $v
     * @throws Exception
     */
    function throwDump($v)
    {
        ob_start();
        var_dump($v);
        $d = ob_get_contents();
        ob_end_clean();
        throw new Exception($d);
    }


    //Verifica se o werbservice está em modo de testes local
    /**
     * @deprecated Não deve ser mais  utilizado
     */
    public function emTeste()
    {
        if ($_SERVER['SERVER_NAME'] == "localhost") {
            $this->emTeste = true;
        }
    }
}
