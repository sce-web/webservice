<?php

class Webservice
{
    public static function GetSession()
    {
        return "";
    }

    public static function CarregaCategoria($token, $versao, $msg, $sincronizar)
    {
        $WsParam = ConfiguracoesWebservice::getWsParam();

        try {
            $categoria = new Categoria();
            $categoria->authenticateToken($token);
            $categoria->setVersao($versao);
            $categoria->setMsg($msg);
            $categoria->setSincronizar($sincronizar);
            $categoria->carregar();

            $WsParam->Status = $categoria->getStatus();
            $WsParam->Retorno = $categoria->getRetorno();
            $WsParam->Erro = $categoria->getErro();

            $response = $WsParam->asXML();
        } catch (Exception $exc) {
            $WsParam->Status = "false";
            $WsParam->Erro = utf8_decode($exc->getMessage() . "\n file: " . $exc->getFile() . " \n line: " . $exc->getLine());

            $response = $WsParam->asXML();
        }

        return $response;
    }

    public static function CarregaProduto($token, $versao, $msg, $sincronizar)
    {
        $WsParam = ConfiguracoesWebservice::getWsParam();

        try {
            $produto = new Produto();
            $produto->authenticateToken($token);
            $produto->setVersao($versao);
            $produto->setMsg($msg);
            $produto->setSincronizar($sincronizar);
            $produto->carregar();

            $WsParam->Status = $produto->getStatus();
            $WsParam->Retorno = $produto->getRetorno();
            $WsParam->Erro = $produto->getErro();

            $response = $WsParam->asXML();

        } catch (Exception $exc) {
            $WsParam->Status = "false";
            $WsParam->Erro = $exc->getMessage() . "\n file: " . $exc->getFile() . " \n line: " . $exc->getLine();

            $response = $WsParam->asXML();
        }

        return $response;
    }

    public static function ListaPedidoNaoProcessadoPelaSce()
    {

        $banco = DB_DATABASE;
        $usuario = DB_USERNAME;
        $senha = DB_PASSWORD;
        $hostname = DB_HOSTNAME;

        $conn = mysqli_connect($hostname, $usuario, $senha);

        mysqli_select_db($conn, $banco) or die("N?o foi poss?vel conectar ao banco MySQL");

        if (!$conn) {
            echo "N�o foi poss�vel conectar ao banco MySQL. ";
            exit;
        }

        $WsParam = ConfiguracoesWebservice::getWsParam();

        try {
            $pedidos = mysqli_query($conn, "SELECT * FROM `order` WHERE order_status_id = 5");

            $conteudo = "";

            while ($pedido = mysqli_fetch_assoc($pedidos)) {

                $Order_Id = $pedido["order_id"];
                $Customer_id = $pedido["customer_id"];
                $Date_Added = $pedido["date_added"];
                $Customer_Tipo = "PF";

                $customer = mysqli_query($conn, "
                    SELECT *
                    FROM `customer` c
                    WHERE customer_id = {$pedido["customer_id"]};
                ");

                # region
                $customFieldPosition = Webservice::getCustomFieldPositions($conn);

                $orderPaymentCustomFields = json_decode($pedido['payment_custom_field'], true);

                /** OpenCart 2.0 fallback */
                if (!is_array($orderPaymentCustomFields)) {
                    $orderPaymentCustomFields = unserialize($pedido['payment_custom_field']);
                }

                $pedido['payment_number'] = $orderPaymentCustomFields[$customFieldPosition['number']];
                $pedido['payment_complement'] = $orderPaymentCustomFields[$customFieldPosition['complement']];

                $orderShippingCustomFields = json_decode($pedido['shipping_custom_field'], true);

                /** OpenCart 2.0 fallback */
                if (!is_array($orderShippingCustomFields)) {
                    $orderShippingCustomFields = unserialize($pedido['shipping_custom_field']);
                }

                $pedido['shipping_number'] = $orderShippingCustomFields[$customFieldPosition['number']];
                $pedido['shipping_complement'] = $orderShippingCustomFields[$customFieldPosition['complement']];
                # endregion

                $pedido['delivery_date'] = '';
                $pedido['delivery_time'] = '';

                $Customer_Documento = "";
                $Customer_IE = "";

                if ($dcust = mysqli_fetch_assoc($customer)) {

                    $Empresa_Id = isset($dcust["empresa_id"]) ? $dcust["empresa_id"] : 0;

                    $customerCustomFields = json_decode($dcust['custom_field'], true);

                    /** OpenCart 2.0 fallback */
                    if (!is_array($customerCustomFields)) {
                        $customerCustomFields = unserialize($dcust['custom_field']);
                    }


                    $Customer_Documento = str_replace(
                        array('-', '/', '.'),
                        array('', '', ''),
                        $customerCustomFields[$customFieldPosition['cpf']]
                    );
                    $Customer_Tipo = (strlen($Customer_Documento) == 14) ? "PJ" : "PF";
                    //$Customer_IE = $dcust["ie"];

                }

                $Customer_Nome = $pedido["firstname"] . $pedido["lastname"];

                if (isset($pedido["dtnasc"]) && $pedido["dtnasc"] != "0000-00-00") {
                    $Customer_DTNasc = date("Y-m-d G:i:s", $pedido["dtnasc"]);
                } else {
                    $Customer_DTNasc = "";
                }

                $Customer_Sexo = 'M';//$pedido["sex"];
                $Customer_email = $pedido["email"];
                $Customer_tel = $pedido["telephone"];
                $Customer_MesmoEndereco = (($pedido["payment_address_1"] == $pedido["shipping_address_1"]) && ($pedido["payment_address_2"] == $pedido["shipping_address_2"])) ? "S" : "N";
                $Customer_Entrega_Endereco = trim($pedido["shipping_address_1"]);
                $Customer_Entrega_Numero = $pedido["shipping_number"];
                $Customer_Entrega_Complemento = $pedido["shipping_complement"];
                $Customer_Entrega_Referencia = $pedido["shipping_company"]; //company é a referencia no OC
                $Customer_Entrega_Bairro = $pedido["shipping_address_2"];
                $Customer_Entrega_Cidade = $pedido["shipping_city"];
                $Customer_Entrega_CEP = $pedido["shipping_postcode"];

                $estados = mysqli_query($conn, "
                    SELECT *
                    FROM `zone`
                    WHERE country_id = 30
                    AND `name` = '{$pedido["shipping_zone"]}'
                ") or die("Erro na consulta");

                if ($estado = mysqli_fetch_assoc($estados)) {
                    $Customer_Entrega_Estado = $estado["code"];
                } else {
                    $Customer_Entrega_Estado = $pedido["shipping_zone"];
                }

                $Customer_Entrega_Pais = $pedido["shipping_country"];
                $Customer_Cobranca_Endereco = trim($pedido["payment_address_1"]);
                $Customer_Cobranca_Numero = $pedido["payment_number"];
                $Customer_Cobranca_Complemento = $pedido["payment_complement"];
                $Customer_Cobranca_Referencia = $pedido["payment_company"]; //company é a referencia no OC
                $Customer_Cobranca_Bairro = $pedido["payment_address_2"];
                $Customer_Cobranca_Cidade = $pedido["payment_city"];
                $Customer_Cobranca_CEP = $pedido["payment_postcode"];
                $Customer_Cobranca_Pais = $pedido["payment_country"];

                $estados = mysqli_query($conn, "
                    SELECT *
                    FROM `zone`
                    WHERE country_id = 30
                    AND `name` = '{$pedido["payment_zone"]}'
                ") or die("Erro na consulta");

                if ($estado = mysqli_fetch_assoc($estados)) {
                    $Customer_Cobranca_Estado = $estado["code"];
                } else {
                    $Customer_Cobranca_Estado = $pedido["shipping_zone"];
                }


                //$estados = mysqli_query($conn, "SELECT * FROM `zone` WHERE country_id = 30 AND name = '" . $pedido["payment_zone"] . "'") or die("Erro na consulta");

                if ($estado = mysqli_fetch_assoc($estados)) {
                    $Custumer_Entrega_Estado = $estado["code"];
                } else {
                    $Custumer_Cobranca_Estado = $pedido["payment_zone"];
                }

                $SQLclientes = mysqli_query($conn, "
                    SELECT customer_group_id
                    FROM customer
                    WHERE customer_id = {$Customer_id};
                ");


                if ($datacliente = mysqli_fetch_assoc($SQLclientes)) {
                    if ($datacliente["customer_group_id"] == 1) {
                        $Forma_Pagamento_Tipo = "BV";
                    } else {
                        if ($datacliente["customer_group_id"] == 2) {
                            $Forma_Pagamento_Tipo = "BC";
                        }
                    }
                }

                $Forma_Pagamento_DataOperacao = "";
                $Forma_Pagamento_Bandeira = "";
                $Forma_Pagamento_TID = "";
                $Forma_Pagamento_AUTH = "";
                $Forma_Pagamento_Operacao = "";
                $Forma_Pagamento_Parcelas = 1;

                //$Forma_Pagamento_Tipo = "B";
                // E pagto na entrega
                // C de cartao
                // B ou BV de boleto

                $result = mysqli_query($conn,
                    "SELECT payment_method FROM `order` WHERE `order_id` = " . $pedido["order_id"]);
                $order = mysqli_fetch_assoc($result);
                $Forma_Pagamento_Bandeira = $order['payment_method']; //sodexo, dinheiro, etc


                //}


                $totals = mysqli_query($conn, "SELECT * FROM order_total WHERE order_id = " . $pedido["order_id"]);

                $Valor_Desconto_Nota = 0;

                while ($total = mysqli_fetch_assoc($totals)) {
                    if ($total["sort_order"] == "1") {
                        $Sub_total = $total["value"];
                    } else {
                        if ($total["sort_order"] == "3") {
                            $Valor_Frete = $total["value"];
                        } else {
                            if ($total["sort_order"] == "9") {
                                $Total = $total["value"];
                            } else {
                                if ($total["code"] == "coupon") {
                                    $Valor_Desconto_Nota = $total["value"];
                                }
                            }
                        }
                    }
                }


                $sstatus = mysqli_query($conn,
                    "SELECT order_status.order_status_id AS status FROM `order` INNER JOIN order_history ON `order`.order_id = order_history.order_id INNER JOIN order_status ON order_status.order_status_id = order_history.order_status_id WHERE `order`.order_id = " . $pedido["order_id"] . " ORDER BY order_history.order_history_id DESC LIMIT 1");

                $STATUS = "";
                if ($dstatus = mysqli_fetch_assoc($sstatus)) {
                    if ($dstatus["status"] == "5") {
                        $STATUS = "A";
                    } else {
                        if ($dstatus["status"] == "7") {
                            $STATUS = "C";
                        } else {
                            if ($dstatus["status"] == "0") {
                                $STATUS = "PP";
                            }
                        }
                    }
                }

                $DataAprovacao = $pedido["date_modified"];

                $settings = mysqli_query($conn,
                    "SELECT sort_order FROM order_total WHERE sort_order NOT IN (1,9) AND code = 'shipping' AND order_id = " . $pedido["order_id"]) or die("Erro na consulta");

                $Codigo_Frete = "";
                if ($setting = mysqli_fetch_assoc($settings)) {
                    $Codigo_Frete = $setting["sort_order"];
                }


                if ($Codigo_Frete != "3") {
                    $Transportadora = "O pr�prio";
                } else {
                    $totals = mysqli_query($conn, "SELECT * FROM order_total WHERE order_id = " . $pedido["order_id"]);

                    while ($total = mysqli_fetch_assoc($totals)) {
                        if ($total["sort_order"] == "3") {
                            $arrayTransportadora = explode(".", $total["title"]);
                            $ServicoTransportadora = trim($arrayTransportadora[0]);
                            $ServicoTransportadora = '';
                            $dataHoraEncomenda = '';
                            /*if (($ServicoTransportadora == "Sedex") || ($ServicoTransportadora == "PAC") || ($ServicoTransportadora == "Sedex a Cobrar")
                                    || ($ServicoTransportadora == "Sedex10") || ($ServicoTransportadora == "e-Sedex")) {
                                $Transportadora = "Correio";
                                $arrayPrazoFrete = explode(":", $arrayTransportadora[1]);
                                $PrazoFrete = trim($arrayPrazoFrete[1]);
                            } else {
                                $Transportadora = "Frete Fixo";
                                $ServicoTransportadora = "";
                            }*/
                            if (($ServicoTransportadora == "Sedex") || ($ServicoTransportadora == "PAC") || ($ServicoTransportadora == "Sedex a Cobrar")
                                || ($ServicoTransportadora == "Sedex10") || ($ServicoTransportadora == "e-Sedex")
                            ) {
                                $Transportadora = "Correio";
                                $arrayPrazoFrete = explode(":", $arrayTransportadora[1]);
                                //$PrazoFrete = trim($arrayPrazoFrete[1]);
                                $PrazoFrete = '';
                                $tipoPedido = "P";
                            } elseif ($ServicoTransportadora == "Retirar na Loja") {
                                $PrazoFrete = 0;
                                $Transportadora = "Frete Fixo";
                                $tipoPedido = "P"; //Pedido normal
                            } elseif ($ServicoTransportadora = "Encomenda") {
                                $tipoPedido = "E"; //Encomenda
                                $res = mysqli_query($conn,
                                    "SELECT * FROM `order` WHERE order_id = " . $pedido["order_id"]);
                                while ($dados = mysqli_fetch_assoc($res)) {
                                    $PrazoFrete = 0;
                                    $horaInicial = explode(":", $pedido["delivery_time"]);
                                    //$dataHoraEncomenda = $dados['delivery_date'] . " " . $horaInicial[0] . ":30";
                                }
                                $Transportadora = "";
                            } else {
                                $tipoPedido = "P"; //Pedido normal
                                $PrazoFrete = 0;
                                $Transportadora = "Frete Fixo";
                            }
                        }
                    }
                }

                /**
                 * $conteudo = $conteudo . "$Order_Id;$Date_Added;$Customer_Tipo;$Customer_Documento;$Customer_Nome;$Customer_IE;$Customer_DTNasc;$Customer_Sexo;$Customer_email ;$Customer_tel;$Customer_MesmoEndereco;$Customer_Entrega_Endereco;$Customer_Entrega_Numero;$Customer_Entrega_Complemento;$Customer_Entrega_Referencia;$Customer_Entrega_Bairro;$Customer_Entrega_Cidade;$Customer_Entrega_CEP;$Customer_Entrega_Estado;$Customer_Entrega_Pais;$Customer_Cobranca_Endereco;$Customer_Cobranca_Numero;$Customer_Cobranca_Complemento;$Customer_Cobranca_Referencia;$Customer_Cobranca_Bairro;$Customer_Cobranca_Cidade;$Customer_Cobranca_CEP;$Customer_Cobranca_Estado;$Customer_Cobranca_Pais;$Forma_Pagamento_Tipo;$Forma_Pagamento_DataOperacao;$Forma_Pagamento_Operacao;$Forma_Pagamento_Bandeira;$Forma_Pagamento_TID;$Forma_Pagamento_AUTH;$Forma_Pagamento_Parcelas;$Codigo_Frete;$Transportadora;$ServicoTransportadora;$Valor_Frete;$Valor_Desconto_Nota;$Sub_total;$Total;$STATUS;$DataAprovacao;$PrazoFrete;$Customer_id;$Empresa_Id\r\n";
                 */

                $quebraLinha = "\r\n";
                $separador = chr(124);

                $conteudo =
                    $conteudo .
                    "$Order_Id" .
                    $separador .
                    "$Date_Added" .
                    $separador .
                    "$Customer_Tipo" .
                    $separador .
                    "$Customer_Documento" .
                    $separador .
                    "$Customer_Nome" .
                    $separador .
                    "$Customer_IE" .
                    $separador .
                    "$Customer_DTNasc" .
                    $separador .
                    "$Customer_Sexo" .
                    $separador .
                    "$Customer_email " .
                    $separador .
                    "$Customer_tel" .
                    $separador .
                    "$Customer_MesmoEndereco" .
                    $separador .
                    "$Customer_Entrega_Endereco" .
                    $separador .
                    "$Customer_Entrega_Numero" .
                    $separador .
                    "$Customer_Entrega_Complemento" .
                    $separador .
                    "$Customer_Entrega_Referencia" .
                    $separador .
                    "$Customer_Entrega_Bairro" .
                    $separador .
                    "$Customer_Entrega_Cidade" .
                    $separador .
                    "$Customer_Entrega_CEP" .
                    $separador .
                    "$Customer_Entrega_Estado" .
                    $separador .
                    "$Customer_Entrega_Pais" .
                    $separador .
                    "$Customer_Cobranca_Endereco" .
                    $separador .
                    "$Customer_Cobranca_Numero" .
                    $separador .
                    "$Customer_Cobranca_Complemento" .
                    $separador .
                    "$Customer_Cobranca_Referencia" .
                    $separador .
                    "$Customer_Cobranca_Bairro" .
                    $separador .
                    "$Customer_Cobranca_Cidade" .
                    $separador .
                    "$Customer_Cobranca_CEP" .
                    $separador .
                    "$Customer_Cobranca_Estado" .
                    $separador .
                    "$Customer_Cobranca_Pais" .
                    $separador .
                    "$Forma_Pagamento_Tipo" .
                    $separador .
                    "$Forma_Pagamento_DataOperacao" .
                    $separador .
                    "$Forma_Pagamento_Operacao" .
                    $separador .
                    "$Forma_Pagamento_Bandeira" .
                    $separador .
                    "$Forma_Pagamento_TID" .
                    $separador .
                    "$Forma_Pagamento_AUTH" .
                    $separador .
                    "$Forma_Pagamento_Parcelas" .
                    $separador .
                    "$Codigo_Frete" .
                    $separador .
                    "$Transportadora" .
                    $separador .
                    "$ServicoTransportadora" .
                    $separador .
                    "$Valor_Frete" .
                    $separador .
                    "$Valor_Desconto_Nota" .
                    $separador .
                    "$Sub_total" .
                    $separador .
                    "$Total" .
                    $separador .
                    "$STATUS" .
                    $separador .
                    "$DataAprovacao" .
                    $separador .
                    "$PrazoFrete" .
                    $separador .
                    "$Customer_id" .
                    $separador .
                    "$Empresa_Id" .
                    $separador .
                    "$tipoPedido" .
                    $separador .
                    "$dataHoraEncomenda" .
                    $separador .
                    //"$trocoPara" .
                    $quebraLinha;
            }

            $WsParam->Status = "true";
            $WsParam->Retorno = $conteudo;
            $response = $WsParam->asXML();
        } catch (Exception $e) {
            $WsParam->Status = "false";
            $WsParam->Erro = $e->getMessage();

            $response = $WsParam->asXML();
        }

        return $response;
    }

    public static function CapturaPedido($nropedido)
    {
        $WsParam = ConfiguracoesWebservice::getWsParam();

        try {
            $pedidoModel = new pedidoModel();
            $pedidoModel->nroPedido = $nropedido;
            $conteudo = $pedidoModel->capturaPedido();

            $WsParam->Status = "true";
            $WsParam->Retorno = $conteudo;

            $response = $WsParam->asXML();
        } catch (Exception $e) {
            $WsParam->Status = "false";
            $WsParam->Erro = $e->getMessage();

            $response = $WsParam->asXML();
        }

        return $response;
    }

    public static function AvisaPedidoProcessado($nropedido)
    {
        $WsParam = ConfiguracoesWebservice::getWsParam();

        try {
            $banco = DB_DATABASE;
            $usuario = DB_USERNAME;
            $senha = DB_PASSWORD;
            $hostname = DB_HOSTNAME;
            $conn = mysqli_connect($hostname, $usuario, $senha);
            mysqli_select_db($conn, $banco) or die("N�o foi poss�vel conectar ao banco MySQL");
            if (!$conn) {
                throw new Exception("N�o foi poss�vel conectar ao banco MySQL. ");
            }

            $pedido = mysqli_query($conn,
                "SELECT * FROM `order` WHERE order_id = " . $nropedido) or die("Erro na consulta de pedido");


            if ($ped = mysqli_fetch_assoc($pedido)) {
                $email = $ped["email"];
            }

            mysqli_query($conn, "UPDATE `order` SET order_status_id = 17 WHERE order_id = " . $nropedido);
            $status = mysqli_query($conn,
                "INSERT INTO order_history (order_id, order_status_id, notify, `comment`, date_added) VALUES (" . $nropedido . ", 17,1,'',NOW())");

            if ($status) {
                $WsParam->Status = "true";
                $WsParam->Retorno = "ok";

                $response = $WsParam->asXML();
            } else {
                throw new Exception(mysqli_error($conn));
            }
        } catch (Exception $e) {
            $WsParam->Status = "false";
            $WsParam->Erro = $e->getMessage();

            $response = $WsParam->asXML();
        }

        return $response;
    }

    public static function CarregaListaEscolar($token, $versao, $msg, $sincronizar)
    {
        $WsParam = ConfiguracoesWebservice::getWsParam();

        try {

            $listaEscolar = new ListaEscolar();

            $listaEscolar->authenticateToken($token);
            $listaEscolar->alimentarCampos($msg);
            $listaEscolar->carregar();

            $WsParam->Status = "true";

            $response = $WsParam->asXML();
        } catch (Exception $e) {

            $WsParam->Status = "false";
            $WsParam->Retorno = '';
            $WsParam->Erro = $e->getMessage();

            $response = $WsParam->asXML();

        }

        return $response;
    }

    # Specific Webservice Calls

    public static function CarregaCliente($token, $versao, $msg, $sincronizar)
    {
        $WsParam = ConfiguracoesWebservice::getWsParam();

        $WsParam->Status = 'true';

        return $WsParam->asXML();

        if ($token == md5("6a5443f33950da8dcf871ff128f8bcbe")) {

            try {
                $banco = DB_DATABASE;
                $usuario = DB_USERNAME;
                $senha = DB_PASSWORD;
                $hostname = DB_HOSTNAME;
                $conn = mysqli_connect($hostname, $usuario, $senha);
                mysqli_select_db($banco) or die("Não foi possível conectar ao banco MySQL");
                if (!$conn) {
                    throw new Exception("Não foi possível conectar ao banco MySQL.");


                }

                mysqli_query($conn, "SET AUTOCOMMIT=0");
                mysqli_query($conn, "START TRANSACTION");
                $cliente = explode(chr(254), $msg);
                $max = count($cliente);
                for ($i = 0; $i < $max; $i++) {
                    $campo = explode("|", $cliente[$i]);
                    $codigo = $campo[0];
                    $nrocampos = count($campo);

                    $email = explode(";", $campo[$nrocampos - 1]);

                    $msqlclientesantigos = mysqli_query($conn,
                        "SELECT * FROM customer WHERE empresa_id = '" . $campo[0] . "'");
                    $ativo = false;
                    while ($clienteantigos = mysqli_fetch_assoc($msqlclientesantigos)) {
                        for ($j = 0; $j < $nrocampos; $j++) {
                            if ($clienteantigos["email"] == $email[$j]) {
                                $ativo = true;
                            }

                        }

                        if (!$ativo) {
                            mysqli_query($conn,
                                "UPDATE customer SET customer_group_id = 1, empresa_id = '' WHERE email = '" . $clienteantigos["email"] . "'");
                        }

                        $ativo = false;

                    }


                    for ($j = 0; $j < $nrocampos; $j++) {
                        $msqlcliente = mysqli_query($conn, "SELECT * FROM customer WHERE email = '" . $email[$j] . "'");
                        if ($datacliente = mysqli_fetch_assoc($msqlcliente)) {
                            if ($sincronizar == "S") {
                                $tipocliente = 0;

                                if ($campo[1] == "N") {
                                    $tipocliente = 1;
                                } else {
                                    if (($campo[1] == "S") && ($campo[2] == "S")) {
                                        $tipocliente = 2;
                                    } else {
                                        $tipocliente = 3;
                                    }

                                }

                                if (!mysqli_query($conn,
                                    "UPDATE customer SET customer_group_id = " . $tipocliente . ", empresa_id = '" . $campo[0] . "' WHERE  customer_id = " . $datacliente["customer_id"])
                                ) {
                                    throw new Exception("N�o alterou o grupo do cliente com e-mail " . $email[$j] . " mysqError: " . mysqli_error());
                                }

                                if (!mysqli_query($conn,
                                    "UPDATE address SET company_id = '" . $campo[6] . "', tax_id = '" . $campo[4] . "', address_1 = '" . $campo[7] . "', address_2 = '" . $campo[10] . "', city = '" . $campo[11] . "', postcode = '" . $campo[12] . "', numero = '" . $campo[8] . "', complemento = '" . $campo[9] . "', zone_id = (SELECT zone_id FROM zone WHERE code = '" . $campo[13] . "' LIMIT 1), company = '" . $campo[5] . "' WHERE customer_id = " . $datacliente["customer_id"])
                                ) {
                                    throw new Exception("N�o alterou o endere�o do cliente com e-mail " . $email[$j] . " mysqError: " . mysqli_error());
                                }
                            }
                        } else {
                            $tipocliente = 0;

                            if ($campo[1] == "N") {
                                $tipocliente = 1;
                            } else {
                                if (($campo[1] == "S") && ($campo[2] == "S")) {
                                    $tipocliente = 2;
                                } else {
                                    $tipocliente = 3;
                                }

                            }

                            if ($email[$j] != "") {
                                if (!mysqli_query($conn, "INSERT INTO customer (empresa_id, customer_group_id, email, password, status, approved, store_id, date_added) VALUES
                                    ('" . $campo[0] . "', " . $tipocliente . ", '" . $email[$j] . "', '" . md5('mudar123') . "', 1,1,0,now())")
                                ) {
                                    throw new Exception("N�o inseriu o cliente com e-mail " . $email[$j] . " mysqError: " . mysqli_error());
                                }

                                $mysqlcli = mysqli_query($conn,
                                    "SELECT * FROM customer WHERE email = '" . $email[$j] . "'");

                                if ($cli = mysqli_fetch_assoc($mysqlcli)) {
                                    if (!mysqli_query($conn,
                                        "INSERT INTO address (customer_id, company_id, tax_id, address_1, address_2, city, postcode, numero, complemento, country_id, zone_id, company) VALUES (" . $cli["customer_id"] . ", '" . $campo[6] . "', '" . $campo[4] . "', '" . $campo[7] . "', '" . $campo[10] . "', '" . $campo[11] . "', '" . $campo[12] . "', '" . $campo[8] . "', '" . $campo[9] . "', 30, (SELECT zone_id FROM zone WHERE code = '" . $campo[13] . "' LIMIT 1), '" . $campo[5] . "')")
                                    ) {
                                        throw new Exception("N�o inseriu o endere�o do cliente com e-mail " . $email[$j] . " mysqError: " . mysqli_error());
                                    }

                                    $mysqladd = mysqli_query($conn,
                                        "SELECT * FROM address ORDER BY address_id DESC LIMIT 1");

                                    if ($address = mysqli_fetch_assoc($mysqladd)) {
                                        if (!mysqli_query($conn,
                                            "UPDATE customer SET address_id = " . $address["address_id"] . " WHERE customer_id = " . $cli["customer_id"])
                                        ) {
                                            throw new Exception("N�o inseriu o endere�o do cliente com e-mail " . $email[$j] . " mysqError: " . mysqli_error());
                                        }
                                    }
                                }

                                $msg = "Prezado cliente

                                    Sua conta foi criada, e voc� poder� acessar sua conta utilizando seu e-mail: '" . $email[$j] . "'
                                    e senha: 'mudar123' ao visitar nossa loja, ou atrav�s do seguinte link:
                                    http://www.artnit.com.br/index.php?route=account/login

                                    Ao acessar sua conta voc� poder� acessar outros servi�os, inclusive visualizar
                                    compras anteriores e mudar a senha.

                                    Atenciosamente,
                                    Artnit";

                                mail($email[$j], "Artnit - Senha de cadastro", $msg);
                            }


                        }


                    }
                }

                mysqli_query($conn, "COMMIT");
                $WsParam->Status = "true";

                $response = $WsParam->asXML();
            } catch (Exception $e) {
                mysqli_query($conn, "ROLLBACK");
                $WsParam->Status = "false";
                $WsParam->Erro = $e->getMessage();

                $response = $WsParam->asXML();
            }


        } else {
            $WsParam->Status = "false";
            $WsParam->Erro = "O token está inválido.";
            $response = $WsParam->asXML();
        }


        return $response;
    }

    public static function CarregaPedido($token, $versao, $msg, $sincronizar = null)
    {
        $WsParam = ConfiguracoesWebservice::getWsParam();

        try {
            $consultaPedido = new consultaPedido();
            $consultaPedido->processaPedidos($msg);

            $WsParam->Status = "true";

            $response = $WsParam->asXML();
        } catch (Exception $e) {
            $WsParam->Status = "false";
            $WsParam->Erro = $e->getMessage();

            $response = $WsParam->asXML();
        }

        return $response;
    }


    public static function getCustomFieldPositions($conn = null)
    {
        $customFieldsQuery = mysqli_query($conn, "
            SELECT c.custom_field_id, cfd.name
            FROM `custom_field` AS c
            JOIN `custom_field_description` AS cfd USING (custom_field_id)
            WHERE cfd.language_id = 2;
        ");

        $customFields = array();

        while ($customField = mysqli_fetch_assoc($customFieldsQuery)) {
            array_push($customFields, $customField);
        }

        $result = array();

        for ($i = 0; $i < count($customFields); $i++) {
            $result[
            preg_replace('/[`^~\'"]/', null,
                iconv('UTF-8', 'ASCII//TRANSLIT',
                    str_replace(" ", "_",
                        strtolower($customFields[$i]['name']))))] = $customFields[$i]['custom_field_id'];
        }

        return $result;
    }
}