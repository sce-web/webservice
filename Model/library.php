<?php
abstract class Token {
    /**
     * @param mysqli $conn Objeto de conex�o com banco de dados
     * @return string Com o valor da chave de encripta��o da loja (config_encryption)
     * @throws Exception Em caso de erro na query ou caso n�o encontre o campo 'config_encryption' do OpenCart
     */
    static function value(mysqli $conn){
        $sql = "SELECT `value` FROM setting WHERE `key` = 'config_encryption'";

        # verificar se precisa de store_id = 0;
        $res = $conn->query($sql);
        if (!empty($conn->error)) {
             throw new Exception("sql: ".$sql." erro: ".$conn->error);
        }
        if($data = $res->fetch_object()){
            return $data->value;
        } else {
            throw new Exception("sql: {$sql} erro: N�o existe chave e encripta��o (config_encryption) na configura��o da loja!");
        }
    }
}

abstract class Language {
    /**
     * @param mysqli $conn Objeto de conex�o com mo banco de dados
     * @return int Id da linguagem configurada para a loja
     * @throws Exception
     */
    static function value(mysqli $conn){
        $sql = "SELECT language_id FROM `language` WHERE `code` IN(
                SELECT `value` FROM setting WHERE store_id=0 AND `code`='config' AND `key`='config_language');";

        # verificar se precisa de store_id = 0;
        $res = $conn->query($sql);
        if (!empty($conn->error)) {
             throw new Exception("sql: ".$sql." erro: ".$conn->error);
        }
        if($data = $res->fetch_object()){
            return (int)$data->language_id;
        } else {
            throw new Exception("sql: {$sql} erro: N�o existe o campo de configura��o de linguagem (config_language) na loja!");
        }
    }
}

abstract class Erro {
    /**
     * @param string $msg Mensagem formatada para o formato de erro
     * @param Exception $e Objeto de Exception para se obter a mensagem de erro e o stack-trace
     * @return string Mensagem de erro formatada
     */
    static function mensagem($msg, Exception $e) {
        $trace = $e->getTrace();

        $result = '\n#### ERRO ####\nErro: ';

        if (!empty($msg))
            $result .= $msg.'\n';

        $result .= $e->getMessage();
        $result .= '\nOrigem: @ ';
        if($trace[0]['class'] != '') {
          $result .= $trace[0]['class'];
          $result .= '->';
        }
        $result .= $trace[0]['function'];
        $result .= '();\n file: '.$e->getFile().' \n line: '.$e->getLine();

        return $result;
    }
}

/**
 * Logging class:
 * - contains lfile, lwrite and lclose public methods
 * - lfile sets path and name of log file
 * - lwrite writes message to the log file (and implicitly opens log file)
 * - lclose closes log file
 * - first call of lwrite method will open log file implicitly
 * - message is written with the following format: [d/M/Y:H:i:s] (script name) message
 */
abstract class Logging {
    // declare log file and file pointer as private properties
    static private $log_file, $fp;
    // set log file (path and name)
    static public function lfile($path) {
        self::$log_file = $path;
    }
    // write message to the log file
    static public function lwrite($message) {
        // if file pointer doesn't exist, then open log file
        if (!is_resource(self::$fp)) {
            self::lopen();
            
        }
        // define script name
        $script_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
        // define current time and suppress E_WARNING if using the system TZ settings
        // (don't forget to set the INI setting date.timezone)
        $time = @date('[d/M/Y - H:i:s]');
        // write current time, script name and message to the log file
        fwrite(self::$fp, "$time ($script_name) $message" . PHP_EOL);
    }
    // close log file (it's always a good idea to close a file when you're done with it)
    static public function lclose() {
        fclose(self::$fp);
    }
    // open log file (private method)
    static private function lopen() {
        // in case of Windows set default log file
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $log_file_default = 'c:/php/logfile.txt';
        }
        // set default log file for Linux and other systems
        else {
            $log_file_default = '/tmp/logfile.txt';
        }
        // define log file from lfile method or use previously set default
        $lfile = self::$log_file ? self::$log_file : $log_file_default;
        // open log file for writing only and place file pointer at the end of the file
        // (if the file does not exist, try to create it)
        self::$fp = fopen($lfile, 'a') or exit("Can't open $lfile!");
    }
}
?>
