<?php

class consultaPedido {

    //*********** Propriedades ************//

    //Geral
    private $pedidos;
    private $produtos;

    //Capa do pedido
    private $idPedido;
    private $idFilial;
    private $nomeCliente;
    private $cpfCliente;
    private $emailCliente;
    private $status;
    private $origem;
    private $dataReservada;

    //************ Métodos ***************//


    //Função principal
    public function processaPedidos($msg){

        $this->conecta();
        $this->divideStrPorPedido($msg);
        foreach($this->pedidos as $pedido){

            $pedidoCapa = $this->divideStrPorProduto($pedido);
            $pedidoCapa = $this->stringToArray($pedidoCapa);


            $this->produtosToArray();

            $this->alimentaCampos($pedidoCapa);
            if(!$this->existePedido($this->idPedido)){
                $this->inserePedido();
                $this->insereItens();
            }else{
                $this->atualizaPedido();
                $this->excluiItens();
                $this->insereItens();
            }
        }
    }

    //Conecta ao banco
    public  function conecta(){
        $banco = DB_DATABASE;
        $usuario = DB_USERNAME;
        $senha = DB_PASSWORD;
        $hostname = DB_HOSTNAME;

        $conn = mysql_connect($hostname, $usuario, $senha);
        mysql_select_db($banco) or die("Não foi possível conectar ao banco MySQL");
        if (!$conn) {
            echo "Não foi possível conectar ao banco MySQL. ";
            exit;
        }
    }

    //Separa os pedidos
    private function divideStrPorPedido($msg){
        if($_SERVER['HTTP_HOST'] == "localhost"){
            $separador = "þ";
        }else{
            $separador = chr(254);
        }
        $pedidos = explode($separador, trim($msg));
        $this->pedidos = $pedidos;
    }

    //Separa os produtos do pedido
    private function divideStrPorProduto($pedido){
        //Separa por quebra de linha
        $produtos = explode("\n", trim($pedido));
        //Retira os espaços em branco de todos os elementos
        $produtos = array_map('trim', $produtos);
        //Pega a primeira linha que é a capa do pedido
        $pedido = trim($produtos[0]);
        //retira a capa do pedido do array de produtos
        array_shift($produtos);

        $this->produtos = $produtos;
        /*echo "<pre>";
        print_r($produtos);
        echo "</pre>";*/
        return $pedido;
    }


    //Transforma a string passada em um array
    private function stringToArray($str, $separador = "|"){

        return explode($separador, $str);

    }

    private function alimentaCampos($pedido){
        $this->idFilial    = $pedido[1];
        if($pedido[2] == "S"){
            $this->origem       = "Web";
        }else{
            $this->origem = "Loja";
        }

        $this->idPedido     = $pedido[3];
        $this->nomeCliente  = $pedido[4];
        $this->cpfCliente   = str_replace(array(".", ",", "-", "/"), "", $pedido[5]);
        $this->emailCliente = $pedido[6];
        if($pedido[7] == 2){
            $this->status       = "Completo";
        }else{
            $this->status       = "Pendente";
        };
        $this->dataReservada       = $pedido[8];
    }

    private function inserePedido(){
        $sql = "INSERT INTO pedidos
                (
                id,
                idFilial,
                nomeCliente,
                cpfCliente,
                emailCliente,
                `status`,
                origem,
                dataReservada
                )
                values(
                '" . $this->idPedido . "',
                '" . $this->idFilial . "',
                '" . $this->nomeCliente . "',
                '" . $this->cpfCliente . "',
                '" . $this->emailCliente . "',
                '" . $this->status . "',
                '" . $this->origem . "',
                '" . $this->dataReservada . "'
                )
        ";
        mysql_query($sql);
        if (mysql_error()) {
            if($_SERVER["SERVER_NAME"] == "localhost"){
                echo "Função inserePedido " . mysql_error();
            }
            throw new Exception("Função inserePedido sql: " . $sql . " erro: " .mysql_error());
        }

    }

    private function insereItens(){

        //Contador de produtos (cada produto contém um array)
        $i = 0;


        //Para cada produto
        foreach($this->produtos as $produto){

            $sql = "INSERT INTO itensPedido
                    (
                    idPedido,
                    codigoErp,
                    nomeProduto,
                    corProduto,
                    tamanhoProduto,
                    qtdePedida,
                    qtdeReservada
                    )
                    VALUES(
                    '" . $this->idPedido . "',
                    '" . $produto[1] . "',
                    '" . $produto[2] . "',
                    '" . $produto[3] . "',
                    '" . $produto[4] . "',
                    '" . $produto[5] . "',
                    '" . $produto[6] . "'
                    )
            ";
            $result = mysql_query($sql);
            $i++;
            if (mysql_error()) {
                if($_SERVER["SERVER_NAME"] == "localhost"){
                    echo mysql_error();
                }
                throw new Exception("sql: " . $sql . " erro: " .mysql_error());
            }


        }
    }


    private function existePedido($idPedido){
        $sql = "SELECT id FROM pedidos WHERE id ='" . $idPedido . "'";
        $result = mysql_query($sql);
        if (mysql_error()) {
            if($_SERVER["SERVER_NAME"] == "localhost"){
                echo mysql_error();
            }
            throw new Exception("sql: " . $sql . " erro: " .mysql_error());
        }
        if(mysql_num_rows($result) >0){
            return true;
        }else{
            return false;
        }

    }

    //Pega o pedido
    public function getPedido($idPedido){
        $idPedido = str_replace(".", "", $idPedido);
        $idPedido = str_replace("-", "", $idPedido);
        $idPedido = str_replace("/", "", $idPedido);
        $sql = "SELECT * FROM pedidos
                WHERE pedidos.id  ='" . $idPedido . "' OR pedidos.cpfCliente = '" . $idPedido . "' ";
        $result = mysql_query($sql);
        if (mysql_error()) {
            if($_SERVER["SERVER_NAME"] == "localhost"){
                echo mysql_error();
            }
            throw new Exception("sql: " . $sql . " erro: " .mysql_error());
        }
        return $result;
    }

    //Pega os produtos de um pedido
    public function getItens($idPedido){
        $sql = "SELECT * FROM itensPedido
                WHERE idPedido  ='" . $idPedido . "'";
        $result = mysql_query($sql);
        if (mysql_error()) {
            if($_SERVER["SERVER_NAME"] == "localhost"){
                echo "getItens " . mysql_error();
            }
            throw new Exception("getItens sql: " . $sql . " erro: " .mysql_error());
        }
        return $result;
    }


    private function atualizaPedido(){
        $sql = "UPDATE pedidos SET
                idFilial = '" . $this->idFilial . "',
                nomeCliente = '" . $this->nomeCliente . "',
                cpfCLiente = '" . $this->cpfCliente . "',
                emailCliente = '" . $this->emailCliente ."',
                `status` = '" . $this->status . "',
                origem = '" . $this->origem . "',
                dataReservada = '" . $this->dataReservada . "'
                WHERE id = '" . $this->idPedido . "'

        ";
        mysql_query($sql);
        if (mysql_error()) {
            if($_SERVER["SERVER_NAME"] == "localhost"){
                echo "atualizaPedido " . mysql_error();
            }
            throw new Exception("atualizaPedido sql: " . $sql . " erro: " .mysql_error());
        }
    }

    private function excluiItens(){
        $sql = "DELETE FROM itensPedido WHERE idPedido ='" . $this->idPedido . "'";
        mysql_query($sql);
        if (mysql_error()) {
            if($_SERVER["SERVER_NAME"] == "localhost"){
                echo "excluiItens " . mysql_error();
            }
            throw new Exception("excluiItens sql: " . $sql . " erro: " .mysql_error());
        }

    }

    //Transforma a string de cada produto em um array
    private function  produtosToArray(){
        foreach($this->produtos as $produto){
            $produtos[] = $this->stringToArray($produto);
        }
        $this->produtos = $produtos;
        unset($produtos);
    }



}
