<?php


class pedidoModel extends ConfiguracoesWebservice
{

    //Propriedades
    public $nroPedido; //Variável de número do pedido
    public $nomeWS = "wsCapturaPedido"; //Nome do webservice
    public $conn;

    //***Métodos***//
    //Método que conecta ao BD

    public function conecta()
    {

        $banco = DB_DATABASE;
        $usuario = DB_USERNAME;
        $senha = DB_PASSWORD;
        $hostname = DB_HOSTNAME;

        $this->conn = mysqli_connect($hostname, $usuario, $senha);
        mysqli_select_db($this->conn, $banco) or die("Não foi possível conectar ao banco MySQL");
        if (!$this->conn) {
            echo "Não foi possível conectar ao banco MySQL. ";
            exit;
        }
    }

    //Captura pedidox
    public function capturaPedido()
    {
        /** @todo must change mysqli_query for $this->query() */
        $this->conecta();
        $nropedido = $this->nroPedido;

        $sql = "SELECT *, `order`.date_added AS order_date_added
            FROM `order`
            INNER JOIN customer
            ON customer.customer_id = `order`.customer_id
            WHERE `order`.order_id = {$nropedido}";

        $pedidos = mysqli_query($this->conn, $sql);
        $conteudo = "";

        if ($pedido = mysqli_fetch_assoc($pedidos)) {

            $Order_Id = $pedido["order_id"];
            $Customer_id = $pedido["customer_id"];
            $Date_Added = $pedido["order_date_added"];


            if (isset($pedido['delivery_date']) && !empty($pedido['delivery_date'])) {
                $horaInicial = explode(":", $pedido["delivery_time"]);
                $dataHoraEncomenda = $pedido['delivery_date'] . " " . $horaInicial[0] . ":01";
            } else {
                $dataHoraEncomenda = "";
            }

            $customer = mysqli_query($this->conn, "
                SELECT *
                FROM `customer`
                WHERE customer_id = {$pedido["customer_id"]}
            ");

            $customFieldPosition = Webservice::getCustomFieldPositions($this->conn);

            $Customer_Documento = "";
            $Customer_IE = "";
            $contribuinteICMS = "";
            $crt = "";
            $consumidorFinal = "";
            $Customer_Nome = "";
            $Customer_Tipo = "";
            $Empresa_Id = '';

            while ($dcust = mysqli_fetch_array($customer)) {
                $customerCustomFields = json_decode($dcust["custom_field"], true);

                /** OpenCat 2.9 fallback */
                if (!is_array($customerCustomFields)) {
                    $customerCustomFields = unserialize($dcust["custom_field"]);
                }

                $Empresa_Id = isset($dcust["company_id"]) ? $dcust["company_id"] : '';

                $Customer_Sexo = isset($customerCustomFields[$customFieldPosition["sexo"]]) ? $customerCustomFields[$customFieldPosition["sexo"]] : 'M';

                // Pessoa jurídica
                if ($customerCustomFields[$customFieldPosition['cnpj']]) {
                    $Customer_IE = isset($customerCustomFields[$customFieldPosition["inscricao_estadual"]]) ? $customerCustomFields[$customFieldPosition["inscricao_estadual"]] : '';
                    $Customer_Nome = $customerCustomFields[$customFieldPosition["razao_social"]];
                    $contribuinteICMS = $customerCustomFields[$customFieldPosition["contribuinte_icms"]];
                    $Customer_Documento = str_replace("-", "", $customerCustomFields[$customFieldPosition["cnpj"]]);
                    $crt = $customerCustomFields[$customFieldPosition["codigo_do_regime_tributario"]];
                    $consumidorFinal = $pedido['final_consumer'];
                    //Pessoa física
                } else {
                    $Customer_Nome = $pedido["firstname"] . " " . $pedido["lastname"];
                    $Customer_Documento = str_replace("-", "", $customerCustomFields[$customFieldPosition['cpf']]);
                    $contribuinteICMS = 9;
                    $crt = "";
                    $consumidorFinal = 1;
                }
            }

            # region re-set from custom fields
            $shippingCustomFields = json_decode($pedido['shipping_custom_field'], true);

            /** OpenCat 2.9 fallback */
            if (!is_array($shippingCustomFields)) {
                $shippingCustomFields = unserialize($pedido['shipping_custom_field']);
            }

            $pedido['shipping_number'] = $shippingCustomFields[$customFieldPosition['number']];
            $pedido['shipping_complement'] = $shippingCustomFields[$customFieldPosition['complement']];

            $paymentCustomFields = json_decode($pedido['payment_custom_field'], true);

            /** OpenCat 2.9 fallback */
            if (!is_array($paymentCustomFields)) {
                $paymentCustomFields = unserialize($pedido['payment_custom_field']);
            }

            $pedido['payment_number'] = $paymentCustomFields[$customFieldPosition['number']];
            $pedido['payment_complement'] = $paymentCustomFields[$customFieldPosition['complement']];
            # endregion


            $Customer_DTNasc = '1990-01-01'; //$pedido["birthday"];

            $Customer_email = $pedido["email"];
            $Customer_tel = $pedido["telephone"];
            $Customer_MesmoEndereco = (($pedido["payment_address_1"] == $pedido["shipping_address_1"]) && ($pedido["payment_address_2"] == $pedido["shipping_address_2"])) ? "S" : "N";
            $Customer_Entrega_Endereco = trim($pedido["shipping_address_1"]);
            $Customer_Entrega_Numero = $pedido["shipping_number"];
            $Customer_Entrega_Complemento = $pedido["shipping_complement"];
            $pedido['shipping_reference'] = $pedido['shipping_company'];
            $Customer_Entrega_Referencia = $pedido["shipping_reference"];
            $Customer_Entrega_Bairro = $pedido["shipping_address_2"];
            $Customer_Entrega_Cidade = $pedido["shipping_city"];
            $Customer_Entrega_CEP = $pedido["shipping_postcode"];
            $Customer_Entrega_Pais = $pedido["shipping_country"];

            $estados = mysqli_query($this->conn, "
                SELECT *
                FROM `zone`
                WHERE country_id = 30
                AND `name` = '{$pedido["shipping_zone"]}';
            ");

            if ($estados && $estado = mysqli_fetch_assoc($estados)) {
                $Customer_Entrega_Estado = $estado["code"];
            } else {
                $Customer_Entrega_Estado = $pedido["shipping_zone"];
            }

            $Customer_Cobranca_Endereco = trim($pedido["payment_address_1"]);
            $Customer_Cobranca_Numero = $pedido["payment_number"];
            $Customer_Cobranca_Complemento = $pedido["payment_complement"];
            $pedido['payment_reference'] = $pedido['payment_company'];
            $Customer_Cobranca_Referencia = $pedido["payment_reference"];
            $Customer_Cobranca_Bairro = $pedido["payment_address_2"];
            $Customer_Cobranca_Cidade = $pedido["payment_city"];
            $Customer_Cobranca_CEP = $pedido["payment_postcode"];
            $Customer_Cobranca_Pais = $pedido["payment_country"];

            $estados = mysqli_query($this->conn, "
                SELECT *
                FROM `zone`
                WHERE country_id = 30
                AND `name` = '{$pedido["payment_zone"]}';
            ");

            if ($estados && $estado = mysqli_fetch_assoc($estados)) {
                $Customer_Cobranca_Estado = $estado["code"];
            } else {
                $Customer_Cobranca_Estado = $pedido["payment_zone"];
            }

            //
            //$estados = mysqli_query($this->conn,"
            //    SELECT *
            //    FROM  `zone`
            //    WHERE country_id = 30
            //    AND `name` = '{$pedido["payment_zone"]}';
            //") or die("Erro na consulta");
            //
            //if ($estado = mysqli_fetch_assoc($estados)) {
            //    $Customer_Entrega_Estado = $estado["code"];
            //} else {
            //    $Customer_Entrega_Estado = $pedido["payment_zone"];
            //}
            //
            //$Custumer_Cobranca_Pais = $pedido["payment_country"];


            //Pagamento no cartão online
            //Verifica se o módulo da cielo é antigo pelo nome da tabela
            $sql = "SHOW TABLES FROM " . DB_DATABASE . " LIKE 'cielo'";
            $result = mysqli_query($this->conn, $sql);
            $num_rows = mysqli_num_rows($result);
            //Se for o módulo antigo

            $Forma_Pagamento_Tipo = '';
            $Forma_Pagamento_DataOperacao = '';
            $Forma_Pagamento_Operacao = '';
            $Forma_Pagamento_Bandeira = '';
            $Forma_Pagamento_TID = '';
            $Forma_Pagamento_AUTH = '';
            $Forma_Pagamento_Parcelas = '';

            $Transportadora = '';
            $ServicoTransportadora = '';
            $Valor_Frete = '';
            $Sub_total = '';
            $Total = '';

            $PrazoFrete = '';
            $tipoPedido = '';

            if ($num_rows > 0) {

                $tipopgtos = mysqli_query($this->conn, "
                    SELECT *
                    FROM  `cielo`
                    WHERE pedido = {$pedido["order_id"]};
                ");

                if (!!$tipopgtos && $cielo = mysqli_fetch_assoc($tipopgtos)) {
                    $Forma_Pagamento_Tipo = "C";
                    $Forma_Pagamento_DataOperacao = date("Y-m-d G:i:s", $cielo["date"]);
                    $Forma_Pagamento_Bandeira = $cielo["cc"];
                    $Forma_Pagamento_TID = $cielo["tid"];
                    $Forma_Pagamento_AUTH = $cielo["auth"];

                    $historys = mysqli_query($this->conn, "
                        SELECT *
                        FROM order_history
                        WHERE order_status_id = 5
                        AND order_id = {$pedido["order_id"]};
                    ");

                    $parcelas = 0;

                    if ($history = mysqli_fetch_assoc($historys)) {
                        if ($history["comment"] != "") {
                            $parcelas = str_replace($history["comment"], "Parcela(s):", "");
                        } else {
                            $parcelas = 1;
                        }
                    }

                    if ($parcelas == "") {
                        $parcelas = 1;
                    }

                    $settings = mysqli_query($this->conn,
                        "SELECT * FROM `setting` WHERE `group` = 'cielo' AND `key` = 'cielo_sem'") or die("Erro na consulta");

                    $parcelassemjuros = 0;

                    if ($setting = mysqli_fetch_assoc($settings)) {
                        $parcelassemjuros = $setting["value"];
                    }

                    $Forma_Pagamento_Operacao = "CV";

                    if ($parcelas == 1) {
                        $Forma_Pagamento_Operacao = "CV";
                    } else {
                        if ($parcelas <= $parcelassemjuros) {
                            $Forma_Pagamento_Operacao = "CPSJ";
                        } else {
                            if ($parcelas > $parcelassemjuros) {
                                $Forma_Pagamento_Operacao = "CPCJ";
                            }
                        }
                    }

                    $Forma_Pagamento_Parcelas = $parcelas;
                } else {
                    $SQLclientes = mysqli_query($this->conn,
                        "SELECT customer_group_id FROM customer WHERE customer_id = " . $Customer_id);
                    if ($datacliente = mysqli_fetch_assoc($SQLclientes)) {
                        $nomeGrupoClientes = $this->getNomeGrupoClientes($datacliente["customer_group_id"]);
                        if ($nomeGrupoClientes == "Pessoa Jurídica" || $nomeGrupoClientes == "PJ") {
                            $Forma_Pagamento_Tipo = "BC"; // BC boleto faturado (Empresa)
                            $Forma_Pagamento_Bandeira = "Faturado";
                        } else {
                            $sql = "SELECT payment_method, payment_code, date_added FROM `order` WHERE order_id = " . $pedido["order_id"];

                            $SQLclientes = mysqli_query($this->conn, $sql);
                            if ($datacliente = mysqli_fetch_array($SQLclientes)) {
                                if ($datacliente['payment_method'] == "PagSeguro" || $datacliente['payment_method'] == "Boleto Bancário" || $datacliente['payment_method'] == "Boleto Banco do Brasil") {
                                    $Forma_Pagamento_Tipo = "BV"; //Boleto online
                                } elseif (strrpos($datacliente['payment_method'], "boleto") === false) {
                                    $Forma_Pagamento_Tipo = "E"; //Pagamento na entrega
                                } else {
                                    $Forma_Pagamento_Tipo = "BV"; //Boleto online
                                }
                                $Forma_Pagamento_Bandeira = $datacliente['payment_method']; //sodexo, dinheiro, boleto, etc
                                $Forma_Pagamento_DataOperacao = date("Y-m-d G:i:s");
                                $Forma_Pagamento_Operacao = "";
                                $Forma_Pagamento_TID = "";
                                $Forma_Pagamento_AUTH = "";
                                $Forma_Pagamento_Parcelas = "1";
                            }
                        }
                    }
                }
                //Se não for o módulo antigo da cielo
            } else {

                $Forma_Pagamento_DataOperacao = date("Y-m-d G:i:s");
                $Forma_Pagamento_TID = "";
                $Forma_Pagamento_AUTH = "";
                $Forma_Pagamento_Operacao = "";
                $Forma_Pagamento_Parcelas = 1;

                //Se o pedido estiver na tabela da cielo é porque foi feito com cartão online
                $tipopgtos = mysqli_query($this->conn,
                    "SELECT * FROM `order_cielo` WHERE order_id = " . $pedido["order_id"]);
                //Se o pagamento for por cartão online
                if (!!$tipopgtos && $cielo = mysqli_fetch_assoc($tipopgtos)) {
                    $Forma_Pagamento_Tipo = "C";
                    $Forma_Pagamento_DataOperacao = date("Y-m-d G:i:s"); //date("Y-m-d G:i:s", $cielo["data"]);
                    $Forma_Pagamento_Bandeira = $cielo["ccMethod"];
                    $Forma_Pagamento_TID = $cielo["tidCielo"];
                    $parcelas = $cielo['numParcelas'];
                    $Forma_Pagamento_AUTH = ""; //$cielo["auth"];


                    $settings = mysqli_query($this->conn,
                        "SELECT * FROM `setting` WHERE `group` = 'buypagecieloloja5' AND `key` LIKE '%" . $Forma_Pagamento_Bandeira . "_sem'") or die("Erro na consulta");
                    $parcelassemjuros = 0;
                    if ($setting = mysqli_fetch_assoc($settings)) {
                        $parcelassemjuros = $setting["value"]; ///Corrigir
                    }
                    $Forma_Pagamento_Operacao = "CV";

                    if ($parcelas == 1) {
                        $Forma_Pagamento_Operacao = "CV";
                    } else {
                        if ($parcelas <= $parcelassemjuros) {
                            $Forma_Pagamento_Operacao = "CPSJ";
                        } else {
                            if ($parcelas > $parcelassemjuros) {
                                $Forma_Pagamento_Operacao = "CPCJ";
                            }
                        }
                    }
                    $Forma_Pagamento_Parcelas = $parcelas;
                    //Outras formas de pagamento
                } else {
                    $SQLclientes = mysqli_query($this->conn,
                        "SELECT customer_group_id FROM customer WHERE customer_id = " . $Customer_id);
                    if ($datacliente = mysqli_fetch_assoc($SQLclientes)) {
                        $nomeGrupoClientes = $this->getNomeGrupoClientes($datacliente["customer_group_id"]);
                        if ($nomeGrupoClientes == "Pessoa Jurídica" || $nomeGrupoClientes == "PJ") {
                            $Forma_Pagamento_Tipo = "BC"; // B Cboleto faturado (Empresa)
                            $Forma_Pagamento_Bandeira = "Faturado";
                        } else {
                            $sql = "SELECT payment_method, payment_code, date_added FROM `order` WHERE order_id = " . $pedido["order_id"];
                            $SQLclientes = mysqli_query($this->conn, $sql);
                            if ($datacliente = mysqli_fetch_assoc($SQLclientes)) {
                                if ($datacliente['payment_method'] == "PagSeguro" || $datacliente['payment_method'] == "Boleto Bancário") {
                                    $Forma_Pagamento_Tipo = "BV"; //Boleto online
                                } elseif (strrpos($datacliente['payment_method'], "boleto") === false) {
                                    $Forma_Pagamento_Tipo = "E"; //Pagamento na entrega
                                } else {
                                    $Forma_Pagamento_Tipo = "BV"; //Boleto online
                                }
                                $Forma_Pagamento_Bandeira = $datacliente['payment_method']; //sodexo, dinheiro, boleto, etc
                                $Forma_Pagamento_DataOperacao = $datacliente['date_added'];
                                $Forma_Pagamento_Operacao = "";
                                $Forma_Pagamento_TID = "";
                                $Forma_Pagamento_AUTH = "";
                                $Forma_Pagamento_Parcelas = "1";
                            }
                        }
                    }
                }


                //$Forma_Pagamento_Tipo = "BV";
                // E pagto na entrega
                // C de cartao
                // B ou BV pagamento no boleto
                //CPFJ credito parcelado sem juros
            }


            $DataAprovacao = date("Y-m-d G:i:s", strtotime($pedido["date_modified"]));
            $totals = mysqli_query($this->conn, "SELECT * FROM order_total WHERE order_id = " . $pedido["order_id"]);
            $Valor_Desconto_Nota = 0;

            while ($total = mysqli_fetch_assoc($totals)) {

                if ($total["sort_order"] == "1") {
                    $Sub_total = $total["value"];
                } else {
                    if ($total["sort_order"] == "3") {
                        $Valor_Frete = $total["value"];
                    } else {
                        if ($total["sort_order"] == "9") {
                            $Total = $total["value"];
                        } else {
                            if ($total["code"] == "coupon") {
                                $Valor_Desconto_Nota = $total["value"];
                            }
                        }
                    }
                }
            }
            $sstatus = mysqli_query($this->conn, "
                SELECT order_status.order_status_id AS status
                FROM `order`
                INNER JOIN order_history ON `order`.order_id = order_history.order_id
                INNER JOIN order_status ON order_status.order_status_id = order_history.order_status_id
                WHERE `order`.order_id = {$pedido["order_id"]};
            ");

            $STATUS = "";

            if ($sstatus && $dstatus = mysqli_fetch_assoc($sstatus)) {
                if ($dstatus["status"] == "5") {
                    $STATUS = "A";
                } else {
                    if ($dstatus["status"] == "7") {
                        $STATUS = "C";
                    } else {
                        if ($dstatus["status"] == "1") {
                            $STATUS = "PP";
                        }
                    }
                }
            }
            $settings = mysqli_query($this->conn, "
                SELECT sort_order
                FROM order_total
                WHERE sort_order NOT IN (1,9)
                AND code = 'shipping'
                AND order_id = {$pedido["order_id"]};
            ");

            $Codigo_Frete = "";
            $trocoPara = 0;

            if ($settings && $setting = mysqli_fetch_assoc($settings)) {
                $Codigo_Frete = $setting["sort_order"];
            }

            if ($Codigo_Frete != "3") {
                $Transportadora = "O próprio";
                $tipoPedido = "E"; //Encomenda
                $ServicoTransportadora = "Encomenda";

                //$tipoPedido = "D"; //Delivery
                $res = mysqli_query($this->conn, "
                    SELECT *
                    FROM `order`
                    WHERE order_id = {$pedido["order_id"]};
                ");

                while ($res && $dados = mysqli_fetch_assoc($res)) {
                    $PrazoFrete = 0;
                    $horaInicial = explode(":", $pedido["delivery_time"]);
                    $dataHoraEncomenda = $dados['delivery_date'] .' ' . $horaInicial[0] . ':01';
                }
                $Transportadora = "O proprio";

                $res = mysqli_query($this->conn, "
                    SELECT shipping_troco_para
                    FROM `order`
                    WHERE order_id = {$Order_Id};
                ");

                while ($res && $row = mysqli_fetch_assoc($res)) {
                    $trocoPara = str_replace(",", ".", $row['shipping_troco_para']);
                    //$trocoPara = number_format($trocoPara, 2, '.', '');
                    $trocoPara = str_replace(",", ".", $trocoPara);
                }
            } else {

                $totals = mysqli_query($this->conn, "
                    SELECT *
                    FROM order_total
                    WHERE order_id = {$pedido["order_id"]}
                ");

                while ($totals && $total = mysqli_fetch_assoc($totals)) {
                    if ($total["sort_order"] == "3") {

                        $arrayTransportadora = explode(".", $total["title"]);
                        $ServicoTransportadora = trim($arrayTransportadora[0]);

                        if ((strtolower($ServicoTransportadora) == "sedex") || (strtolower($ServicoTransportadora) == "pac") || (strtolower($ServicoTransportadora) == "sedex a cobrar") || (strtolower($ServicoTransportadora) == "sedex10") || (strtolower($ServicoTransportadora) == "e-sedex")) {
                            $Transportadora = "Correio";
                            $arrayPrazoFrete = explode("Entrega em", $arrayTransportadora[1]);
                            $PrazoFrete = trim($arrayPrazoFrete[1]);
                            $tipoPedido = "P";
                        } elseif ($ServicoTransportadora == "Retirar na Loja" || $ServicoTransportadora == "Taxa Fixa de Frete") {
                            $PrazoFrete = 0;
                            $Transportadora = "Frete Fixo";
                            $tipoPedido = "P"; //Pedido normal
                        } elseif ($ServicoTransportadora == "Frete Grátis") {
                            $PrazoFrete = 0;
                            $Transportadora = "Frete Fixo";
                            $tipoPedido = "P"; //Pedido normal
                            $Transportadora = "Correio";
                        } elseif ($ServicoTransportadora == "Encomenda") {
                            $tipoPedido = "E"; //Encomenda
                            //$tipoPedido = "D"; //Delivery
                            $res = mysqli_query($this->conn,
                                "SELECT * FROM `order` WHERE order_id = " . $pedido["order_id"]);
                            while ($dados = mysqli_fetch_assoc($res)) {

                                $PrazoFrete = 0;
                                $horaInicial = explode(":", $pedido["delivery_time"]);
                                $dataHoraEncomenda = $dados['delivery_date'] . " " . $horaInicial[0] . ":01";
                            }
                            $Transportadora = "O proprio";

                            $sql = "SELECT shipping_troco_para FROM `order` WHERE order_id = " . $Order_Id;
                            $res = mysqli_query($this->conn, $sql);

                            while ($row = mysqli_fetch_assoc($res)) {
                                $trocoPara = str_replace(",", ".", $row['shipping_troco_para']);
                                //$trocoPara = number_format($trocoPara, 2, '.', '');
                                $trocoPara = str_replace(",", ".", $trocoPara);
                            }

                        } else {
                            $tipoPedido = "P"; //Pedido normal
                            $PrazoFrete = 0;
                            $Transportadora = "Frete Fixo";
                        }
                    }
                }
            }

            $quebraLinha = chr(10);
            $separador = chr(124);

            $observacaoEntrega = " - " . mb_substr($pedido['comment'], 0, 400);
            $observacaoEntrega = str_replace(chr(13), "", $observacaoEntrega);
            $observacaoEntrega = str_replace(chr(10), chr(248), $observacaoEntrega);
            //Monta a string de retorno do webservice
            $conteudo = $conteudo .
                "$Order_Id" .
                $separador .
                "$Date_Added" .
                $separador .
                "$Customer_Tipo" .
                $separador .
                "$Customer_Documento" .
                $separador .
                "$Customer_Nome" .
                $separador .
                "$Customer_IE" .
                $separador .
                "$Customer_DTNasc" .
                $separador .
                "$Customer_Sexo" .
                $separador .
                "$Customer_email " .
                $separador .
                "$Customer_tel" .
                $separador .
                "$Customer_MesmoEndereco" .
                $separador .
                "$Customer_Entrega_Endereco" .
                $separador .
                "$Customer_Entrega_Numero" .
                $separador .
                "$Customer_Entrega_Complemento" .
                $separador .
                "$Customer_Entrega_Referencia" . "$observacaoEntrega" .
                $separador .
                "$Customer_Entrega_Bairro" .
                $separador .
                "$Customer_Entrega_Cidade" .
                $separador .
                "$Customer_Entrega_CEP" .
                $separador .
                "$Customer_Entrega_Estado" .
                $separador .
                "$Customer_Entrega_Pais" .
                $separador .
                "$Customer_Cobranca_Endereco" .
                $separador .
                "$Customer_Cobranca_Numero" .
                $separador .
                "$Customer_Cobranca_Complemento" .
                $separador .
                "$Customer_Cobranca_Referencia" .
                $separador .
                "$Customer_Cobranca_Bairro" .
                $separador .
                "$Customer_Cobranca_Cidade" .
                $separador .
                "$Customer_Cobranca_CEP" .
                $separador .
                "$Customer_Cobranca_Estado" .
                $separador .
                "$Customer_Cobranca_Pais" .
                $separador .
                "$Forma_Pagamento_Tipo" .
                $separador .
                "$Forma_Pagamento_DataOperacao" .
                $separador .
                "$Forma_Pagamento_Operacao" .
                $separador .
                "$Forma_Pagamento_Bandeira" .
                $separador .
                "$Forma_Pagamento_TID" .
                $separador .
                "$Forma_Pagamento_AUTH" .
                $separador .
                "$Forma_Pagamento_Parcelas" .
                $separador .
                $Codigo_Frete .
                $separador .
                "$Transportadora" .
                $separador .
                "$ServicoTransportadora" .
                $separador .
                "$Valor_Frete" .
                $separador .
                "$Valor_Desconto_Nota" .
                $separador .
                "$Sub_total" .
                $separador .
                "$Total" .
                $separador .
                "$STATUS" .
                $separador .
                "$DataAprovacao" .
                $separador .
                "$PrazoFrete" .
                $separador .
                "$Customer_id" .
                $separador .
                $Empresa_Id .
                $separador .
                "$tipoPedido" .
                $separador .
                "$dataHoraEncomenda" .
                $separador .
                "$trocoPara" .
                $separador .
                "$contribuinteICMS" .
                $separador .
                "$crt" .
                $separador .
                "$consumidorFinal" .
                $quebraLinha;


            // tipoPedido
            // P - pedido normal
            // D - delivery - entrega imediata - horário dentro do horário de entrega
            // E - encomenda -
            // gravar hora da última conexão para comparar se a internet caiu (infelizmente estamos fora do ar)
            // Não deixar vender quanto o gap de horário da ultima conexão ultrapassou o limite bloquei novas vendas
            // Se existe pedido do tipo delivery e o prazo da conexão expirou - cancelar o pedido se der problema
            // Encomenda - pedido normal que tem data de entrega fixa.
            //DataHoraEncomenda
            // - Só alimenta quando for encomenda
            // Formato data Y-M-D-G-I-S
            //TrocoPara
            //Delivery paga depois

            $Codigo_de_Produto = "";
            //Pega todos os produtos de um pedido
            $sql = "SELECT * FROM order_product WHERE order_id = '" . $pedido["order_id"] . "'";
            $produtos_pedido = mysqli_query($this->conn, $sql);

            $numeroDeProdutos = mysqli_num_rows($produtos_pedido);
            $i = $numeroDeProdutos;

            while ($produto = mysqli_fetch_assoc($produtos_pedido)) {
                $i--;
                $order_product_id = $produto['order_product_id'];
                $sql = "SELECT * FROM order_option WHERE order_id = '" . $pedido["order_id"] . "' AND order_product_id = '" . $order_product_id . "'  ORDER BY name";
                $queryOptions = mysqli_query($this->conn, $sql);
                $optionValues = array();
                $num_rows = mysqli_num_rows($queryOptions);

                if ($num_rows > 0) {

                    while ($option = mysqli_fetch_assoc($queryOptions)) {
                        $optionValues[] = $option;
                    }

                    $sql = "SELECT sku
                            FROM product_option_variant
                            WHERE product_option_variant_id = (
                                SELECT DISTINCT tabela.product_option_variant_id
                                FROM product_option_variant_value AS tabela
                                JOIN (
                                    SELECT product_option_value_id,product_option_variant_id
                                    FROM product_option_variant_value
                                    WHERE product_option_value_id = {$optionValues[0]['product_option_value_id']}
                                ) AS oo1
                                JOIN (
                                    SELECT product_option_value_id, product_option_variant_id
                                    FROM product_option_variant_value
                                    WHERE product_option_value_id = {$optionValues[1]['product_option_value_id']}
                                ) AS oo2
                                WHERE tabela.product_option_variant_id = oo1.product_option_variant_id
                                AND oo2.product_option_variant_id = oo1.product_option_variant_id
                                AND tabela.product_option_variant_id != 0
                            );";


                    $querySku = mysqli_query($this->conn, $sql);
                    $num_rows2 = mysqli_num_rows($querySku);
                    if ($num_rows2 > 0) {
                        while ($sku = mysqli_fetch_array($querySku)) {
                            $Codigo_de_Produto = $sku['sku'];
                        }
                    }


                    //$Codigo_de_Produto = $produto["product_id"];
                    $Qtde = $produto["quantity"];
                    $Preco_Unitario = $produto["price"];
                    $Total = $produto["total"];
                } else {

                    $sql = "SELECT sku FROM product WHERE product_id = '" . $produto['product_id'] . "'";
                    $querySku = mysqli_query($this->conn, $sql);
                    $num_rows2 = mysqli_num_rows($querySku);
                    if ($num_rows2 > 0) {
                        while ($sku = mysqli_fetch_array($querySku)) {
                            $Codigo_de_Produto = $sku['sku'];
                        }

                        $Qtde = $produto["quantity"];
                        $Preco_Unitario = $produto["price"];
                        $Total = $produto["total"];

                    } else {

                        $Codigo_de_Produto = "";
                        $Qtde = "";
                        $Preco_Unitario = "";
                        $Total = "";

                    }


                }
                $conteudo = $conteudo .
                    "$Codigo_de_Produto" .
                    $separador .
                    "$Qtde" .
                    $separador .
                    "$Preco_Unitario" .
                    $separador .
                    "$Total";


                if ($i > 0) {
                    $conteudo .= $separador . $quebraLinha;
                }

                //$conteudo = $conteudo ."$Codigo_de_Produto;$Qtde;$Preço_Unitario;$Total;\r\n";
            }


            //if ($_SERVER['SERVER_NAME'] == "localhost") {
            //    echo "<pre>";
            //    echo $conteudo;
            //    echo "</pre>";
            //}
            return $conteudo;
        }
    }


    public function getNomeGrupoClientes($customer_group_id)
    {
        $sql = "SELECT name FROM customer_group_description WHERE customer_group_id = '" . $customer_group_id . "' LIMIT 1";
        $query = mysqli_query($this->conn, $sql);
        while ($row = mysqli_fetch_array($query)) {
            return $row['name'];
        }
    }


}