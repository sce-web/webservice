<?php

class Categoria extends CategoriaHelp
{
    const STATUS_ATIVA = 1;
    const STATUS_INATIVA = 0;

    private function carregarHelp()
    {

        //Categoria Ativa
        if ($this->statusCategoria == "S") {
            if (empty($this->idCategoriaOpenCart)) {
                $this->insereCategoria();
            } else {
                $this->atualizarCategoria();
            }
        } else {
            if ($this->statusCategoria == "N") {
                //Categoria Inativa
                if (isset($this->idCategoriaOpenCart)) { //Se existe no DB
                    //Desabilitar a categoria
                    $this->atualizarCategoria();
                }

            }
        }

    }

    public function carregar()
    {

        //Controla a abertura de uma transação
        $bIniciouTrans = false;
        try {

            $this->conn = Connection::open();

            $token = md5(Token::value($this->conn));

            //begin trans
            $this->conn->autocommit(false);
            $bIniciouTrans = true;

            $categoria = explode("\n", $this->msg);
            $max = count($categoria);

            for ($i = 0; $i < $max; $i++) {
                try {
                    $campos = explode(";", $categoria[$i]);
                    $this->alimentaCampos($campos);
                    $this->carregarHelp();
                } catch (Exception $exc) {
                    throw new Exception("Ocorreu um erro na categoria codigo : " . $this->idCategoriaERP . " -> " . $exc->getMessage());
                }
            }
            if ($bIniciouTrans) {
                $this->conn->commit();
            }


            $this->status = 'true';
            $this->retorno = ""; //Sucesso! quantidade inserido ".$this->qtdInserido." qtdAtualizado: ".$this->qtdAtualizado;
            $this->erro = "";

            Connection::close();
        } catch (Exception $exc) {
            //Desfaz transação
            if ($bIniciouTrans) {
                $this->conn->rollback();
            }

            $this->status = "false";
            $this->retorno = "";
            $this->erro = "" . $exc->getMessage() . "\n file: " . $exc->getFile() . " \n line: " . $exc->getLine();
        }

    }

    /**
     *
     */
}