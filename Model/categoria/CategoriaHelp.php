<?php
abstract class CategoriaHelp extends ConfiguracoesWebservice {
    #metodos set

    public $nomeWS = "wsCarregaCatregoria"; //Nome do webservice
    protected $language_id;

    #campos do campo msg
    protected $idCategoriaERP;
    protected $nomeCategoria;
    protected $codigoCategoriaPai;
    protected $ordemCategoria;
    protected $statusCategoria;

    #pego do banco
    protected $idCategoriaOpenCart;
    protected $codigoCategoriaPaiOpenCart;

    protected function trazerChaveDB() {
        $sql = "SELECT value FROM setting WHERE `key`='config_encryption'";
        #verificar se precisa de store_id=0;
        $res = $this->conn->query($sql);
        if ($this->conn->error) {
            throw new Exception("sql: " . $sql . " erro: " . $this->conn->error);
        }
        if ($data = $res->fetch_object()) {
            return $data->value;
        }
    }

    protected function alimentaCampos($campos) {
        $this->language_id = Language::value($this->conn);
        $this->idCategoriaERP = trim(isset($campos["0"]) ? $campos["0"] : "");
        $this->nomeCategoria = trim(isset($campos["1"]) ? $campos["1"] : "");
        $this->codigoCategoriaPai = trim(isset($campos["5"]) ? $campos["5"] : "");
        $this->ordemCategoria = trim(isset($campos["6"]) ? $campos["6"] : "");
        $this->statusCategoria = trim(isset($campos["7"]) ? $campos["7"] : "");

        $this->idCategoriaOpenCart = $this->getIdCategoriaOpenCart();
        $this->codigoCategoriaPaiOpenCart = $this->getCodigoCategoriaPaiOpenCart();

        if (empty($this->idCategoriaERP)) {
            throw new Exception("É necessário fornecer o código da categoria");
        }

        if ($this->statusCategoria != "S" and $this->statusCategoria != "N") {
            throw new Exception("É necessário fornecer o status da categoria como S ou N (valor fornecido = " . $this->statusCategoria . ")");
        }

        if ($this->statusCategoria == "S") {
            //Validar regras de execução
            if (empty($this->nomeCategoria)) {
                throw new Exception("É necessário fornecer o nome da categoria");
            }

            //Verificar se encontrou a Cat PAI no DB
            if (!empty($this->codigoCategoriaPai) and empty($this->codigoCategoriaPaiOpenCart)) {
                throw new Exception("Não foi possível encontrar a Categoria PAI ID = " . $this->codigoCategoriaPai);
            }
        }

    }

    protected function getIdCategoriaOpenCart() {
        $res = $this->query("
            SELECT category_id
            FROM category
            WHERE category_alternate_id = '{$this->idCategoriaERP}'");
        while($row = $res->fetch_assoc()){
            return (int)$row['category_id'];
        }
    }

    /**
     * Se statusCategoria for igual a S, então seleciono idCategoriaPai para inserir a nova categoria
     */
    protected function getCodigoCategoriaPaiOpenCart() {

        $sql = "SELECT category_id from category WHERE category_alternate_id = '" . $this->codigoCategoriaPai . "'";
        $res = $this->query($sql);
        while($row = $res->fetch_assoc()){
            return $row['category_id'];
        }

    }

    protected function atualizarCategoria() {

        $status = ($this->statusCategoria == "S") ? 1 : 0;

        $sql = "UPDATE category SET `status`  = " . $status . " WHERE category_id = '" . $this->idCategoriaOpenCart . "'";
        $this->conn->query($sql);

        if ($this->conn->error) {
            throw new Exception("sql: " . $sql . " erro: " . $this->conn->error);
        } else if (($status == 1) and ($this->sincronizar == "S")) {
            $this->gravaDescricaoCategoria();
            $this->gravarToStore();
        }
    }

    protected function deletaCategoria() {
        //Deleta a categoria de todas as tabelas
        $this->query("DELETE FROM `category` WHERE `category_id` = {$this->idCategoriaOpenCart};");
        $this->query("DELETE FROM `category_description` WHERE `category_id` = {$this->idCategoriaOpenCart};");
        $this->query("DELETE FROM `category_filter` WHERE `category_id` = {$this->idCategoriaOpenCart};");
        $this->query("DELETE FROM `category_path` WHERE `category_id` = {$this->idCategoriaOpenCart};");
        $this->query("DELETE FROM `category_to_layout` WHERE `category_id` = {$this->idCategoriaOpenCart};");
        $this->query("DELETE FROM `category_to_store` WHERE `category_id` = {$this->idCategoriaOpenCart};");
    }

    protected function gravaDescricaoCategoria()
    {
        $query = $this->query(
            "SELECT category_id
            FROM category_description
            WHERE category_id = {$this->idCategoriaOpenCart};"
        );

        if ($data = $query->fetch_object()) {

            $this->query("
                UPDATE category_description
                SET `name` = '{$this->nomeCategoria}', meta_title = '{$this->nomeCategoria}'
                WHERE category_id = {$this->idCategoriaOpenCart}
                AND language_id = {$this->language_id};
            ");

        } else {

            $this->query(
                "INSERT INTO category_description(
                    category_id,
                    language_id,
                    `name`,
                    description,
                    meta_title,
                    meta_description,
                    meta_keyword
                ) VALUES (
                    {$this->idCategoriaOpenCart},
                    {$this->language_id},
                    '{$this->nomeCategoria}',
                    '{$this->nomeCategoria}',
                    '{$this->nomeCategoria}',
                    '{$this->nomeCategoria}',
                    '{$this->nomeCategoria}'
                );"
            );

        }
    }

    protected function gravarToStore() {
        $this->query("
            INSERT INTO category_to_store (
                category_id,
                store_id
            ) VALUES (
              {$this->idCategoriaOpenCart},
              0
            ) ON DUPLICATE KEY UPDATE store_id = store_id;
        ");
    }

    protected function insereCategoria() {
        $this->codigoCategoriaPaiOpenCart = trim($this->codigoCategoriaPaiOpenCart);
        $codigoPAI = !empty($this->codigoCategoriaPaiOpenCart) ? $this->codigoCategoriaPaiOpenCart : 0;

        try {
            $this->query(
                "INSERT INTO category (
                    category_alternate_id,
                    parent_id,
                    `top`,
                    `column`,
                    sort_order,
                    `status`,
                    date_added
                ) VALUES  (
                    '{$this->idCategoriaERP}',
                    {$codigoPAI},
                    1,
                    1,
                    {$this->ordemCategoria},
                    1 ,
                    NOW()
                );"
            );
        }catch (Exception $exc) {
            throw new Exception("Ocorreu um erro ao inserir a categoria " . $this->idCategoriaERP . " -> " . $exc->getMessage());
        }

        $this->idCategoriaOpenCart = $this->conn->insert_id;

        $this->gravaDescricaoCategoria();
        $this->gravarToStore();
        $this->gravarPath();

    }

    //A tabela path é tabela que grava a URL da categoria
    //A tabela path é tabela que grava a URL da categoria
    protected function gravarPath() {

        $category_id = $this->idCategoriaOpenCart;
        $level = 0;
        if((int)$this->codigoCategoriaPaiOpenCart != 0){

            $categoriaPai = $this->getCodigoCategoriaPaiOpenCart();
            //Se existir a categoria pai
            if(!empty($categoriaPai)) {
                $categoriaPai = (int)$categoriaPai;
                //Insere um registro na tabela com a categoria pai como path
                $this->query("INSERT INTO category_path (category_id, path_id, level) VALUES ({$category_id}, '{$categoriaPai}', {$level});");
                $level++;
                $this->query("INSERT INTO category_path (category_id, path_id, level) VALUES ({$category_id}, {$category_id}, {$level});");
            } else {
                $this->query("INSERT INTO category_path (category_id, path_id, level) VALUES ({$category_id}, {$category_id}, {$level});");
            }

        }else{
            $this->query("INSERT INTO category_path (category_id, path_id, level) VALUES ({$category_id}, {$category_id}, {$level});");
        }

        /* $category_id = $this->idCategoriaOpenCart;

        $res = $this->query("SELECT * FROM category WHERE parent_id <> 0;");


        //$numRows = mysql_num_rows($res);
        //var_dump($category_id);

        if($res->fetch_assoc()){
            $level = 1;

        }*/

    }
}

?>
