<?php
class Produto extends ProdutoHelp
{

    public function carregar()
    {
        //Controla a abertura de uma transação
        $bIniciouTrans = false;
        try {

            //begin trans
            $this->conn->autocommit(false);
            $bIniciouTrans = true;

            $separador = chr(254);

            $produtos = explode($separador, $this->msg); //para rodar no automagazine
            $max = count($produtos);

            for ($i = 0; $i < $max; $i++) {

                try {

                    $campos = explode(chr(124), $produtos[$i]);
                    $campos_size = sizeof($campos);

                    for ($j = 0; $j < $campos_size; $j++) {
                        $campos[$j] = $this->escape($campos[$j]);
                    }

                    $this->alimentaCampos($campos);
                    $this->carregarHelp();

                } catch (Exception $exc) {
                    throw new Exception("Ocorreu um erro com o produto código : " . $this->codigoProdutoERP . " ->" . $exc->getMessage() . "\nTRACE: " . $exc->getTraceAsString());
                }
            }

            if ($bIniciouTrans) {
                $this->conn->commit();
            }

            $this->status = "true";
            $this->retorno = ""; //Sucesso!
            $this->erro = "";

            Connection::close();
        } catch (Exception $exc) {
            //Desfaz transação
            if ($bIniciouTrans) {
                $this->conn->rollback();
            }

            $this->status = "false";
            $this->retorno = "";
            $this->erro = $exc->getMessage() . "\n file: " . $exc->getFile() . " \n line: " . $exc->getLine();
        }
    }

    private function carregarHelp()
    {

        if ($this->habilitado == "S") {//Se produto Ativo
            //Tem fabricante pois é opcional
            if (!empty($this->nomeFabricanteERP)) {
                //Se não encontrou o Fabricante Cadastrar
                if (empty($this->idFabricanteOpenCart)) {
                    $this->insereFabricante();
                } else {
                    $this->atualizarFabricante(); //Atualizar Alterações Case Sensitive
                }
            }
        }
        //Se produto comum
        if (empty($this->codigoProdutoPaiERP)) {
            //Se não encontrado no DB
            if (empty($this->codigoProdutoOpenCart)) {
                $this->insereProduto();
            } else {
                $this->atualizarProduto();
            }
        } else { // Se produto GRADE (Cor e Tamanho)
            //Se for produto pai
            if (empty($this->codigoProdutoOpenCartPai)) {
                //Insere o produto pai
                $this->insereProdutoGrade();
            } else {
                //Atualiza o produto pai
                $this->atualizarProdutoGrade();

            }
        }

    }

    public function escape($string)
    {
        return addslashes($string);
    }
}