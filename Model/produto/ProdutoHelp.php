<?php

/**
 * Class ProdutoHelp
 * Classe de ajuda na importação dos produtos do Webservice
 *
 * Nova documentação realizada em 07/10/2015
 *
 * É NECESSÁRIO QUE TODA E QUALQUER MUDANÇA SEJA DOCUMENTADA
 * E A VERSÃO DE ATUALIZAÇÃO SEJA ALTERADA DE ACORDO COM O TIPO DE ALTERAÇÃO
 * Nós utlizamos o versionamento semântico: http://semver.org/v2.0.0
 *
 */
abstract class ProdutoHelp extends ConfiguracoesWebservice
{
    /**
     * Nome do webservice
     * @var string
     */
    public static $nomeWS = "wsCarregaProduto"; //Nome do webservice

    /**
     * Possível possibilidades para nome de fotos
     *
     * Nome da foto contruido como: $codigoBarras + [a-f]|''
     *
     * @var array
     */
    public static $arrayFotos = array("", "a", "b", "c", "d", "e", "f");
    # NÃO APAGAR AS ASPAS VAZIAS - Array com o número de fotos possíveis por produto

    /**
     * Posição 0 do Vetor de Entrada
     *
     * Código do ERP do produto (um ID com possiblidade de letras)
     * * Não possui tamanho máximo de caractéres
     * * Não possui tamanho mínimo de caractéres
     * @var string
     */
    protected $codigoProdutoERP;

    /**
     * Posição 1 do Vetor de Entrada
     *
     * Código do ERP do produto pai (um ID com possibilidade de letras)
     *
     * * Apenas aplicável se existem produtos de grade
     * * Somente virá preenchido se este for filho de um produto de grade
     * * Não possui tamanho máximo de caractéres
     * * Não possui tamanho mínimo de caractéres
     * @var string
     */
    protected $codigoProdutoPaiERP;

    /**
     * Posição 2 do Vetor de Entrada
     *
     * Código do ERP da(s) categoria(s) do produto (um ID com possibilidade de letras)
     * * Não possui tamanho máximo de caractéres
     * * Não possui tamanho mínimo de caractéres
     * * Cada ID de uma categoria é separado por uma vírgula [\,]
     * @var string
     */
    protected $codigoCategoriaERP;

    /**
     * Posição 3 do Vetor de Entrada
     *
     * Código de Barras do produto (Como um ID único, geralmente númerico, mas não necessariamente)
     * * Possui um tamanho mínimo de ? caractéres
     * * Possui um tamanho máximo de ? caractéres
     * @var string
     */
    protected $codigoBarras;

    /**
     * Código de Barras do produto pai (Como um ID único, geralmente númerico, mas não necessariamente)
     * * Apenas aplicável se existem produtos de grade
     * * Possui um tamanho mínimo de ? caractéres
     * * Possui um tamanho máximo de ? caractéres
     * @var string
     */
    protected $codigoBarrasPai = null;

    /**
     * Posição 4 do Vetor de Entrada
     *
     * Nome do produto
     * * Não pode ser nulo ou vazio
     * * Possui um tamanho mínimo de ? caractéres
     * * Possui um tamanho máximo de ? caractéres
     * @var string
     */
    protected $nomeProduto;

    /**
     * Posição 5 do Vetor de Entrada
     *
     * Descrição do produto
     * * Não possui um tamanho mínimo de caractéres
     * * Não possui um tamanho máximo de caractéres
     * @var string
     */
    protected $descricaoProduto;

    /**
     * Posição 6 do Vetor de Entrada
     *
     * Quantidade de decimais da unidade de estoque do produto
     * * Em relação a tipo de unidade de medida do produto
     * * Possíveis valores são: [0-3]
     * @var integer
     */
    protected $quantidadeDecimal;

    /**
     * Posição 7 do Vetor de Entrada
     *
     * Cor do produto
     * * Apenas aplicável se existirem produtos de grade
     * * Não pode ser nulo ou vazio caso seja produto de grade
     * * Não possui tamanho máximo de caractéres
     * @var string
     */
    protected $corGrade;

    /**
     * Posição 8 do Vetor de Entrada
     *
     * Tamanho do produto
     * * Apenas aplicável se existirem produtos de grade
     * * Não pode ser nulo ou vazio caso seja produto de grade
     * * Não possui valor máximo
     * @var string
     */
    protected $tamanhoGrade;

    /**
     * Posição 9 do Vetor de Entrada
     *
     * Nome do fabricante do produto no ERP
     * * Não possui tamanho mínimo de caractéres
     * * Não possui tamanho máximo de caractéres
     * @var string
     */
    protected $nomeFabricanteERP;

    /**
     * Posição 10 do Vetor de Entrada
     *
     * Abreviação da unidade de medida do produto
     * * Não possui tamanho mínimo de caractéres
     * * Não possui tamanho máximo de caractéres
     * @var string
     */
    protected $unidadeMedida;

    /**
     * Posição 11 do Vetor de Entrada
     *
     * Primeiro classificador do produto
     *
     * * Atualmente esse valor está apenas vinculado a o Autor,
     * mas possui o objetivo de ser um classificador genérico no futuro
     *
     * * Não possui tamanho mínimo de caractéres
     * * Não possui tamanho máximo de caractéres
     * @var string
     */
    protected $classificador1;

    /**
     * Posição 12 do Vetor de Entrada
     *
     * Segundo classificador do produto
     *
     * * Atualmente esse valor está apenas vinculado a Edição,
     * mas possui o objetivo de ser um classificador genérico no futuro
     *
     * * Não possui tamanho mínimo de caractéres
     * * Não possui tamanho máximo de caractéres
     * @var string
     */
    protected $classificador2;

    /**
     * Posição 13 do Vetor de Entrada
     *
     * Terceiro classificador do produto
     *
     * * Atualmente esse valor está apenas vinculado a o Número de páginas,
     * mas possui o objetivo de ser um classificador genérico no futuro
     *
     * * Não possui tamanho mínimo de caractéres
     * * Não possui tamanho máximo de caractéres
     * @var string
     */
    protected $classificador3;

    /**
     * Posição 14 do Vetor de Entrada
     *
     * Peso bruto, em kilogramas, do produto
     * * Necessário para o cálculo correto do frete
     * * Não pode ser nulo ou vazio
     * * Possui um valor mínimo de 0.001 (?)
     * * Não possui um valor máximo
     * @var float
     */
    protected $pesoBrutoKG;

    /**
     * Posição 15 do Vetor de Entrada
     *
     * Altura, em centimetros, do produto
     * * Necessário para o cálculo correto do frete
     * * Não pode ser nulo ou vazio
     * * Possui um valor mínimo de 0.001 (?)
     * * Não possui um valor máximo
     * @var float
     */
    protected $alturaCM;

    /**
     * Posição 16 do Vetor de Entrada
     *
     * Largura, em centimetros, do produto
     * * Necessário para o cálculo correto do frete
     * * Não pode ser nulo ou vazio
     * * Possui um valor mínimo de 0.001 (?)
     * * Não possui um valor máximo
     * @var float
     */
    protected $larguraCM;

    /**
     * Posição 17 do Vetor de Entrada
     *
     * Comprimento, em centimetros, do produto
     * * Necessário para o cálculo correto do frete
     * * Não pode ser nulo ou vazio
     * * Possui um valor mínimo de 0.001 (?)
     * * Não possui um valor máximo
     * @var float
     */
    protected $comprimentoCM;

    /**
     * Posição 19 do Vetor de Entrada
     *
     * Quantidade mínima do produto em estoque para a venda
     * * Alguma ação será tomada de acordo com a configuração da $quantidadeMinimaOpcao
     * * Apenas se a confirmação de estoque estiver ativa
     * * Não possui valor mínimo
     * * Não possui valor máximo
     * @var float
     */
    protected $quantidadeMinimaVenda;

    /**
     * Posição 13 do Vetor de Entrada
     *
     * Possíveis opções de configuração do produto para quando o estoque estiver ao valor mínimo
     * * Possíveis valores são: CE|ND
     *     * ND: Produto Indisponível
     *     * CE: Confirma Estoque pelo Lojista
     * @var string
     */
    protected $quantidadeMinimaOpcao;

    /**
     * Posição 21 do Vetor de Entrada
     *
     * Si1tuação do estoque do produto
     * * Não pode estar nulo
     * * Pode estar negativo
     * @var float
     */
    protected $estoque;

    /**
     * Posição 22 do Vetor de Entrada
     *
     * Preço de venda do produto
     * * Não pode estar nulo
     * * Aceita no máximo até duas casas decimais de precisão
     * @var float
     */
    protected $precoVenda;

    /**
     * Posição 23 do Vetor de Entrada
     *
     * Contém a verificação se o produto está habilitado para estar na loja ou não
     * * Não pode estar nulo ou vazio
     * * Deve ser convertido para booleano (S = true, N = false)
     * * Possíveis valores são: S|N
     * @var string
     */
    protected $habilitado;

    # Versão v02

    #Codigo da COR no ERP
    /**
     * Posição 24 do Vetor de Entrada
     *
     * Código de Cor no ERP
     * @var string
     */
    protected $codCorERP;
    /**
     * Posição 25 do Vetor de Entrada
     * @var string
     */
    protected $refCorERP;

    #Codigo do Tamanho no ERP
    /**
     * Posição 26 do Vetor de Entrada
     *
     * Código de Tamanho no ERP
     * @var string
     */
    protected $codTamERP;

    /**
     * Posição 27 do Vetor de Entrada
     * @var string
     */
    protected $refTamERP;

    #Novos Atributos
    /**
     * Posição 28 do Vetor de Entrada
     * @var string
     */
    protected $tipoProdutoId;

    /**
     * Posição 29 do Vetor de Entrada
     * @var string
     */
    protected $descTipo;

    /**
     * Posição 30 do Vetor de Entrada
     * @var string
     */
    protected $descClassificador1;

    /**
     * Posição 31 do Vetor de Entrada
     * @var string
     */
    protected $descClassificador2;

    /**
     * Posição 32 do Vetor de Entrada
     * @var string
     */
    protected $descClassificador3;

    #pego do banco
    /**
     * Array com os ids das categorias no OpenCart
     * @var array
     */
    protected $codigoCategoriaOpenCart;

    /**
     * Id da última categoria no OpenCart
     * @var int
     */
    protected $codigoUltimaCategoriaOpenCart;

    /**
     * Id do produto no OpenCart
     * @var int
     */
    protected $codigoProdutoOpenCart;

    /**
     * Id do produto Pai no OpenCart
     * @var int
     */
    protected $codigoProdutoOpenCartPai;

    /**
     * Id do fabricante do produto no OpenCart
     * @var int
     */
    protected $idFabricanteOpenCart;

    /**
     * Id da 'option' de cor no OpenCart
     * @var int
     */
    protected $idCorOpenCart;

    /**
     * Id da 'option' de tamanho no OpenCart
     * @var int
     */
    protected $idTamOpenCart;

    /**
     * Id da 'option' de cor no OpenCart em relação ao produto
     * @var int
     */
    protected $idCorProductOptionOpenCart;

    /**
     * Id da 'option' de tamanho no OpenCart em relação ao produto
     * @var int
     */
    protected $idTamProductOptionOpenCart;

    /**
     * Id da 'option' de valor da cor no OpenCart em relação ao produto
     * @var int
     */
    protected $product_option_value_id_cor;

    /**
     * Id da 'option' de valor da tamanho no OpenCart em relação ao produto
     * @var int
     */
    protected $product_option_value_id_tam;

    /**
     * Deprecated properties
     */

    /**
     * Posição 18 do Vetor de Entrada
     *
     * Contém a verificação se é para confirmar o estoque ou não
     * * Deve ser convertido para booleano (S = true, N = false)
     * * Possíveis valores são: S|N
     * @var string
     * @deprecated Em tese, não é mais utilizado
     */
    protected $confirmaEstoqueSN;

    /**
     * Contém a verificação se o webservice deve ou não ignorar a verificação se
     * o produto está habilitado ou não
     * Possíveis valores são: S|N
     * @var string
     * @deprecated Por ter sido transferido para uma configuração do webservice no banco de dados
     */
    protected $sempreHabilitado;

    /* Methods */

    /**
     * Retorna o texto de retorno para o XML
     * @return string
     */
    public function getRetorno()
    {
        return $this->retorno;
    }

    /**
     * Retorna o status de retorno para o XML
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Retorna o texto de erro desencodado de utf8 de retorno para o XML
     * @return string
     */
    public function getErro()
    {
        return utf8_decode($this->erro);
    }

    /**
     * Desabilita o produto durante o processo por algum motivo
     * E isso é salvo no log de erro
     *
     * @param string $reason
     */
    protected function logError($reason)
    {
        Logging::lwrite($reason);
        Logging::lclose();
    }

    /**
     * Alimenta todos as propriedades do objeto para manipulação e inserção no banco de dados
     *
     * @param array $campos Com todos os campos vindos da mensagem de entrada do webservice
     * @throws Exception Em caso de erro nas validações ou nas querys internas
     */
    protected function alimentaCampos($campos)
    {
        try {
            $this->language_id = Language::value($this->conn);
            $this->codigoProdutoERP = trim(isset($campos["0"]) ? $campos["0"] : "");
            $this->codigoProdutoPaiERP = trim(isset($campos["1"]) ? $campos["1"] : "");
            $this->codigoCategoriaERP = trim(isset($campos["2"]) ? $campos["2"] : "");
            $this->codigoBarras = trim(isset($campos["3"]) ? $campos["3"] : "");
            $this->nomeProduto = trim(isset($campos["4"]) ? $campos["4"] : "");
            $this->nomeProduto = str_replace("'", chr(34), $this->nomeProduto);
            $this->descricaoProduto = trim(isset($campos["5"]) ? $campos["5"] : "");
            $this->descricaoProduto = str_replace("'", chr(34), $this->descricaoProduto);
            $this->quantidadeDecimal = trim(isset($campos["6"]) ? (int)$campos["6"] : 0);
            $this->corGrade = trim(isset($campos["7"]) ? $campos["7"] : "");
            $this->tamanhoGrade = trim(isset($campos["8"]) ? $campos["8"] : "");
            $this->nomeFabricanteERP = trim(isset($campos["9"]) ? $campos["9"] : "");
            $this->unidadeMedida = trim(isset($campos["10"]) ? $campos["10"] : "");
            $this->classificador1 = trim(isset($campos["11"]) ? $campos["11"] : "");
            $this->classificador2 = trim(isset($campos["12"]) ? $campos["12"] : "");
            $this->classificador3 = trim(isset($campos["13"]) ? $campos["13"] : "");

            $pesoBrutoKG = trim(isset($campos["14"]) ? $campos["14"] : "");
            $this->pesoBrutoKG = (float)str_replace(",", ".", $pesoBrutoKG);

            $alturaCM = trim(isset($campos["15"]) ? $campos["15"] : "");
            $this->alturaCM = (float)str_replace(',', '.', $alturaCM);

            $larguraCM = trim(isset($campos["16"]) ? $campos["16"] : "");
            $this->larguraCM = (float)str_replace(",", ".", $larguraCM);

            $comprimentoCM = trim(isset($campos["17"]) ? $campos["17"] : "");
            $this->comprimentoCM = (float)str_replace(",", ".", $comprimentoCM);

            $this->confirmaEstoqueSN = trim(isset($campos["18"]) ? $campos["18"] : "");
            $this->quantidadeMinimaVenda = trim(isset($campos["19"]) ? (float)$campos["19"] : 0);
            $this->quantidadeMinimaOpcao = trim(isset($campos["20"]) ? $campos["20"] : "");
            $this->estoque = trim(isset($campos["21"]) ? (float)$campos["21"] : 0);

            $precoVenda = trim(isset($campos["22"]) ? $campos["22"] : "");
            $this->precoVenda = (float)str_replace(",", ".", $precoVenda);

            if ($this->alteraPreco($this->precoVenda)) {
                $this->precoVenda = $this->alteraPreco($this->precoVenda);
            }

            $this->habilitado = trim(isset($campos["23"]) ? $campos["23"] : "");
            $this->codCorERP = trim(isset($campos["24"]) ? $campos["24"] : "");
            $this->refCorERP = trim(isset($campos["25"]) ? $campos["25"] : "");
            $this->codTamERP = trim(isset($campos["26"]) ? $campos["26"] : "");
            $this->refTamERP = trim(isset($campos["27"]) ? $campos["27"] : "");
            $this->tipoProdutoId = trim(isset($campos["28"]) ? $campos["28"] : "");
            $this->descTipo = trim(isset($campos["29"]) ? $campos["29"] : "");
            $this->descClassificador1 = trim(isset($campos["30"]) ? $campos["30"] : "");
            $this->descClassificador2 = trim(isset($campos["31"]) ? $campos["31"] : "");
            $this->descClassificador3 = trim(isset($campos["32"]) ? $campos["32"] : "");

            //Propetries do DB
            $this->codigoUltimaCategoriaOpenCart = null;
            $this->idCorOpenCart = null;
            $this->idTamOpenCart = null;
            $this->idCorProductOptionOpenCart = null;
            $this->idTamProductOptionOpenCart = null;
            $this->product_option_value_id_cor = null;
            $this->product_option_value_id_tam = null;

            $this->codigoProdutoOpenCart = $this->getCodigoProdutoOpenCart();
            $this->codigoProdutoOpenCartPai = $this->getCodigoProdutoOpenCartPai();

            $this->idFabricanteOpenCart = $this->getIdFabricanteOpenCart();


            $codigoCategoriasErp = explode(",", $this->codigoCategoriaERP);

            //Zerar a variável para não vir com lixo de outros produtos
            $this->codigoCategoriaOpenCart = array();

            foreach ($codigoCategoriasErp as $codigoCategoriaErp) {
                if ($codigoCategoriaErp != "") {
                    $category_id = $this->getCodigoCategoriaOpenCart($codigoCategoriaErp);
                    //array_push($this->codigoCategoriaOpenCart, $category_id);
                    $results = $this->getAllParentCategoriesIds($category_id);
                    foreach ($results as $result) {
                        array_push($this->codigoCategoriaOpenCart, $result["category_id"]);
                    }
                }
            }


            //Validar produto
            if (empty($this->codigoProdutoERP)) {
                throw new Exception("É necessário fornecer o código do produto");
            }

            if ($this->habilitado != "S" AND $this->habilitado != "N") {
                throw new Exception("É necessário fornecer o status do produto como S ou N (valor fornecido = " . $this->habilitado . ")");
            }

            $this->idTamOpenCart = null;
            $this->idCorOpenCart = null;

            if ($this->habilitado == "S") {

                //Se código pai existe (Produto Grade)
                if (!empty($this->codigoProdutoPaiERP)) {

                    //É necessário criar as Option/Option_description COR(-1) e TAMANHO(-2)
                    $this->checkOptionCor();
                    $this->checkOptionCorDescricao();

                    $this->checkOptionTamanho();
                    $this->checkOptionTamanhoDescricao();

                    $this->idTamOpenCart = $this->getIdTamOpenCart();
                    $this->idCorOpenCart = $this->getIdCorOpenCart();
                }

                //Validar regras de execução
                if (empty($this->nomeProduto)) {
                    throw new Exception("É necessário fornecer o nome do produto");
                }

                if (empty($this->codigoCategoriaOpenCart)) {
                    throw new Exception("Não foi possível encontrar a Categoria ID = " . $this->codigoCategoriaERP);
                }
            }
        } catch (Exception $exc) {
            //throw new Exception(Erro::mensagem("",$exc),$exc->getCode(),$exc);
            throw new Exception($exc->getMessage() . "\nTRACE: " . $exc->getTraceAsString());
        }
    }

    /**
     * Altera o preço do produto caso esteja configurado no webservice
     * @param float|string $precoVenda
     * @return float O novo preço de venda ou null em caso de não ocorrido alteração
     */
    protected function alteraPreco($precoVenda)
    {
        $novoPreco = 0;
        $query = $this->query(
            "SELECT *
            FROM web_configuration
            WHERE webservice = 'alteraPrecos'"
        );

        while ($data = $query->fetch_assoc()) {
            $operadores = explode(",", $data['opcao']);

            if ($operadores[0] === "+") {
                if (isset($operadores[1])) {
                    if ($operadores[1] === "%") {
                        $porcentagem = (100 - $data['value']) / 100;
                        $novoPreco = $precoVenda / $porcentagem;
                    }
                } else {
                    $novoPreco = $precoVenda + $data['value'];
                }
            } elseif ($operadores[0] === "-") {
                if (isset($operadores[1])) {
                    if ($operadores[1] === "%") {
                        $porcentagem = (100 - $data['value']) / 100;
                        $novoPreco = $precoVenda * $porcentagem;
                    }
                } else {
                    $novoPreco = $precoVenda - $data['value'];
                }
            }
        }

        return $novoPreco;
    }

    /**
     * Retorna um array contendo
     * @param string $codigoCategoriaErp Código ERP da categoria
     * @return array Contendo o Id da categoria e todos as  do da categoria no OpenCart ou 0 caso não exista
     * @throws Exception Em caso de erro na query
     */
    protected function getCodigoCategoriaOpenCart($codigoCategoriaErp)
    {
        $res = $this->query(
            "SELECT category_id
            FROM category
            WHERE category_alternate_id = '{$codigoCategoriaErp}';"
        );

        if ($data = $res->fetch_object()) {
            $response = (int)$data->category_id;
        } else {
            $response = 0;
        }

        return $response;
    }

    /**
     * Retorna o id do código do produto baseado no Código ERP (sku)
     * @return int Id do código do produto no OpenCart ou 0 caso não encontre
     * @throws Exception Em caso de erro na query
     */
    protected function getCodigoProdutoOpenCart()
    {
        $query = $this->query(
            "SELECT product_id
            FROM product
            WHERE sku = '{$this->codigoProdutoERP}'"
        );

        if ($data = $query->fetch_object()) {
            $response = (int)$data->product_id;
        } else {
            $response = 0;
        }

        return $response;
    }

    /**
     * Retorna o id do código do produto pai baseado no Código ERP (sku)
     * @return int Id do código do produto pai no OpenCart ou 0 caso não encontre
     * @throws Exception
     */
    protected function getCodigoProdutoOpenCartPai()
    {
        $query = $this->query(
            "SELECT product_id
            FROM product
            WHERE sku = '{$this->codigoProdutoPaiERP}';"
        );

        if ($data = $query->fetch_object()) {
            $response = (int)$data->product_id;
        } else {
            $response = 0;
        }

        return $response;
    }

    /**
     * Retorna O id do fabricante baseado no nome do fabricante no ERP
     * @return int O id do fabricante no Opencart baseado no nome ou 0 caso não encontre
     * @throws Exception Em caso de falha na query
     */
    protected function getIdFabricanteOpenCart()
    {
        $response = 0;

        if (!empty($this->nomeFabricanteERP)) {
            $query = $this->query(
                "SELECT manufacturer_id
                FROM manufacturer WHERE
                UPPER(`name`) = '" . strtoupper($this->nomeFabricanteERP) . "';"
            );

            if ($data = $query->fetch_object()) {
                $response = $data->manufacturer_id;
            }

        }

        return $response;
    }

    /**
     * Verifica se a 'option' de cor existe no banco de dados e insere se não existe
     * @throws Exception Se existe algum erro ao selectionar/inserir a 'option' de cor (-1) do/no banco de dados
     */
    protected function checkOptionCor()
    {
        $query = $this->query("SELECT option_id FROM `option` WHERE option_id = -1");
        if (!$data = $query->fetch_object()) {
            //Insert name, option_id
            $this->query(
                "INSERT INTO `option` (`option_id`, type, sort_order)
                SELECT -1,'image',MAX(sort_order)+1 FROM `option`;"
            );
        }
    }

    /**
     * Verifica se a descrição da 'option' de cor exite no banco e insere se não existe
     * @throws Exception Em caso de falha na query
     */
    protected function checkOptionCorDescricao()
    {
        $query = $this->query(
            "SELECT option_id
            FROM option_description
            WHERE option_id = -1 AND language_id = {$this->language_id};"
        );
        if (!$data = $query->fetch_object()) {
            //Insert name, option_id
            $this->query(
                "INSERT INTO option_description (`option_id`,`name`,language_id)
                VALUES(-1,'Cores', {$this->language_id});"
            );
        }
    }

    /**
     * Verifica se a 'option' de tamanho já existe no banco de dados e insere se não existe
     * @throws Exception Se existe algum erro ao selectionar/inserir a 'option' de tamanho (-2) do/no banco de dados
     */
    protected function checkOptionTamanho()
    {
        $query = $this->query("SELECT option_id FROM `option` WHERE option_id = -2");
        if (!$data = $query->fetch_object()) {
            //Insert name, option_id
            $this->query(
                "INSERT INTO `option` (`option_id`, type, sort_order)
                SELECT -2, 'image', MAX(sort_order)+1 FROM `option`;"
            );
        }
    }

    /**
     * Verifica se a descrição da 'option' de tamanho já existe no banco de dados e insere se não existe
     * @throws Exception Se existe algum erro ao selectionar/inserir a 'option' de tamanho (-2) do/no banco de dados
     */
    protected function checkOptionTamanhoDescricao()
    {
        $query = $this->query(
            "SELECT option_id
            FROM option_description
            WHERE option_id = -2 AND language_id= {$this->language_id};"
        );
        if (!$data = $query->fetch_object()) {
            //Insert name, option_id
            $this->query(
                "INSERT INTO option_description (`option_id`,`name`,language_id)
                VALUES(-2,'Tamanhos', {$this->language_id});"
            );
        }

    }

    /**
     * @return null|int O id da option value de tamanho caso exista, senão retorna null
     * @throws Exception Em caso de erro na query
     */
    protected function getIdTamOpenCart()
    {
        $response = null;

        if (!empty($this->codTamERP)) {
            //$sql="SELECT option_value_id FROM option_value WHERE option_id = -2 and option_value_alternate_id='" . $this->codTamERP . "'";
            $query = $this->query(
                "SELECT option_value.option_value_id
                FROM option_value
                JOIN option_value_sce USING(option_value_id)
                WHERE option_value_sce.option_value_alternate_id = '{$this->codTamERP}'
                    AND option_value.option_id = -2;"
            );

            if ($data = $query->fetch_object()) {
                $response = $data->option_value_id;
            }
        }

        return $response;
    }

    /**
     * @return null
     * @throws Exception
     */
    protected function getIdCorOpenCart()
    {
        $response = null;

        if (!empty($this->codCorERP)) {
            //$sql="SELECT option_value_id FROM option_value WHERE option_id = -1 and option_value_alternate_id='" . $this->codCorERP . "'";
            $query = $this->query(
                "SELECT option_value.option_value_id
                FROM option_value
                JOIN option_value_sce USING (option_value_id)
                WHERE option_value_sce.option_value_alternate_id = '{$this->codCorERP }'
                    AND option_value.option_id = -1;"
            );

            if ($data = $query->fetch_object()) {
                $response = $data->option_value_id;
            }
        }

        return $response;
    }

    /**
     * Insere o fabricante
     * @throws Exception Em caso de erro na query
     */
    protected function insereFabricante()
    {
        $this->query(
            "INSERT INTO manufacturer(`name`,sort_order)
            SELECT '{$this->nomeFabricanteERP}', MAX(sort_order) + 1 FROM manufacturer;"
        );

        $this->idFabricanteOpenCart = $this->conn->insert_id;

        $this->query(
            "INSERT INTO manufacturer_to_store(manufacturer_id ,store_id)
            VALUES ('{$this->idFabricanteOpenCart}', 0);"
        );

    }

    /**
     * Atualiza o fabricante
     * @throws Exception Em caso de erro na query
     */
    protected function atualizarFabricante()
    {
        $this->query(
            "UPDATE manufacturer
            SET `name` = '{$this->nomeFabricanteERP}'
            WHERE manufacturer_id = '{$this->idFabricanteOpenCart}';"
        );
    }

    /**
     * SQL de status que retém estoque
     * @return string
     */
    protected function getSqlStatusRetemEstoque()
    {
        return "SELECT DISTINCT order_status_id FROM order_status WHERE retains_stock = 1";
    }

    //Se o arquivo da foto não existir, a função substitui ela por no_foto.jpg
    /**
     * @throws Exception
     */
    protected function atualizarProduto()
    {
        if (empty($this->precoVenda)) {
            $this->logError("Erro de Atualização: O produto (SKU: $this->codigoProdutoERP|Cod. Barras: {$this->codigoBarras}) está com o preço vazio (0,00)");
            $this->habilitado = 'N';
        }


        if ($this->habilitado == "S") {
            $this->insereGrupoAtributo();
            $this->insereGrupoAtributo();
            $this->insereAtributo();
            $this->insereProdutoDescricao($this->codigoProdutoOpenCart);
            $this->insereProdutLoja($this->codigoProdutoOpenCart);
            $this->insereCategoriaProduto($this->codigoProdutoOpenCart);
            $this->atualizaFotoPrincipal();
            $this->insereFotos($this->codigoProdutoOpenCart, $this->codigoBarras);

            //Verifica se está setado pra subir sempre habilitado
            $estoque = $this->getEstoqueValue();
            $status = $this->getStatusValue($estoque);


            $this->query(
                "UPDATE product
                SET model = '{$this->codigoBarras}',
                    upc = '{$this->codigoBarras}',
                    manufacturer_id = {$this->idFabricanteOpenCart},
                    minimum  = 1,
                    `status`  = {$status} ,
                    weight = {$this->pesoBrutoKG},
                    height = {$this->alturaCM},
                    width  = {$this->larguraCM},
                    length = {$this->comprimentoCM},
                    quantity = {$estoque},
                    price = {$this->precoVenda}
                WHERE product_id = {$this->codigoProdutoOpenCart};"
            );

            //Atributos
            $this->atualizaClassificadores();

            //Descontos
            $descontos = new Descontos($this->conn);
            $descontos->aplicaDescontos($this->codigoProdutoOpenCart, $this->precoVenda);
        } else {
            //Se não habilitado apenas mude o status
            $this->query(
                "UPDATE product
                SET `status` = 0
                WHERE product_id = {$this->codigoProdutoOpenCart};"
            );

        }
    }

    //Insere as fotos secundárias que estiverem presentes no diretório de imagens
    /**
     * @throws Exception
     */
    protected function insereGrupoAtributo()
    {
        $attribute_group_id = $this->getGrupoId();

        $query = $this->query("SELECT * FROM attribute_group WHERE attribute_group_id = {$attribute_group_id};");

        if ($data = $query->fetch_object()) {

            $this->query(
                "UPDATE attribute_group_description
                SET `name` = '{$this->descTipo}'
                WHERE sce_id =  '{$this->tipoProdutoId}';"
            );

        } else {

            $sql = "INSERT INTO attribute_group (attribute_group_id, sce_id, sort_order)
                    VALUES ({$attribute_group_id }, '{$this->tipoProdutoId}', 0)";

            $this->query($sql);

            if ($this->conn->affected_rows > 0) {

                $this->query(
                    "INSERT INTO attribute_group_description (attribute_group_id, language_id, sce_id, `name`)
                    VALUES ({$attribute_group_id}, 2, '{$this->tipoProdutoId}', '{$this->descTipo}')"
                );

            } else {
                throw new Exception("SQL:: {$sql} | ERROR: Não ocorreu inserção do grupo de atributo (attribute_group)");
            }

        }

        $this->insereAtributo();
    }

    /**
     * Monta e retorna a constante de id do grupo
     * @return int O id do grupo
     */
    protected function getGrupoId()
    {
        return (int)('-' . (!!$this->tipoProdutoId ? $this->tipoProdutoId : 1) . '00');
    }

    protected function insereAtributo()
    {

        $id = $this->getGrupoId();

        $id1 = $id - 1;
        $id2 = $id - 2;
        $id3 = $id - 3;

        $query = $this->query("SELECT * FROM attribute WHERE attribute_group_id = {$id};");

        if ($data = $query->fetch_object()) {

            if (!empty($this->classificador1)) {
                $this->query(
                    "UPDATE attribute_description
                    SET `name` = '{$this->descClassificador1}'
                    WHERE attribute_id = {$id1};"
                );
            }

            if (!empty($this->classificador2)) {
                $this->query(
                    "UPDATE attribute_description
                    SET `name` = '{$this->descClassificador2}'
                    WHERE attribute_id = {$id2};"
                );
            }

            if (!empty($this->classificador3)) {
                $this->query(
                    "UPDATE attribute_description
                    SET `name` = '{$this->descClassificador3}'
                    WHERE attribute_id = {$id3};"
                );
            }

        } else {

            if (!empty($this->classificador1)) {
                $sql = "INSERT INTO attribute (attribute_id, attribute_group_id, sort_order)
                        VALUES ({$id1},{$id}, 0);";

                $this->query($sql);

                if ($this->conn->affected_rows > 0) {
                    $this->query(
                        "INSERT INTO attribute_description (attribute_id, language_id, sce_id, `name`)
                        VALUES ({$id1}, 2, '{$this->tipoProdutoId}', '{$this->descClassificador1}');"
                    );
                } else {
                    throw new Exception("SQL: {$sql} | ERROR: Não ocorreu a inserção do atributo (attribute)");
                }
            }

            if (!empty($this->classificador2)) {
                $sql = "INSERT INTO attribute (attribute_id, attribute_group_id, sort_order)
                        VALUES ({$id2}, {$id}, 0);";

                $this->query($sql);

                if ($this->conn->affected_rows > 0) {
                    $this->query(
                        "INSERT INTO attribute_description (attribute_id, language_id, sce_id, `name`)
                        VALUES ({$id2}, 2, '{$this->tipoProdutoId}', '{$this->descClassificador2}');"
                    );
                } else {
                    throw new Exception("SQL: {$sql} | ERROR: Não ocorreu a inserção do atributo (attribute)");
                }
            }

            if (!empty($this->classificador3)) {
                $sql = "INSERT INTO attribute (attribute_id, attribute_group_id, sort_order)
                         VALUES ({$id3}, {$id}, 0);";

                $this->query($sql);

                if ($this->conn->affected_rows > 0) {
                    $this->query(
                        "INSERT INTO attribute_description (attribute_id, language_id, sce_id, `name`)
                            VALUES ({$id3}, 2, '{$this->tipoProdutoId}', '{$this->descClassificador3}');"
                    );
                } else {
                    throw new Exception("SQL: {$sql} | ERROR: Não ocorreu a inserção do atributo (attribute)");
                }
            }
        }
    }

    /**
     * @param $product_id
     * @throws Exception
     */
    protected function insereProdutoDescricao($product_id)
    {
        $query = $this->query(
            "SELECT product_id
            FROM product_description
            WHERE product_id = {$product_id} AND language_id = {$this->language_id};"
        );

        if ($data = $query->fetch_object()) {
            if ($this->sincronizar == "S") {

                $this->query(
                    "UPDATE product_description
                    SET `name` = '{$this->nomeProduto}',
                        description = '{$this->descricaoProduto}',
                        meta_title = '{$this->nomeProduto}'
                    WHERE product_id = {$product_id} AND  language_id = {$this->language_id};"
                );

            }

        } else {
            $this->query(
                "INSERT INTO product_description (
                    product_id,
                    language_id,
                    `name`,
                    description,
                    `meta_title`
                ) VALUES (
                    {$product_id},
                    {$this->language_id},
                   '{$this->nomeProduto}',
                   '{$this->descricaoProduto}',
                   '{$this->nomeProduto}'
                );"
            );

        }
    }

    /**
     * Insere a descrição do produto de
     * @param int $product_id
     * @throws Exception
     */
    protected function insereProdutoDescricaoGrade($product_id)
    {
        $query = $this->query(
            "SELECT product_id
            FROM product_description
            WHERE product_id = {$product_id} AND language_id = {$this->language_id};"
        );

        if ($data = $query->fetch_object()) {
            if ($this->sincronizar == "S" && $this->codigoProdutoERP == $this->codigoProdutoPaiERP) {

                $this->query(
                    "UPDATE product_description
                    SET `name` = '{$this->nomeProduto}',
                        description = '{$this->descricaoProduto}',
                        meta_title = '{$this->nomeProduto}'
                    WHERE product_id = {$product_id} AND language_id = {$this->language_id};"
                );

            }

        } else {
            $this->query(
                "INSERT INTO product_description (
                    product_id,
                    language_id,
                    `name`,
                    description,
                    `meta_title`
                ) VALUES (
                    {$product_id},
                    {$this->language_id},
                   '{$this->nomeProduto}',
                   '{$this->descricaoProduto}',
                   '{$this->nomeProduto}'
                );"
            );

        }
    }

    /**
     * @param int $product_id O id do produto no OpenCart
     * @throws Exception
     */
    protected function insereProdutLoja($product_id)
    {
        $query = $this->query(
            "SELECT product_id
            FROM product_to_store
            WHERE product_id = {$product_id} AND store_id = 0;"
        );

        if (!$data = $query->fetch_object()) {
            $this->query(
                "INSERT INTO product_to_store (product_id, store_id)
                VALUES ({$product_id}, 0);"
            );
        }
    }

    /**
     * @param $product_id
     * @return array
     * @throws Exception
     */
    protected function insereCategoriaProduto($product_id)
    {
        //A referencia da lista escolar na tabela produtct_to_category não pode ser removida, pois o webservice que insere o produto na lista é outro
        $idListaEscolar = $this->getCodigoListaEscolar();

        $res = $this->query(
            "SELECT category_id
            FROM product_to_category
            WHERE product_id = {$product_id};"
        );

        while ($row = $res->fetch_assoc()) {
            $category_id = $row['category_id'];
            if (!$this->eFilha($category_id, $idListaEscolar)) {
                //Excluo todas as categorias que não forem da lista ecolar
                $this->query(
                    "DELETE FROM product_to_category
                    WHERE product_id = {$product_id}
                    AND category_id = {$category_id};"
                );
            }
        }

        //Retiro os valores duplicados
        $codigoCategorias = array_unique($this->codigoCategoriaOpenCart);

        //Inclui todas as categorias vindas do ERP
        foreach ($codigoCategorias as $codigoCategoria) {

            //Insere a categoria vinda do erp se ela não for filha de lista escolar
            if (!$this->eFilha($codigoCategoria, $idListaEscolar)) {
                $this->query(
                    "INSERT INTO product_to_category (product_id, category_id)
                    VALUES({$product_id} , {$codigoCategoria});"
                );
                $categoriasInseridas[] = $codigoCategoria;
            }
        }
        return $codigoCategorias;
    }

    /**
     * Retorna todas a hierarquia de categorias dos produtos
     *
     * @param integer $category_id
     * @return array
     * @throws Exception
     */
    protected function getAllParentCategoriesIds($category_id)
    {
        $query = $this->query("
            SELECT t.category_id, @pv := t.parent_id parent_id
            FROM (SELECT * FROM category ORDER BY category_id DESC) t
            JOIN (SELECT @pv := {$category_id}) tmp
            WHERE t.category_id = @pv;
        ");

        $response = array();

        while ($row = $query->fetch_assoc()) {
            array_push($response, $row);
        }

        return $response;
    }

    /**
     * Retorna o id da categoria com o nome "LISTA ESCOLAR"
     *
     * @return int
     * @throws Exception
     */
    protected function getCodigoListaEscolar()
    {
        $consulta = $this->query("SELECT category_id FROM category_description WHERE `name` = 'LISTA ESCOLAR'");
        $idLista = 0;

        if ($res = $consulta->fetch_object()) {
            $idLista = (int)$res->category_id;
        }

        return $idLista;
    }

    protected function eFilha($categoriaFilha, $categoriaPai)
    {
        //Se as categorias passadas não forem vaziaas
        if (!empty($categoriaFilha) && !empty($categoriaPai)) {
            //Seleciona todas as filhas de lista escolar
            $res = $this->query(
                "SELECT category_id
                FROM category
                WHERE parent_id = {$categoriaPai};"
            );
            while ($row = $res->fetch_assoc()) {
                //Filha direta
                if ($categoriaFilha == $row['category_id']) {
                    return true;
                } else {
                    $resLista = $this->query("SELECT * FROM category WHERE parent_id = {$row['category_id']}");
                    while ($rowLista = $resLista->fetch_assoc()) {
                        if ($rowLista['category_id'] == $categoriaFilha) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Atualiza a foto principal do produto (produto pai, no caso de produtos de grade)
     * @throws Exception Em caso de erro na query, ou os valores de Id estejam vazios
     */
    public function atualizaFotoPrincipal()
    {
        if ($this->codigoProdutoOpenCartPai != 0) {
            $product_id = $this->codigoProdutoOpenCartPai;
        } elseif ($this->codigoProdutoOpenCart) {
            $product_id = $this->codigoProdutoOpenCart;
        } else {
            throw new Exception("Não foi possível encontrar o product_id na função atualizaFotoPrincipal");
        }

        $url = "data/" . $this->codigoBarras . self::$arrayFotos[0] . ".jpg";

        if (file_exists(DIR_IMAGE . $url) || !isset(self::$arrayFotos[1])) {
            $this->query(
                "UPDATE product
                SET image = '{$url}', sort_order = 1
                WHERE product_id = {$product_id};"
            );
        } else {
            $url = "data/" . $this->codigoBarras . self::$arrayFotos[1] . ".jpg";
            if (file_exists(DIR_IMAGE . $url)) {
                $this->query(
                    "UPDATE product
                    SET image = '{$url}', sort_order = 1
                    WHERE product_id = {$product_id};"
                );
            }
        }
    }

    /**
     * Insere a(s)foto(s) do produto no OpenCart
     * @param int $product_id Id do produto no OpenCaet
     * @param string $codigoBarras Código de barras do produto
     * @throws Exception Em caso de erro na query
     */
    protected function insereFotos($product_id, $codigoBarras)
    {
        $i = 0; //Ordem de exibição
        //$j = 0;
        foreach (self::$arrayFotos as $legenda) {

            $url = "data/{$codigoBarras}{$legenda}.jpg";

            //Verifica se a imagem existe e a insere na tabela product_image
            if (file_exists(DIR_IMAGE . $url)) {

                $this->query(
                    "INSERT INTO product_image (product_id, image, sort_order)
                        SELECT * FROM (SELECT {$product_id}, '{$url}', {$i}) AS tmp
                        WHERE NOT EXISTS (
                            SELECT image FROM product_image WHERE image = '{$url}'
                    ) LIMIT 1;"
                );

            }
            $i++;
        }
    }

    //Insere todas as categorias de um produto
    //Recebe o id do protuo e retorna as categorias inseridas

    protected function getCategoriasOpencartPeloProductId($product_id)
    {
        $categoriasOpencart = array();
        $query = $this->query(
            "SELECT category_id
            FROM product_to_category
            WHERE product_id = {$product_id}"
        );

        while ($data = $query->fetch_assoc()) {
            $categoriasOpencart[] = $data['category_id'];
        }

        return $categoriasOpencart;
    }

    public function categoriasSemControleEstoque($categorias)
    {
        $result = $this->query(
            "SELECT `value`
            FROM web_configuration
            WHERE `option` = 'categoriasSemControleEstoque'"
        );

        $catSemContEstq = '';

        while ($row = $result->fetch_assoc()) {
            $catSemContEstq = $row['value'];
        }

        $catSemContEstq = explode(",", $catSemContEstq);

        if (count($catSemContEstq) > 0) {

            foreach ($catSemContEstq as $catSemEstq) {
                foreach ($categorias as $categoria) {
                    if ($categoria == $catSemEstq || $this->eFilha($categoria, $catSemEstq)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected function atualizaClassificadores()
    {
        $this->query(
            "DELETE FROM product_attribute
            WHERE product_id = {$this->codigoProdutoOpenCart};"
        );

        $id = $this->getGrupoId();
        $id1 = $id - 1;
        $id2 = $id - 2;
        $id3 = $id - 3;

        $this->insereClassificador($this->classificador1, $id1);
        $this->insereClassificador($this->classificador2, $id2);
        $this->insereClassificador($this->classificador3, $id3);
    }

    protected function insereClassificador($texto, $id)
    {
        if (!empty($texto)) {
            $this->query(
                "INSERT INTO product_attribute (
                    product_id,
                    attribute_id,
                    language_id,
                    `text`
                ) VALUES (
                    {$this->codigoProdutoOpenCart},
                    {$id},
                    2,
                    '{$texto}'
                );"
            );
        }
    }

    protected function atualizarProdutoGrade()
    {

        if ($this->habilitado == "S") {

            $this->insereGrupoAtributo();

            //Precisa ser executado antes do update da UltimaCatProd na tab Produto
            //para apagar a categoria anterior
            $this->insereCategoriaProduto($this->codigoProdutoOpenCartPai);

            $sql = "UPDATE product SET ";

            if ($this->codigoProdutoERP == $this->codigoProdutoPaiERP) {
                $sql .= "model = '{$this->codigoBarras}',
                         price = {$this->precoVenda},";
            }

            $sql .= "minimum = 1,
                    manufacturer_id = {$this->idFabricanteOpenCart},
                    status = 1,
                    weight = {$this->pesoBrutoKG},
                    height = {$this->alturaCM},
                    width  = {$this->larguraCM},
                    length = {$this->comprimentoCM},
                    has_option = 1,
                    stock_status_id = 6
                    WHERE product_id = {$this->codigoProdutoOpenCartPai}";

            $this->query($sql);

            //Atualiza Descrição
            if ($this->sincronizar == "S") {
                $this->insereProdutoDescricaoGrade($this->codigoProdutoOpenCartPai);
            }

            $this->insereProdutLoja($this->codigoProdutoOpenCartPai);
            $this->insereCor();
            $this->insereTamanho();

            //Alimenta a tabela Protuct_Option
            $this->defineProdutodeCorTamanho();
            $this->insereProdutoCorTamanhoLegenda();
            $this->insereProdutoCorTamanhoEstoque();

            $this->getCodigoBarrasProdutoPai();
            $this->atualizaFotoPrincipal();

            $this->insereFotos($this->codigoProdutoOpenCartPai, $this->codigoBarras);
            $this->atualizaPesoProdutoGrade();

            //Atributos
            $this->atualizaClassificadores();

            $descontos = new Descontos($this->conn);
            $descontos->aplicaDescontos($this->codigoProdutoOpenCartPai, $this->precoVenda);
        } else {

            //Se não habilitado apenas mude o status da option
            $sql = "UPDATE product_option_variant
                    SET active = 0
                    WHERE sku = '{$this->codigoProdutoERP}'";

            $this->query($sql);

            $this->atualizaStatusProdutoPaiGrade();
        }
    }

    protected function insereCor()
    {
        $cor = $this->corGrade;
        $cor = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $cor));
        $cor = strtolower(str_replace(" ", "", $cor));
        $cor = str_replace("/", "", $cor);

        if (empty($this->idCorOpenCart)) {
            $this->query("INSERT INTO option_value (`option_id`, sort_order, image)
                          VALUES (-1 , 0, 'data/cores/{$cor}.jpg');");

            $this->idCorOpenCart = $this->conn->insert_id;

            $this->query("INSERT INTO option_value_sce (option_value_id, option_value_alternate_id)
                          VALUES ({$this->idCorOpenCart}, '{$this->codCorERP}');");

        } else {

            $this->query("UPDATE option_value
                          SET image = 'data/cores/{$cor}.jpg'
                          WHERE option_value_id = {$this->idCorOpenCart};");

        }

        $this->atualizarCorDescricao();
    }

    protected function atualizarCorDescricao()
    {
        if (empty($this->codCorERP)) {
            $query = $this->query(
                "SELECT option_value_id
                 FROM option_value_description
                 WHERE option_value_id = {$this->idCorOpenCart} AND language_id = {$this->language_id};"
            );

            if ($data = $query->fetch_object()) {
                //Update name, option_id
                $sql = "UPDATE option_value_description
                        SET `name` = '{$this->corGrade}', `option_id` = -1
                        WHERE option_value_id = {$this->idCorOpenCart} AND language_id = {$this->language_id};";
            } else {
                //Insert name, option_id
                $sql = "INSERT INTO option_value_description(`option_id`,option_value_id,language_id ,`name`)
                        VALUES('-1', '{$this->idCorOpenCart}', {$this->language_id}, '{$this->corGrade}');";
            }

            $this->query($sql);

        }
    }

    protected function insereTamanho()
    {
        if (empty($this->idTamOpenCart)) {

            $this->query("INSERT INTO option_value(`option_id`, image, sort_order)
                          VALUES(-2, 'data/tam/{$this->refTamERP}.jpg', 0);");

            $this->idTamOpenCart = $this->conn->insert_id;

            $this->query("INSERT INTO option_value_sce (option_value_id, option_value_alternate_id)
                    VALUES ({$this->idTamOpenCart}, '{$this->codTamERP}');");

        } else {
            $this->query("UPDATE option_value
                          SET image = 'data/tam/{$this->refTamERP}.jpg'
                          WHERE option_value_id = '{$this->idTamOpenCart}';");
        }

        $this->atualizarTamanhoDescricao();
    }

    protected function atualizarTamanhoDescricao()
    {
        if (!empty($this->codTamERP)) {
            $query = $this->query(
                "SELECT option_value_id
                FROM option_value_description
                WHERE option_value_id = {$this->idTamOpenCart} AND language_id = {$this->language_id}"
            );

            if ($data = $query->fetch_object()) {
                //Update name, option_id
                $sql = "UPDATE option_value_description
                        SET `name` = '{$this->tamanhoGrade}', `option_id` = -2
                        WHERE option_value_id = '{$this->idTamOpenCart}' AND language_id = {$this->language_id};";

            } else {
                //Insert name, option_id
                $sql = "INSERT INTO option_value_description(`option_id`, option_value_id, language_id ,`name`)
                        VALUES(-2, '{$this->idTamOpenCart}', {$this->language_id}, '{$this->tamanhoGrade}');";

            }

            $this->query($sql);

        }
    }

    protected function defineProdutodeCorTamanho()
    {
        //Procurando COR
        $query = $this->query(
            "SELECT product_option_id
            FROM product_option
            WHERE product_id = '{$this->codigoProdutoOpenCartPai}' AND option_id = -1"
        );

        if ($data = $query->fetch_object()) {
            $this->idCorProductOptionOpenCart = $data->product_option_id;
        } else {
            $this->query(
                "INSERT INTO product_option (product_id, option_id, `value`, required)
                VALUES ({$this->codigoProdutoOpenCartPai}, -1, 'Constante Cor', 1);"
            );

            $this->idCorProductOptionOpenCart = $this->conn->insert_id;
        }

        //Procurando TAMANHO
        $query = $this->query(
            "SELECT product_option_id
                FROM product_option
                WHERE product_id = '{$this->codigoProdutoOpenCartPai}' AND option_id = -2"
        );

        if ($data = $query->fetch_object()) {
            $this->idTamProductOptionOpenCart = $data->product_option_id;
        } else {
            $this->query("INSERT INTO product_option (product_id, option_id, `value`, required)
                          VALUES ({$this->codigoProdutoOpenCartPai}, -2, 'Contante Tamanho',  1)");

            $this->idTamProductOptionOpenCart = $this->conn->insert_id;
        }

    }

    protected function insereProdutoCorTamanhoLegenda()
    {
        //Procurando COR
        $query = $this->query(
            "SELECT product_option_value_id
            FROM product_option_value
            WHERE product_option_id = {$this->idCorProductOptionOpenCart}
                AND product_id= {$this->codigoProdutoOpenCartPai}
                AND option_id = -1
                AND option_value_id = '{$this->idCorOpenCart}';"
        );

        if ($data = $query->fetch_object()) {
            $this->product_option_value_id_cor = $data->product_option_value_id;
        } else {
            $this->query(
                "INSERT INTO product_option_value (
                    product_option_id,
                    product_id,
                    option_id,
                    option_value_id,
                    quantity,
                    subtract,
                    price,
                    price_prefix,
                    points,
                    points_prefix,
                    weight,
                    weight_prefix
                ) VALUES (
                    '{$this->idCorProductOptionOpenCart}',
                    '{$this->codigoProdutoOpenCartPai}',
                    '-1',
                    '{$this->idCorOpenCart}',
                    '{$this->estoque}',
                    0,
                    0,
                    '+',
                    0,
                    '+',
                    '{$this->pesoBrutoKG}',
                    '+'
                );"
            );

            $this->product_option_value_id_cor = $this->conn->insert_id;
        }


        //Procurando TAMANHO
        $query = $this->query(
            "SELECT product_option_value_id
            FROM product_option_value
            WHERE product_option_id = '{$this->idTamProductOptionOpenCart}'
                AND product_id = '{$this->codigoProdutoOpenCartPai}'
                AND option_id = -2
                AND option_value_id = {$this->idTamOpenCart};"
        );

        if ($data = $query->fetch_object()) {
            $this->product_option_value_id_tam = $data->product_option_value_id;
        } else {
            $this->query(
                "INSERT INTO product_option_value (
                    product_option_id,
                    product_id,
                    option_id,
                    option_value_id,
                    quantity,
                    subtract,
                    price,
                    price_prefix,
                    points,
                    points_prefix,
                    weight,
                    weight_prefix
                )
                VALUES (
                    {$this->idTamProductOptionOpenCart},
                    {$this->codigoProdutoOpenCartPai},
                    -2,
                    {$this->idTamOpenCart},
                    0,
                    0,
                    0,
                    '+',
                    0,
                    '+',
                    0,
                    '+'
                );"
            );

            $this->product_option_value_id_tam = $this->conn->insert_id;
        }

    }

    //Verifica se o produto está na lista escolar

    protected function insereProdutoCorTamanhoEstoque()
    {
        if ($this->sempreHabilitado(self::$nomeWS)) {
            $status = 1;
            $estoque = 5;
        } else {

            $estoque = $this->estoque - $this->getQtdePedidoPendenteGRADE($this->product_option_value_id_cor,
                    $this->product_option_value_id_tam);

            //Pega todas as categorias de um produto
            $categoriasOpencart = $this->getCategoriasOpencartPeloProductId($this->codigoProdutoOpenCart);
            //Verifica se precisa controlar o estoque
            if ($this->categoriasSemControleEstoque($categoriasOpencart)) {
                $status = 1;
            } else {
                $status = ($estoque < 1) ? 0 : 1;
            }
        }

        /**
         * Tive que localizar o produto pela sua cor/tam (var)
         *
         * pois o modulo openstock cria os registro pela tela com todas as combinações
         * logo existirá produto sem sku e inativos que não poderão ser vendidos
         * $sql="SELECT id FROM product_option_variant
         *         WHERE sku='" . $this->codigoProdutoERP . "' and product_id='" . $this->codigoProdutoOpenCartPai . "';";
         */
        $query = $this->query(
            "SELECT product_option_variant_id AS id
            FROM product_option_variant
            WHERE sku = '{$this->codigoProdutoERP}';"
        );

        $image_url = "data/{$this->codigoBarras}.jpg";

        if (!file_exists(DIR_IMAGE . $image_url)) {
            $image_url = "";
        }

        if ($data = $query->fetch_object()) {
            /**
             * o estoque passa a ser o estoque do automagazine - a qtde em pedidos pendentes
             */
            $this->query(
                "UPDATE product_option_variant
                SET product_id = '{$this->codigoProdutoOpenCartPai}',
                    sku = '{$this->codigoProdutoERP}',
                    subtract = 1,
                    price = {$this->precoVenda},
                    active = {$status},
                    stock = {$estoque},
                    model = '{$this->codigoBarras}',
                    image = '{$image_url}'
                WHERE product_option_variant_id = {$data->id};"
            );
        } else {
            $this->query(
                "INSERT INTO product_option_variant (
                    product_id,
                    sku,
                    stock,
                    active,
                    subtract,
                    price,
                    model,
                    image
                ) VALUES (
                    {$this->codigoProdutoOpenCartPai},
                    '{$this->codigoProdutoERP}',
                    {$estoque},
                    {$status},
                    1,
                    {$this->precoVenda},
                    '{$this->codigoBarras}',
                    '{$image_url}'
                );"
            );
        }

        $this->insereProdutoCorTamanhoVariacao($this->conn->insert_id);
        $this->atualizaStatusProdutoPaiGrade();
    }

    //Insere no_image nos produtos sem imagem
    /**
     * @param $cor_id
     * @param $tam_id
     * @return int
     * @throws Exception Em caso de erro na query
     */
    protected function getQtdePedidoPendenteGRADE($cor_id, $tam_id)
    {
        $res = $this->query(
            "SELECT IFNULL(SUM(order_product.quantity), 0) AS qtde
            FROM  `order`
                INNER JOIN order_product ON (`order`.order_id = order_product.order_id)
                INNER JOIN order_option  AS opt_cor ON (order_product.order_product_id = opt_cor.order_product_id)
                INNER JOIN order_option AS opt_tam ON (order_product.order_product_id = opt_tam.order_product_id)
            WHERE (`order`.order_status_id IN({$this->getSqlStatusRetemEstoque()})
                AND opt_cor.product_option_value_id = {$cor_id}
                AND opt_tam.product_option_value_id = {$tam_id});"
        );

        if ($data = $res->fetch_object()) {
            $response = $data->qtde;
        } else {
            $response = 0;
        }

        return $response;
    }

    /**
     * Seta a propriedade $this->codigoBarrasPai com o código de barras do produto pai da grade
     * @throws Exception Em caso de erro na query
     */
    protected function atualizaStatusProdutoPaiGrade()
    {
        $res = $this->query(
            "SELECT product_option_variant_id AS id
                FROM product_option_variant
                WHERE product_id = '{$this->codigoProdutoOpenCartPai}' AND active = 1;"
        );

        if ($data = $res->fetch_object()) {
            $status = 1;
        } else {
            $status = 0;
        }

        /**
         * Caso não tenha encontrado nenhuma opção ativa
         * tornar o produto inativo
         */
        $this->query(
            "UPDATE product
            SET `status` = {$status}
            WHERE product_id= '{$this->codigoProdutoOpenCartPai}';"
        );

    }

    /**
     * Retorna todos os códigos de barra de uma grade
     * @throws Exception Em caso de erro na query
     */
    protected function getCodigoBarrasProdutoPai()
    {
        $result = $this->query(
            "SELECT model
            FROM product
            WHERE product_id = '{$this->codigoProdutoOpenCartPai}';"
        );

        if ($data = $result->fetch_object()) {
            $this->codigoBarrasPai = $data->model;
        }
    }

    /**
     * Insere as fotos do produto de grade
     * @throws Exception Em caso de erro na query
     */
    protected function insereFotosProdutoGrade()
    {
        //insere as fotos na tabela de grade
        $urlImagem = "data/" . $this->codigoBarrasPai . self::$arrayFotos[0] . ".jpg";

        if (file_exists(DIR_IMAGE . $urlImagem)) {
            $this->query(
                "UPDATE product_option_variant
                SET image = '{$urlImagem}'
                WHERE product_id  = {$this->codigoProdutoOpenCartPai};"
            );
        }
    }

    //Insere o desconto para grupo de clientes setado na tabela de configurações ex: desconto para revenda
    /**
     * Atualiza o peso do produto de grande no OpenStock do OpenCart
     * @throws Exception Em caso de erro na query
     */
    protected function atualizaPesoProdutoGrade()
    {
        $this->query("
            UPDATE product_option_variant
            SET weight = '{$this->pesoBrutoKG}'
            WHERE product_id = {$this->codigoProdutoOpenCart};");
    }

    /**
     * Insere as variações de cor e tamanho do produto
     * @param int $variation_id
     * @throws Exception Em caso de erro na query
     */
    protected function insereProdutoCorTamanhoVariacao($variation_id)
    {

        if (!$this->query(
            "SELECT product_option_variant_value_id
            FROM product_option_variant_value
            WHERE product_id = {$this->codigoProdutoOpenCartPai}
            AND product_option_variant_id = {$variation_id}
            AND product_option_value_id = {$this->product_option_value_id_cor};"
        )->fetch_object()
        ) {

            $this->query("
                INSERT INTO product_option_variant_value (
                    product_id,
                    product_option_variant_id,
                    product_option_value_id,
                    sort_order
                ) VALUES (
                    {$this->codigoProdutoOpenCartPai},
                    {$variation_id},
                    {$this->product_option_value_id_cor},
                    1
                );
            ");

        }

        if (!$this->query(
            "SELECT product_option_variant_value_id
            FROM product_option_variant_value
            WHERE product_id = {$this->codigoProdutoOpenCartPai}
            AND product_option_variant_id = {$variation_id}
            AND product_option_value_id = {$this->product_option_value_id_tam};"
        )->fetch_object()
        ) {
            $this->query(
                "INSERT INTO product_option_variant_value (
                    product_id,
                    product_option_variant_id,
                    product_option_value_id,
                    sort_order
                ) VALUES (
                    {$this->codigoProdutoOpenCartPai},
                    {$variation_id},
                    {$this->product_option_value_id_tam},
                    2
                );"
            );
        }
    }

    //Verifica se uma categoria é filha de outra
    /**
     * Insere o produto no OpenCart, suas descrições e etc
     * @throws Exception Em caso de erro na query
     */
    protected function insereProduto()
    {

        if ($this->habilitado == "S") {

            $this->codigoCategoriaOpenCart = isset($this->codigoCategoriaOpenCart) ? $this->codigoCategoriaOpenCart : 0;

            $this->insereGrupoAtributo();

            $this->getCategoriasOpencartPeloProductId($this->codigoProdutoOpenCart);

            $this->estoque = $this->getEstoqueValue();

            $status = $this->getStatusValue($this->estoque);

            if (empty($this->precoVenda)) {
                $this->logError("Erro de Inserção: O produto (SKU: $this->codigoProdutoERP|Cod. Barras: {$this->codigoBarras}) está com o preço vazio (0,00)");
                $status = 0;
            }

            $this->query("
                INSERT INTO product (
                    model,
                    quantity,
                    price,
                    manufacturer_id,
                    sku,
                    `weight`,
                    `height`,
                    `width`,
                    `length`,
                    sort_order,
                    `status`,
                    weight_class_id,
                    length_class_id,
                    minimum,
                    date_added
                ) VALUES (
                    '{$this->codigoBarras}',
                    {$this->estoque},
                    {$this->precoVenda},
                    {$this->idFabricanteOpenCart},
                    '{$this->codigoProdutoERP}',
                    {$this->pesoBrutoKG},
                    {$this->alturaCM},
                    {$this->larguraCM},
                    {$this->comprimentoCM},
                    2,
                    {$status},
                    1,
                    1,
                    1,
                    NOW()
                );
            ");

            $this->codigoProdutoOpenCart = $this->conn->insert_id;

            $this->insereProdutoDescricao($this->codigoProdutoOpenCart);
            $this->insereProdutLoja($this->codigoProdutoOpenCart);

            $this->insereCategoriaProduto($this->codigoProdutoOpenCart);

            $this->atualizaFotoPrincipal();
            $this->insereFotos($this->codigoProdutoOpenCart, $this->codigoBarras);

            //Atributos
            $this->atualizaClassificadores();

            $descontos = new Descontos($this->conn);
            $descontos->aplicaDescontos($this->codigoProdutoOpenCart, $this->precoVenda);

        }
    }

    //Retorna o id da categoria lista escolar
    /**
     * Insere o produto de grade no OpenCart, com sua descrição e etc
     * @throws Exception Em caso de erro na query
     */
    protected function insereProdutoGrade()
    {
        if ($this->habilitado == "S") {

            $this->codigoCategoriaOpenCart = isset($this->codigoCategoriaOpenCart) ? $this->codigoCategoriaOpenCart : 0;

            //extensão cor/tam pede isso

            $this->query(
                "INSERT INTO product (
                    price,
                    model,
                    manufacturer_id,
                    sku,
                    image,
                    `weight`,
                    `height`,
                    `width`,
                    `length`,
                    sort_order,
                    `status`,
                    weight_class_id,
                    length_class_id,
                    minimum,
                    has_option,
                    stock_status_id,
                    date_added
                ) VALUES (
                    {$this->precoVenda},
                    '{$this->codigoBarras}',
                    {$this->idFabricanteOpenCart},
                    '{$this->codigoProdutoPaiERP}',
                    'data/{$this->codigoBarras}.jpg',
                    {$this->pesoBrutoKG},
                    {$this->alturaCM},
                    {$this->larguraCM},
                    {$this->comprimentoCM},
                    2,
                    1,
                    1,
                    1,
                    1,
                    1,
                    6,
                    NOW()
                );"
            );

            $this->codigoProdutoOpenCartPai = $this->conn->insert_id;

            $this->insereProdutoDescricaoGrade($this->codigoProdutoOpenCartPai);
            $this->insereProdutLoja($this->codigoProdutoOpenCartPai);
            $this->insereCategoriaProduto($this->codigoProdutoOpenCartPai);

            $this->insereCor();
            $this->insereTamanho();

            //Alimenta a tabela Protuct_Option
            $this->defineProdutodeCorTamanho();

            $this->insereProdutoCorTamanhoLegenda();

            $this->insereProdutoCorTamanhoEstoque();

            $this->codigoBarrasPai = $this->codigoBarras;

            $this->atualizaFotoPrincipal();
            $this->insereFotos($this->codigoProdutoOpenCartPai, $this->codigoBarrasPai);

            //$this->procuraArquivoCor();

            //Atributos
            $this->atualizaClassificadores();

            $descontos = new Descontos($this->conn);
            $descontos->aplicaDescontos($this->codigoProdutoOpenCartPai, $this->precoVenda);
        }

    }

    /**
     * Realiza a lógica e retorna o estoque do produto no OpenCart
     * @return float
     */
    private function getEstoqueValue()
    {
        if ($this->sempreHabilitado(self::$nomeWS)) {
            $estoque = 10;
        } else if ($this->categoriasSemControleEstoque($this->getCategoriasOpencartPeloProductId($this->codigoProdutoOpenCart))) {
            $estoque = 10;
        } else {
            $estoque = $this->estoque;
        }

        return $estoque;
    }

    /**
     * Realiza a lógica e retorna se o status do produto é ativo ou inativo
     * @param float $estoque Estoque do produto
     * @return int Valor do status do produto (1 - Verdadeiro ou 0 - Falsos)
     */
    private function getStatusValue($estoque)
    {
        if ($this->sempreHabilitado(self::$nomeWS)) {
            $status = 1;
        } else if ($this->categoriasSemControleEstoque($this->getCategoriasOpencartPeloProductId($this->codigoProdutoOpenCart))) {
            $status = 1;
        } else {
            $minimum = $this->quantidadeMinimaVenda > 0 ? $this->quantidadeMinimaVenda : 0;
            $status = ($estoque < $minimum) ? 0 : 1;
        }

        return $status;
    }

}