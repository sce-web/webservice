<?php

class Descontos
{

    private $productId;
    private $valorInicial;

    /**
     * Objeto de conexão com o banco de dados
     * @var mysqli
     */
    private $conn;

    public function __construct(mysqli $conn)
    {
        $this->conn = $conn;
    }

    protected function query($sql)
    {
        $response = $this->conn->query($sql);

        if ($this->conn->error) {
            throw new Exception("SQL: {$sql} | ERROR: {$this->conn->error}");
        }

        return $response;
    }

    public function aplicaDescontos($productId, $valorInicial)
    {
        $this->productId = $productId;
        $this->valorInicial = $valorInicial;

        //Verifica se a tabela de Descontos existe
        $sql = "SHOW TABLES FROM " . DB_DATABASE . " LIKE 'discount'";

        $result = $this->query($sql);
        if ($result->fetch_assoc()) {
            $this->descontoPorCategoria();
        }
    }

    protected function descontoPorCategoria()
    {
        $res = $this->query("SELECT * FROM discount;");
        while ($row = $res->fetch_assoc()) {

            //Categorias com desconto
            $categoriasComDesconto = $this->juntaCategorias(explode(",", $row['categories_with_discount']));

            //Categorias sem desconto
            $categoriasSemDesconto = $this->juntaCategorias(explode(",", $row['categories_without_discount']));

            //Deleta todos os Descontos
            $this->deletaDescontos();

            //Se a categoria com desconto for 0 (todas) ou pertencer ao menos uma das categorias passadas e não pertencer a uma categoria sem desconto, insere o desconto
            if (!$this->pertenceCategoria($categoriasSemDesconto)) {
                if ($this->pertenceCategoria($categoriasComDesconto) || $categoriasComDesconto[0] == 0) {
                    $valorDesconto = $row['value'];
                    $dataInicial = $row['starting_date'];
                    $dataFinal = $row['ending_date'];
                    $prioridade = $row['priority'];

                    $valorVenda = $this->calculaValorVenda($this->valorInicial, $valorDesconto);

                    //Para todos os grupos de clientes, insiro o desconto por categoria
                    $gruposClientesIds = explode(",", $row['customer_groups']);
                    foreach ($gruposClientesIds as $grupoClienteId) {
                        $this->insere($grupoClienteId, $prioridade, $valorVenda, $dataInicial, $dataFinal);
                    }
                }

            }

        }

    }

    //Deleta todos os Descontos de um produto
    protected function deletaDescontos()
    {
        $sql = "DELETE FROM product_special WHERE product_id =" . $this->productId;
        $this->query($sql);
    }

    //Calcula o valor de venda
    protected function calculaValorVenda($valorInicial, $desconto)
    {
        $valorVenda = $valorInicial * (1 - $desconto);
        return $valorVenda;
    }

    //Verifica se um produto pertence as categorias passadas
    protected function pertenceCategoria($categorias)
    {
        foreach ($categorias as $categoria) {
            if (!empty($categoria)) {

                $res = $this->query(
                    "SELECT *
                    FROM product_to_category
                    WHERE category_id = {$categoria}
                    AND product_id = {$this->productId};"
                );

                if ($res->fetch_object()) {
                    return true;
                }
            }
        }
        return false;
    }

    //Pega todas as categorias filhas
    protected function pegaCategoriasFilhas($categorias)
    {
        $categoriasFilhas = array();
        foreach ($categorias as $categoria) {
            if (!empty($categoria)) {
                $sql = "SELECT category_id FROM category WHERE parent_id =" . $categoria;
                $res = $this->query($sql);
                while ($row = $res->fetch_assoc()) {
                    $categoriasFilhas[] = $row['category_id'];
                }
            }
        }
        return $categoriasFilhas;
    }

    //Junta as categorias
    protected function juntaCategorias($categorias)
    {
        $categoriasFilhas = $this->pegaCategoriasFilhas($categorias);
        $categorias = array_merge($categorias, $categoriasFilhas);
        return $categorias;
    }

    //Insere os Descontos
    protected function insere($grupoClienteId, $prioridade, $valorVenda, $dataInicial, $dataFinal)
    {
        $this->query(
            "INSERT INTO product_special (
                product_id,
                customer_group_id,
                priority,
                price,
                date_start,
                date_end
            ) VALUES (
                {$this->productId},
                {$grupoClienteId},
                '{$prioridade}',
                {$valorVenda},
                '{$dataInicial}',
                '{$dataFinal}'
            );"
        );
    }


}
