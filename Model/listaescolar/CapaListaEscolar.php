<?php

/**
 * Class CapaListaEscolar
 * Example: 'C;1201211124143628;AL-1º;ALECRIM - 1º ANO EF - 2015;1201212263671459; - ALECRIM;0;S'
 */
class CapaListaEscolar extends CategoriaTipo
{
    /**
     * Primeiro campo da mensagem
     * Identificador (no caso da Capa 'C' e no caso do item 'I')
     *
     * Segundo campo da mensagem
     * Guid lista
     *
     * Terceiro campo da mensagem
     * Nome resumido da lista
     *
     * Quarto campo da mensagem
     * Nome da lista
     */

    /**
     * Quinto campo da mensagem e sexto campo
     *
     * @var Colegio referente a capa
     */
    private $colegio;

    /**
     * @var float Porcentagem a ser descontado do valor de cada item da lista
     */
    private $desconto;

    /**
     * @param array $capaArray Capa da ListaEscolar com os valores em um array posicional ou associativo
     * @param bool|false $isAssoc Se o array passado como parametro é associativo
     * @return CapaListaEscolar
     */
    public static function fromArray(array $capaArray, $isAssoc = false)
    {
        $capaListaEscolar = new CapaListaEscolar();

        if ($isAssoc) {
            throw new BadMethodCallException("Not yet implemented!");
        } else {
            $capaListaEscolar->setGuid($capaArray[1]);
            $capaListaEscolar->setNome($capaArray[3]);
            $capaListaEscolar->setColegio(new Colegio($capaArray[4], $capaArray[5]));
            $capaListaEscolar->setDesconto($capaArray[6]);
            $capaListaEscolar->setAtiva($capaArray[7]);
        }

        return $capaListaEscolar;
    }

    /**
     * @return Colegio
     */
    public function getColegio()
    {
        return $this->colegio;
    }

    /**
     * @param Colegio $colegio
     */
    public function setColegio($colegio)
    {
        $this->colegio = $colegio;
    }

    /**
     * @return float
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * @param float $desconto
     */
    public function setDesconto($desconto)
    {
        $this->desconto = (float)$desconto;
    }


}