<?php

abstract class CategoriaTipo
{
    /**
     * Id da Categoria (category_id)
     *
     * @var int
     */
    protected $id;

    /**
     * Guid da Categoria (category_alternate_id)
     *
     * @var string
     */
    protected $guid;

    /**
     * Nome da Categoria
     *
     * @var string
     */
    protected $nome;

    /**
     * Se está ativa ou não
     *
     * @var bool
     */
    protected $ativa;

    /**
     * Colégio constructor.
     * @param string $guid
     * @param string $nome
     * @param int $id
     * @param bool $ativa
     */
    public function __construct($guid = "", $nome = "", $id = 0, $ativa = true)
    {
        $this->guid = $guid;
        $this->nome = $nome;
        $this->id = (int)$id;
        $this->ativa = $this->parseBool($ativa);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return boolean
     */
    public function isAtiva()
    {
        return $this->ativa;
    }

    /**
     * @param boolean $ativa
     */
    public function setAtiva($ativa)
    {
        $this->ativa = $this->parseBool($ativa);
    }

    protected function parseBool($variable)
    {
        if (is_string($variable)) {
            return $variable === "S" || $variable === "1";
        }

        if (is_float($variable)) {
            $variable = (int)$variable;
        }

        if (is_int($variable)) {
            return $variable !== 0;
        }

        return $variable;
    }

}