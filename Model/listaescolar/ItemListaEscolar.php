<?php

/**
 * Class ItemListaEscolar
 * Example: 'I;50411;;050411;1;0'
 */
class ItemListaEscolar
{
    /**
     * Primeiro campo da mensagem
     * Identificador (no caso do item 'I' e no caso da Capa 'C')
     */

    /**
     * Segundo campo da mensagem
     *
     * @var string SKU do produto
     */
    private $sku;

    /**
     * Terceiro campo da mensagem
     * ?
     */

    /**
     * Quarto campo da mensagem
     *
     * @var string código de barras do produto
     */
    private $codigoDeBarras;

    /**
     * Quinto campo da mensagem
     *
     * @var integer Quantidade do produto na lista
     */
    private $quantidade;

    /**
     * Sexto campo da mensagem
     * ?
     */

    /**
     * @var int Id do produto no Opencart
     */
    private $id;

    /**
     * @param array $itemArray Item da ListaEscolar com os valores em um array posicional ou associativo
     * @param bool|false $isAssoc Se o array passado como parametro é associativo
     * @return ItemListaEscolar
     */
    public static function fromArray(array $itemArray, $isAssoc = false)
    {
        $itemListaEscolar = new ItemListaEscolar();

        if ($isAssoc) {
            throw new BadMethodCallException("Not yet implemented");
        } else {
            $itemListaEscolar->setSku($itemArray[1]);
            $itemListaEscolar->setCodigoDeBarras($itemArray[3]);
            $itemListaEscolar->setQuantidade($itemArray[4]);
        }

        return $itemListaEscolar;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getCodigoDeBarras()
    {
        return $this->codigoDeBarras;
    }

    /**
     * @param string $codigoDeBarras
     */
    public function setCodigoDeBarras($codigoDeBarras)
    {
        $this->codigoDeBarras = $codigoDeBarras;
    }

    /**
     * @return integer
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = (int)$quantidade;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}