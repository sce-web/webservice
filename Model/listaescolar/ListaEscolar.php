<?php

/**
 * C;1201211124143628;AL-1º;ALECRIM - 1º ANO EF - 2015;1201212263671459; - ALECRIM;0;S
 * I;50411;;050411;1;0
 * I;46769;;046769;1;0
 * I;57236;;057236;1;0
 * I;45344;;9788576751045;1;0
 * I;75340;;075340;1;0
 * I;45280;;9788502073999;1;0
 * I;37967;;7897664254657;1;0
 * I;73326;;073326;3;0
 * I;65168;;065168;1;0
 * I;74266;;074266;5;0
 * I;68642;;068642;1;0
 * I;49802;;049802;2;0
 * I;71373;;071373;2;0
 * I;71017;;071017;1;0
 * I;62124;;062124;1;0
 * I;64405;;064405;1;0
 * I;68645;;068645;8;0
 * I;38674;;7898017035466;1;0
 * I;56403;;056403;1;0
 * I;69772;;069772;1;0
 * I;8138;;8765;2;0
 * I;41126;;7898441790771;2;0
 * I;10885;;11567;2;0
 * I;61966;;061966;1;0
 * I;42114;;7898924193631;1;0
 * I;61187;;061187;1;0
 * I;36961;;7897249567349;1;0
 * I;67660;;067660;1;0
 * I;68666;;068666;1;0
 * I;64496;;064496;1;0
 */
class ListaEscolar extends ConfiguracoesWebservice
{
    const LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR = 0;
    const LEVEL_CATEGORIA_COLEGIO = 1;
    const LEVEL_CATEGORIA_GRAU_COLEGIO = 2;

    /**
     * @var CapaListaEscolar
     */
    protected $capa;

    /**
     * @var Collection
     */
    protected $items;

    /**
     * Id da categoria com o nome "LISTA ESCOLAR" no OpenCart
     * @var integer
     */
    protected $categoriaListaEscolarId;

    public function alimentarCampos($message)
    {
        $lista = explode("\n", $message);

        $capa = array_shift($lista);

        $this->capa = CapaListaEscolar::fromArray(explode(";", $capa));

        $this->items = new Collection();

        foreach ($lista as $item) {
            $this->items->push(ItemListaEscolar::fromArray(explode(";", $item)));
        }

    }

    public function carregar()
    {
        //Variável para o campo PATH

        //Se existir a categoria lista escolar
        $listaEscolarQuery = $this->query("
            SELECT category_id
            FROM category_description
            WHERE name = 'LISTA ESCOLAR'
        ");

        if ($listaEscolar = $listaEscolarQuery->fetch_assoc()) {
            $this->categoriaListaEscolarId = (int)$listaEscolar["category_id"];
        } else {
            /** Se não existir categoria "Lista Escolar", insere */
            $this->categoriaListaEscolarId = $this->inserirCategoria();
            $this->inserirCategoriaDescricao($this->categoriaListaEscolarId, "LISTA ESCOLAR");
            $this->inserirCategoriaParaLoja($this->categoriaListaEscolarId);
            $this->inserirCategoriaPathLevel(
                $this->categoriaListaEscolarId,
                $this->categoriaListaEscolarId,
                self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
            );
        }

        $this->carregarCapa();



        /** Limpa relacionamento dos produtos com a categoria da série */
        if ($this->capa->isAtiva()) {
            $parent = $this;

            $this->query("DELETE FROM product_to_category WHERE category_id = {$this->capa->getId()};");

            $this->items->iterate(function ($item, $index) use ($parent) {
                $parent->carregarItem($item);
            });

           //if (!$this->query("DELETE FROM product_school_list WHERE category_id = '" . $codigo_categoria . "'")) {
           //    throw new Exception("Ocorreu erro ao deletar os produtos da lista codigo " . $campo[1] . " - mysqlerror:" . mysql_error());
           //}
        }

    }

    /**
     * Carrega a capa (O colégio e a série)
     * @throws Exception
     */
    public function carregarCapa()
    {

        /**  category_alternate_id é diferente de vazio? */
        $colegioGuid = $this->capa->getColegio()->getGuid();

        if (!empty($colegioGuid)) {

            if ($this->capa->isAtiva()) {

                //COLÉGIOS
                $colegios = $this->query("
                    SELECT
                      c.category_id,
                      c.status,
                      cd.name,
                      c.category_alternate_id
                    FROM category AS c
                    JOIN category_description AS cd USING (category_id)
                    WHERE category_alternate_id = '{$colegioGuid}';
                ");

                //SE JÁ EXISTIR O COLÉGIO
                if ($colegioArray = $colegios->fetch_assoc()) {

                    $colegio = new Colegio(
                        $colegioArray["category_alternate_id"],
                        $colegioArray["name"],
                        (int)$colegioArray["category_id"],
                        $colegioArray['status']
                    );

                    $colegioCategoriaId = $colegio->getId();

                    /** Re-ativa se a categoria estava inativa */
                    if (!$colegio->isAtiva()) {
                        $this->atualizarCategoriaStatus($colegioCategoriaId, Categoria::STATUS_ATIVA);
                    }

                    /** Atualiza o nome caso o nome do colegio tenha mudado */
                    if ($this->capa->getColegio()->getNome() !== $colegio->getNome()) {
                        $this->atualizarCategoriaDescricao($colegioCategoriaId, $this->capa->getColegio()->getNome());
                    }

                    $levelCategoriaPaiListaEscolar = self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR;
                    $levelCategoriaColegio = self::LEVEL_CATEGORIA_COLEGIO;

                    $colegioPaths = $this->queryFetchAll("
                        SELECT `level`
                        FROM category_path
                        WHERE (
                          category_id = {$colegioCategoriaId}
                          AND path_id = {$this->categoriaListaEscolarId}
                          AND `level` = {$levelCategoriaPaiListaEscolar}
                        ) OR (
                          category_id = {$colegioCategoriaId}
                          AND path_id = {$colegioCategoriaId}
                          AND `level` = {$levelCategoriaColegio}
                        );
                    ");

                    $colegioPathsNumber = count($colegioPaths);

                    switch ($colegioPathsNumber) {
                        case 0:
                            /** Não existem paths definidos */
                            $this->inserirCategoriaPathLevel(
                                $colegioCategoriaId,
                                $colegioCategoriaId,
                                self::LEVEL_CATEGORIA_COLEGIO
                            );

                            $this->inserirCategoriaPathLevel(
                                $colegioCategoriaId,
                                $this->categoriaListaEscolarId,
                                self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                            );
                            break;
                        case 1:
                            /** Existe apenas um path*/
                            if ($colegioPaths[0]["level"] === self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR) {
                                $this->inserirCategoriaPathLevel(
                                    $colegioCategoriaId,
                                    $colegioCategoriaId,
                                    self::LEVEL_CATEGORIA_COLEGIO
                                );
                            } else {
                                $this->inserirCategoriaPathLevel(
                                    $colegioCategoriaId,
                                    $this->categoriaListaEscolarId,
                                    self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                                );
                            }
                            break;
                    }

                } else {
                    /** Se não existir o colégio, insere. */

                    # Insere a categoria referente a escola e retorna o seu Id
                    $colegioCategoriaId = $this->inserirCategoria(
                        $this->categoriaListaEscolarId,
                        $this->capa->getColegio()->getGuid()
                    );

                    # Salva o Id na escola
                    $this->capa->getColegio()->setId($colegioCategoriaId);

                    # Insere a descrição da categoria
                    $this->inserirCategoriaDescricao(
                        $colegioCategoriaId,
                        $this->capa->getColegio()->getNome()
                    );

                    # Referencia a categoria a loja
                    $this->inserirCategoriaParaLoja($colegioCategoriaId);

                    # region: Constroi o caminho de profundidade

                    $this->inserirCategoriaPathLevel(
                        $colegioCategoriaId,
                        $colegioCategoriaId,
                        self::LEVEL_CATEGORIA_COLEGIO
                    );

                    $this->inserirCategoriaPathLevel(
                        $colegioCategoriaId,
                        $this->categoriaListaEscolarId,
                        self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                    );

                    # endregion

                }

                /** SÉRIES */
                $serieQuery = $this->query("
                    SELECT category_id, category_alternate_id, parent_id
                    FROM category
                    WHERE category_alternate_id = '{$this->capa->getGuid()}'
                ");


                if ($serie = $serieQuery->fetch_assoc()) {
                    /** SE A SÉRIE JÁ EXISTIR */
                    $serieCategoriaId = $serie["category_id"];
                    $serieCategoriaPaiAntigaId = $serie["parent_id"];

                    # Atualiza o status e o novo pai da categoria
                    $this->atualizarCategoriaStatusECategoriaPai(
                        $serieCategoriaId,
                        Categoria::STATUS_ATIVA,
                        $colegioCategoriaId
                    );

                    $this->atualizarCategoriaDescricao($serieCategoriaId, $this->capa->getNome());

                    $this->removerCasoSemFilhos($serieCategoriaPaiAntigaId);
                } else {
                    /**  SE A SÉRIE NÃO EXISTIR, INSERE */

                    $serieCategoriaId = $this->inserirCategoria($colegioCategoriaId, $this->capa->getGuid());

                    # Insere a descrição da categoria
                    $this->inserirCategoriaDescricao($serieCategoriaId, $this->capa->getNome());

                    # Relaciona a categoria com a loja
                    $this->inserirCategoriaParaLoja($serieCategoriaId);

                    # region: Constroi o caminho de profundidade

                    $this->inserirCategoriaPathLevel(
                        $serieCategoriaId,
                        $this->categoriaListaEscolarId,
                        self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                    );

                    $this->inserirCategoriaPathLevel(
                        $serieCategoriaId,
                        $colegioCategoriaId,
                        self::LEVEL_CATEGORIA_COLEGIO
                    );

                    $this->inserirCategoriaPathLevel(
                        $serieCategoriaId,
                        $serieCategoriaId,
                        self::LEVEL_CATEGORIA_GRAU_COLEGIO
                    );

                    # endregion

                }

            } else {
                /** Desativa a Categoria da Lista Escolar */
                $this->atualizarCategoriaStatusAlternateId($colegioGuid, Categoria::STATUS_INATIVA);
            }

        }

        /** Se o campo category_alternate_id vier vazio e se ainda assim a categoria estiver habilitada */
        if ($this->capa->isAtiva()) {

            /** Atualiza somente a série */
            $series = $this->query("
                SELECT category_id
                FROM category
                WHERE category_alternate_id = '{$this->capa->getGuid()}';
            ");

            /** Se achar a série */
            if ($serie = $series->fetch_assoc()) {
                $serieId = $serie["category_id"];
                $this->capa->setId($serieId);

                $this->atualizarCategoriaStatus($serieId, Categoria::STATUS_INATIVA);
                $this->atualizarCategoriaDescricao($serieId, $this->capa->getNome());

                $levelCategoriaGrauColegio = self::LEVEL_CATEGORIA_GRAU_COLEGIO;
                $levelCategoriaColegio = self::LEVEL_CATEGORIA_COLEGIO;
                $levelCategoriaPaiListaEscolar = self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR;

                $seriePath = $this->queryFetchAll("
                    SELECT `level`
                    FROM category_path
                    WHERE (
                      category_id = {$serieId}
                      AND path_id = {$serieId}
                      AND `level` = {$levelCategoriaGrauColegio}
                    ) OR (
                      category_id = {$serieId}
                      AND path_id = {$this->capa->getColegio()->getId()}
                      AND `level` = {$levelCategoriaColegio}
                    ) OR (
                      category_id = {$serieId}
                      AND path_id = {$this->categoriaListaEscolarId}
                      AND `level` = {$levelCategoriaPaiListaEscolar}
                    ) ORDER BY `level`;
                ");

                $seriePathsNumber = count($seriePath);

                switch ($seriePathsNumber) {
                    case 0:
                        /** Não existem paths definidos */
                        $this->inserirCategoriaPathLevel(
                            $serieId,
                            $serieId,
                            self::LEVEL_CATEGORIA_GRAU_COLEGIO
                        );

                        $this->inserirCategoriaPathLevel(
                            $serieId,
                            $this->capa->getColegio()->getId(),
                            self::LEVEL_CATEGORIA_COLEGIO
                        );

                        $this->inserirCategoriaPathLevel(
                            $serieId,
                            $this->categoriaListaEscolarId,
                            self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                        );
                        break;
                    case 1:
                        /** Existe apenas um path*/
                        if ((int)$seriePath[0]["level"] === self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR) {
                            $this->inserirCategoriaPathLevel(
                                $serieId,
                                $serieId,
                                self::LEVEL_CATEGORIA_GRAU_COLEGIO
                            );

                            $this->inserirCategoriaPathLevel(
                                $serieId,
                                $this->capa->getColegio()->getId(),
                                self::LEVEL_CATEGORIA_COLEGIO
                            );

                        } else if ((int)$seriePath[0]['level'] === self::LEVEL_CATEGORIA_COLEGIO) {
                            $this->inserirCategoriaPathLevel(
                                $serieId,
                                $serieId,
                                self::LEVEL_CATEGORIA_GRAU_COLEGIO
                            );

                            $this->inserirCategoriaPathLevel(
                                $serieId,
                                $this->categoriaListaEscolarId,
                                self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                            );
                        } else {
                            $this->inserirCategoriaPathLevel(
                                $serieId,
                                $this->capa->getColegio()->getId(),
                                self::LEVEL_CATEGORIA_COLEGIO
                            );

                            $this->inserirCategoriaPathLevel(
                                $serieId,
                                $this->categoriaListaEscolarId,
                                self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                            );
                        }

                        break;
                    case 2:
                        if ((int)$seriePath[0]['level'] === self::LEVEL_CATEGORIA_COLEGIO
                            && (int)$seriePath[1]['level'] === self::LEVEL_CATEGORIA_GRAU_COLEGIO) {
                            $this->inserirCategoriaPathLevel(
                                $serieId,
                                $this->categoriaListaEscolarId,
                                self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                            );
                        } else if ((int)$seriePath[0]['level'] === self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                            && (int)$seriePath[1]['level'] === self::LEVEL_CATEGORIA_GRAU_COLEGIO) {
                            $this->inserirCategoriaPathLevel(
                                $serieId,
                                $this->capa->getColegio()->getId(),
                                self::LEVEL_CATEGORIA_COLEGIO
                            );
                        } else if ((int)$seriePath[0]['level'] === self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR
                            && (int)$seriePath[1]['level'] === self::LEVEL_CATEGORIA_COLEGIO) {
                            $this->inserirCategoriaPathLevel(
                                $serieId,
                                $serieId,
                                self::LEVEL_CATEGORIA_GRAU_COLEGIO
                            );
                        }
                        break;

                }


            } else {
                /** Se não achar a série */

                $serieId = $this->inserirCategoria($this->categoriaListaEscolarId, $this->capa->getGuid());

                $this->capa->setId($serieId);

                $this->inserirCategoriaDescricao($serieId, $this->capa->getNome());

                $this->inserirCategoriaParaLoja($serieId);

                # region Constroi o caminho de profundidade do Path da categoria

                $this->inserirCategoriaPathLevel($serieId, $this->categoriaListaEscolarId,
                    self::LEVEL_CATEGORIA_PAI_LISTA_ESCOLAR);

                $this->inserirCategoriaPathLevel($serieId, $this->capa->getColegio()->getId(),
                        self::LEVEL_CATEGORIA_COLEGIO);

                $this->inserirCategoriaPathLevel($serieId, $serieId, self::LEVEL_CATEGORIA_COLEGIO);

                # endregion

            }



        } else {
            /** Se a categoria estiver desabilitasda somente desativa ela no OpenCart  */
            //
            $this->atualizarCategoriaStatusAlternateId($this->capa->getGuid(), Categoria::STATUS_INATIVA);
        }





    }

    /**
     * Carrega os itens da ListaEscolar
     * @param ItemListaEscolar $item
     * @throws Exception
     */
    public function carregarItem($item)
    {
        /** Procura o Id do produto no OpenCart */
        $produtoQuery = $this->query("SELECT product_id FROM product WHERE sku = '{$item->getSku()}';");

        if ($produto = $produtoQuery->fetch_assoc()) {
            /** Se achou o produto, salva o Id */
            $item->setId($produto["product_id"]);
        } else {
            /** Se não achou, procura se é de grade */
            $produtoGradeQuery = $this->query("SELECT product_id FROM product_option_variant WHERE sku = '{$item->getSku()}';");

            if ($produto = $produtoGradeQuery->fetch_assoc()) {
                /** Se achou o produto, salva o Id */
                $item->setId($produto["product_id"]);
            }
        }

        /** Se o produto existe no OpenCart */
        if ($item->getId() !== 0) {
            /** Verifica se o produto já está na categoria da série da Lista Escolar */
            $verificacaoQuery = $this->query("
                SELECT product_id, category_id
                FROM product_to_category
                WHERE category_id = {$this->capa->getId()}
                AND product_id = {$item->getId()};
            ");

            if (!$verificacaoQuery->fetch_assoc()) {
                /** Insere o produto na categoria caso ele não exista */
                $this->query("
                    INSERT INTO product_to_category (
                        product_id,
                        category_id
                    ) VALUES (
                        {$item->getId()},
                        {$this->capa->getId()}
                    );
                ");

                $consultaQuery = $this->query("
                    SELECT product_id, category_id
                    FROM product_school_list
                    WHERE product_id = {$item->getId()}
                    AND category_id = {$this->capa->getId()};
                ");
                if (!$consultaQuery->fetch_assoc()) {

                    //INSERE O PRODUTO NA LISTA
                    $this->query("
                        INSERT INTO product_school_list (
                            product_id,
                            category_id,
                            quantity_list
                        ) VALUES (
                            {$item->getId()},
                            {$this->capa->getId()},
                            {$item->getQuantidade()}
                        );");
                }
            }

            $this->query("UPDATE `product` SET `status`= 1 WHERE  `product_id`= {$item->getId()};");

        }

    }

    /**
     * @param int $categoriaPaiId Id da categoria pai em que será
     * @param string $categoriaGuid Guid da categoria no AutoMagazine
     * @param int $ordem
     * @return int
     * @throws Exception
     */
    public function inserirCategoria($categoriaPaiId = 0, $categoriaGuid = '', $ordem = 0)
    {
        $this->query("
            INSERT INTO category (
                parent_id,
                `top`,
                `column`,
                sort_order,
                `status`,
                date_added,
                category_alternate_id
            ) VALUES (
                {$categoriaPaiId},
                1,
                1,
                {$ordem},
                1,
                NOW(),
                '{$categoriaGuid}'
            );
        ");

        return (int)$this->conn->insert_id;
    }

    /**
     * @param int $categoriaId
     * @param string $categoriaNome
     * @param string $categoriaDescricao
     * @throws Exception
     */
    public function inserirCategoriaDescricao($categoriaId, $categoriaNome, $categoriaDescricao = "")
    {
        $categoriaId = (int)$categoriaId;
        $categoriaMetaTitle = $this->criarMetaString($categoriaNome);
        $categoriaMetaDescricao = $this->criarMetaString($categoriaDescricao);

        $this->query("
            INSERT INTO category_description (
                category_id,
                language_id,
                name,
                description,
                meta_title,
                meta_description,
                meta_keyword
            ) VALUES (
                {$categoriaId},
                2,
                '{$categoriaNome}',
                '{$categoriaDescricao}',
                '{$categoriaMetaTitle}',
                '{$categoriaMetaDescricao}',
                ''
            );
        ");
    }

    /**
     * @param int $categoriaId
     * @param int $pathId
     * @param int $level
     * @throws Exception
     */
    public function inserirCategoriaPathLevel($categoriaId, $pathId, $level)
    {
        $categoriaId = (int)$categoriaId;
        $pathId = (int)$pathId;
        $level = (int)$level;

        $this->query("
            INSERT INTO category_path (
                category_id,
                path_id,
                level
            ) VALUES (
                {$categoriaId},
                {$pathId},
                {$level}
            );
        ");

    }

    /**
     * @param int $categoriaId Id da categoria no OpenCart
     * @throws Exception
     */
    public function inserirCategoriaParaLoja($categoriaId)
    {
        $categoriaId = (int)$categoriaId;

        $this->query("
            INSERT INTO category_to_store (
                category_id,
                store_id
            ) VALUES (
                {$categoriaId},
                0
            );
        ");
    }

    /**
     * @param int $categoriaId
     * @param int $categoriaPaiId
     * @throws Exception
     */
    public function atualizarCategoriaCategoriaPai($categoriaId, $categoriaPaiId)
    {
        $categoriaId = (int)$categoriaId;
        $categoriaPaiId = (int)$categoriaPaiId;

        $this->query("
            UPDATE category
            SET parent_id = {$categoriaPaiId}
            WHERE category_id = {$categoriaId};
        ");
    }

    /**
     * @param $categoriaId
     * @param $status
     * @throws Exception
     */
    public function atualizarCategoriaStatus($categoriaId, $status)
    {
        $categoriaId = (int)$categoriaId;
        $status = (int)$status;

        $this->query("
            UPDATE category
            SET status = {$status}
            WHERE category_id = {$categoriaId};
        ");
    }

    public function atualizarCategoriaStatusAlternateId($categoriaAlternateId, $status)
    {
        $status = (int)$status;

        $this->query("
            UPDATE category
            SET status = {$status}
            WHERE category_alternate_id = '{$categoriaAlternateId}';
        ");
    }

    /**
     * @param $categoriaId
     * @param $status
     * @param $categoriaPaiId
     * @throws Exception
     */
    public function atualizarCategoriaStatusECategoriaPai($categoriaId, $status, $categoriaPaiId)
    {
        $categoriaId = (int)$categoriaId;
        $status = (int)$status;
        $categoriaPaiId = (int)$categoriaPaiId;

        $this->query("
            UPDATE category SET
                status = {$status},
                parent_id = {$categoriaPaiId}
            WHERE category_id = {$categoriaId};
        ");
    }

    /**
     * @param string $categoriaId
     * @param string $categoriaNome
     * @param string $categoriaDescricao
     */
    public function atualizarCategoriaDescricao($categoriaId, $categoriaNome, $categoriaDescricao = "")
    {
        $categoriaId = (int)$categoriaId;
        $categoriaMetaTitle = $this->criarMetaString($categoriaNome);
        $categoriaMetaDescricao = $this->criarMetaString($categoriaDescricao);

        $this->query("
            UPDATE category_description SET
              `name` = '{$categoriaNome}',
              description = '{$categoriaDescricao}',
              meta_title = '{$categoriaMetaTitle}',
              meta_description = '{$categoriaMetaDescricao}'
            WHERE category_id = {$categoriaId}
        ");
    }

    public function removerCasoSemFilhos($categoriaId)
    {
        //@TODO
    }

    /**
     * Retorna uma Meta String baseada em uma String
     *
     * @param mixed $string
     * @return mixed
     */
    private function criarMetaString($string)
    {
        return str_replace(" ", "-", mb_strtolower($string, "UTF-8"));
    }


}
