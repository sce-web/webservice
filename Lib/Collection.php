<?php

class Collection
{
    /**
     * @var array
     */
    private $items;

    /**
     * @var integer
     */
    private $size;

    /**
     * @var integer
     */
    private $index;

    /**
     * Collection constructor.
     * @param array $items
     */
    public function __construct(array $items = array())
    {
        $this->items = $items;
        $this->size = count($items);
        $this->index = $this->size - 1;
    }

    /**
     * @param $element
     * @param integer|null $index
     */
    public function push($element, $index = null)
    {
        $index = $this->parseIndex($index);

        if (is_null($index)) {
            $this->items[++$this->index] = $element;
        } else {
            $this->items[$index] = $element;
            $this->index = $index;
        }

        ++$this->size;
    }

    /**
     * @param mixed $element
     * @param integer|null $index
     */
    public function insert($element, $index = null)
    {
        $this->push($element, $index);
    }

    public function pop($index = null)
    {
        $index = $this->parseIndex($index);

        if (is_null($index)) {
            $index = $this->index;
        }

        if (isset($items[$index])) {
            $poppedItem = $items[$index];
            unset($items[$index]);
            --$this->size;
            --$this->index;
        } else  {
            $poppedItem = null;
        }

        return $poppedItem;
    }

    public function get($index)
    {
        $index = $this->parseIndex($index);

        return $this->pop($index);
    }

    public function iterate($function)
    {
        if (is_callable($function)) {
            foreach ($this->items as $index => $item) {
                $function($item, $index);
            }
        }
    }

    /**
     * @param integer|string|null $index
     * @throws Exception if invalid index
     * @return integer|null $index
     */
    private function parseIndex($index)
    {
        if ($this->isInteger($index)) {
            $index = (int)$index;
        } else if (!is_null($index)) {
            throw new Exception("Collection index must be null (undefined) or an parseable integer");
        }

        return $index;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    private function isInteger($value)
    {
        return(ctype_digit(strval($value)));
    }
}
