<?php
require_once('Lib/webservice/nusoap.php');

$server = new soap_server;
$server->configureWSDL('server.CarregaListaEscolar', 'urn:server.CarregaListaEscolar');
$server->wsdl->schemaTargetNamespace = 'urn:server.CarregaListaEscolar';
$server->register('CarregaListaEscolar',
    array('token' => 'xsd:string', 'versao' => 'xsd:string', 'msg' => 'xsd:string', 'sincronizar' => 'xsd:string'),
    array('return' => 'xsd:string'),
    'urn:server.CarregaListaEscolar',
    'urn:server.CarregaListaEscolar#CarregaListaEscolar',
    'rpc',
    'encoded',
    'Retorna o resultado'
);

function CarregaListaEscolar($token, $versao, $msg, $sincronizar)
{
    require_once("AutoLoader.php");
    return Webservice::CarregaListaEscolar($token, $versao, $msg, $sincronizar);
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);

