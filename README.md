# Webservice 2.1.3 (OpenCart 2.*)
## Webservice atual em _produção_

Webservice atualmente em produção nos sites

### Dependencias
* Apenas compativel com o OpenCart 2.*
* Arquivo _\'config.php\'_ que fica na root do OpenCart

### Como instalar
Adicionar ele como dependencia de package do composer `sceweb/webservice`

e.g. composer.json:
```
{
    "name": "sceweb/artnit",
    "description": "ArtNit Shop",
    "authors": [
        {
            "name": "Bernardo Araujo",
            "email": "bernardo@scesistemas.com.br"
        }
    ],
    "config": {
        "vendor-dir": "./"
    },
    "require": {
        "sceweb/webservice": "2.1.*"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://hotwer@bitbucket.org/sce-web/webservice.git"
        }
    ]
}
```

Ou ele deve ser colocado no _\'root\'_ do site

## Changelog
* 2.1.3
    - Inserção de `custom fields` referentes a pessoa Jurídica, na Model de Pedido dinâmicamente
    - Destruir o Objeto de conexão, da classe Conection no método de fechar conexão
    - Teste se o cliente é PJ, e alimentação dos dados do pedido de acordo com o tipo do cliente
    - Criação de método SELECTFROM na classe de testes
* 2.1.2
    - Adicionado _fallback_ para a versão de `custom fields` do OpenCart em que os campos são guardados utilizando o `serialize` 
* 2.1.1
    - Corrigido problema em que o `json_decode` necessita de um parametro `true` como segundo argumento para ser retornado como um array associativo 
    - Corrigido problema em que certos campos do pedido e da listagem de pedidos não processados que não estavam sendo preenchidos corretamente
    - Corrigido problema em que o retorno da posição dos compos customizados estavam procurando o valor na variável errada.
* 2.1.0
    - Branch de desenvolvimento do webservice para poder trabalhar com os `custom fields` nativos do OpenCart criado (novo `master`, para o antigo seria `non-oc-fields`)
    - Preparação de mais testes de unidade para o Webservice