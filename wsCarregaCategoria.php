<?php
require_once('Lib/webservice/nusoap.php');

//$categoriaHelp = new Categoria();
//$categoriaHelp->conecta();

//Se não estiver em modo teste carrega o modo webservice

//if (!$produto->emTeste || true) {
$server = new soap_server;
$server->configureWSDL('server.CarregaCategoria', 'urn:server.CarregaCategoria');
$server->wsdl->schemaTargetNamespace = 'urn:server.CarregaCategoria';
$server->register(
    'CarregaCategoria',
    array(
        'token' => 'xsd:string',
        'versao' => 'xsd:string',
        'msg' => 'xsd:string',
        'sincronizar' => 'xsd:string'
    ),
    array('return' => 'xsd:string'),
    'urn:server.CarregaCategoria',
    'urn:server.CarregaCategoria#CarregaCategoria',
    'rpc',
    'encoded',
    'Retorna o resultado'
);
/*
} else {
CarregaCategoria(md5("7a4023605cdd42ea64d706cdb2120481"), 1, "201411223676007;Sacos de Confeitar;;;;;;N
201410305584691;Arquitetura;;;;201410305581119;1;S
", "S");
} */

function CarregaCategoria($token, $versao, $msg, $sincronizar)
{
    require_once("AutoLoader.php");
    return Webservice::CarregaCategoria($token, $versao, $msg, $sincronizar);
    /*
        $categoria = new Categoria();

        try {
            $categoria->authenticateToken($token);

            $categoria->setVersao($versao);
            $categoria->setMsg($msg);
            $categoria->setSincronizar($sincronizar);
            $categoria->carregar();

            $WsParam = $categoria->getWsParam();
            $WsParam->Status = $categoria->getStatus();
            $WsParam->Retorno = $categoria->getRetorno();
          $WsParam->Erro = $categoria->getErro();

            /*
            $return = "<?xml version='1.0' encoding='utf-8'?>
                    <WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
                      <Status>" . $categoria->getStatus() . "</Status>
                      <Retorno>" . $categoria->getRetorno() . "</Retorno>
                      <Erro>" . $categoria->getErro() . "</Erro>
                    </WsParam>";


        return $WsParam->asXML();
    } catch (Exception $exc) {
        $WsParam = $categoria->getWsParam();
        $WsParam->Status = "false";
        $WsParam->Erro = utf8_decode($exc->getMessage() . "\n file: " . $exc->getFile() . " \n line: " . $exc->getLine());

        return $WsParam->asXML();
    }
    */
}

/*
if ($_SERVER['SERVER_NAME'] != "localhost" || true) {
*/
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
/*
}
*/
