<?php
require_once('Lib/webservice/nusoap.php');

$server = new soap_server();
$server->configureWSDL('server.CarregaPedido', 'urn:server.CarregaPedido');
$server->wsdl->schemaTargetNamespace = 'urn:server.CarregaPedido';
$server->register(
    'CarregaPedido',
    array(
        'token' => 'xsd:string',
        'versao' => 'xsd:string',
        'msg' => 'xsd:string',
        'sincronizar' => 'xsd:string'
    ),
    array('return' => 'xsd:string'),
    'urn:server.CarregaPedido',
    'urn:server.CarregaPedido#CarregaPedido',
    'rpc',
    'encoded',
    'Retorna o resultado'
);

function CarregaPedido($token, $versao, $msg, $sincronizar = null)
{
    require_once("AutoLoader.php");
    return Webservice::CarregaPedido($token, $versao, $msg, $sincronizar = null);
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
