<?php

abstract class Connection {
    /**
     * @var mysqli
     */
    private static $conn;

    public static function open(){
        if (!is_null(self::$conn)) {
            return self::$conn;
        }

        self::$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

        if (self::$conn->connect_errno) {
            throw new Exception("Ocorrou um erro ao tentar-se conectar ao banco de dados: " . self::$conn->connect_error);
        }

        return self::$conn;
    }

    public static function close(){
        self::$conn->close();
        self::$conn = null;
    }
}

