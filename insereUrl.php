<?php
header('Content-Type: text/html; charset=UTF-8');
require('../config.php');
$link = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
if (!$link) {
    die('Não foi possível conectar: ' . mysql_error());
}
mysql_select_db(DB_DATABASE);

//Produtos
$sql = "SELECT product_description.product_id, product_description.name FROM product_description INNER JOIN product on product.product_id = product_description.product_id WHERE product.status = 1";
$result = mysql_query($sql);

while ($row = mysql_fetch_array($result)) {
    $string = utf8_encode ( $row['name']);
    $nome = NormalizaURL($string);
    $sql2 = "INSERT INTO `url_alias` (`query`, `keyword`) VALUES ('product_id=" . $row['product_id'] . "', '" . $nome . "');";
    mysql_query($sql2);
}

//Categorias
$sql = "SELECT category_description.category_id, category_description.name FROM category_description INNER JOIN category on category.category_id = category_description.category_id WHERE category.status =1";
$result = mysql_query($sql);

while ($row = mysql_fetch_array($result)) {
    
    $string = utf8_encode ( $row['name']);
    $nome = NormalizaURL($string);
    $sql2 = "INSERT INTO `url_alias` (`query`, `keyword`) VALUES ('category_id=" . $row['category_id'] . "', '" . $nome . "');";
    mysql_query($sql2);
}

function NormalizaURL($string) {
    $table = array(
        'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z',
        'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
        'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
        'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
        'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
        'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
        'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
        'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
        'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
        'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
        'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
        'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y',
        'Ŕ' => 'R', 'ŕ' => 'r',
    );
    // converte para minúsculo
    
    // Traduz os caracteres em $string, baseado no vetor $table
    $string = strtr($string, $table);
    $string = mb_strtolower($string);
    //$string = preg_replace("[´]", "a", $string);
    // remove caracteres indesejáveis (que não estão no padrão)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    // Remove múltiplas ocorrências de hífens ou espaços
    $string = preg_replace("/[\s-]+/", " ", $string);
    // Transforma espaços e underscores em hífens
    $string = preg_replace("/[\s_]/", "-", $string);
    //echo $string . "<br>";
    // retorna a string
    return $string;
}

?>