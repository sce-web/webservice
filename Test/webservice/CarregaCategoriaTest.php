<?php

/**
 * Class CarregaCategoriaTest
 * classe de teste do webservice CarregaCategoria
 */
class CarregaCategoriaTest extends PHPUnit_Framework_TestCase
{
    private static $token = '123456';
    private static $insertDumpFile = 'source/CategoriaDataInsert.txt';

    public function testCarregaCategoria()
    {
        $token = md5(self::$token);
        $message = $this->getFileContents(self::$insertDumpFile);

        $xmlString = Webservice::CarregaCategoria($token, '', $message, '');

        $xml = new DOMDocument();
        $xml->loadXML($xmlString);

        # region webservice response
        $this->assertEquals('', $xml->getElementsByTagName('Erro')->item(0)->nodeValue);
        $this->assertEquals('', $xml->getElementsByTagName('Retorno')->item(0)->nodeValue);
        $this->assertEquals('true', $xml->getElementsByTagName('Status')->item(0)->nodeValue);
        # endregion

        $categories = $this->selectFrom('category', 'category_id, parent_id, category_alternate_id', 'ORDER BY category_alternate_id');
        $this->assertEquals(5, count($categories));

        $categoryDescription = $this->selectFrom('category_description','name', "WHERE category_id = {$categories[0]['category_id']}");
        $this->assertEquals('Camisas', $categoryDescription[0]['name']);

        $categoryPaths = $this->selectFrom('category_path', 'path_id, level', "WHERE category_id = {$categories[0]['category_id']}");
        $this->assertEquals(1, count($categoryPaths));
        $this->assertEquals($categories[0]['category_id'], $categoryPaths[0]['path_id']);
        $this->assertEquals('0', $categoryPaths[0]['level']);

        $categoryToStore = $this->selectFrom('category_to_store', 'store_id', "WHERE category_id = {$categories[0]['category_id']}");
        $this->assertEquals(0, $categoryToStore[0]['store_id']);

        /**
         * 201509254464672;Camisas;;;;;2;S
         * 201509254469014;Polo;;;;;;N
         * 201509254471907;Regatas;;;;;;N
         * 20150925461163;Casacos;;;;;3;S
         * 201509254644618;Camisetas Manga Curta;;;;201509254464672;1;S
         * 201510096126533;Botas;;;;;1;S
         * 201510096133446;Camisa Polo;;;;;;N
         * 201511242602068;Outlet;;;;;4;S
         */

    }

    private function selectFrom($table, $fields = "*", $specify = ""){
        if (empty($table)) {
            throw new Exception('$table cannot be empty.');
        }

        return $this->connection->query("SELECT {$fields} FROM $table {$specify};")->fetchAll(PDO::FETCH_ASSOC);
    }

    private function getFileContents($filePath)
    {
        return file_get_contents($filePath);
    }

    public function __construct()
    {
        try {
            $this->connection = new PDO('mysql:host=' . DB_HOSTNAME . ';dbname=' . DB_DATABASE,
                DB_USERNAME, DB_PASSWORD
            );
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new ErrorException('Failed to connect to the database/start it > ' . $e->getMessage());
        }
    }

    public function __destruct()
    {
        $this->connection = null;
    }
}