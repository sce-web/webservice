<?php

/**
 * Class CarregaListaEscolarTest
 * classe de teste para o webservice CarregaListaEscolar
 */
class CarregaListaEscolarTest extends PHPUnit_Framework_TestCase
{
    private static $token = "123456";
    private static $insertDumpData = 'source/ListaEscolarDataInsert.txt';

    public function testCarregaListaEscolar()
    {
        $token = md5(self::$token);
        $message = $this->getFileContents(self::$insertDumpData);

        $xmlString = Webservice::CarregaListaEscolar($token, '', $message, '');

        $xml = new DOMDocument();
        $xml->loadXML($xmlString);

        # region webservice response
        $this->assertEquals('', $xml->getElementsByTagName("Erro")->item(0)->nodeValue);
        $this->assertEquals('', $xml->getElementsByTagName("Retorno")->item(0)->nodeValue);
        $this->assertEquals('true', $xml->getElementsByTagName("Status")->item(0)->nodeValue);
        # endregion

        # region list
        $categoryListDescription = $this->selectFrom('category_description', "category_id", "WHERE name = 'LISTA ESCOLAR'");
        $this->assertEquals(1, count($categoryListDescription));

        $categoryListId = $categoryListDescription[0]['category_id'];

        $categoryList = $this->selectFrom('category', 'parent_id', "WHERE category_id = {$categoryListDescription[0]['category_id']}");
        $this->assertEquals(0, (int)$categoryList[0]['parent_id']);
        # endregion

        # region college
        $categoryCollege = $this->selectFrom('category', 'category_id, parent_id', "WHERE category_alternate_id = '1201212263671459'");
        $this->assertEquals(1, count($categoryCollege));
        $this->assertEquals($categoryListId, $categoryCollege[0]['parent_id']);

        $categoryCollegeId = $categoryCollege[0]['category_id'];

        $categoryCollegeDescription = $this->selectFrom('category_description', 'name', "WHERE category_id = {$categoryCollegeId}");
        $this->assertEquals(' - ALECRIM', $categoryCollegeDescription[0]['name']);

        $categoryCollegeToStore = $this->selectFrom('category_to_store',"store_id", "WHERE category_id = {$categoryCollegeId}");
        $this->assertEquals(0, (int)$categoryCollegeToStore[0]['store_id']);

        $categoryCollegePath = $this->selectFrom('category_path', "path_id, level", "WHERE category_id = {$categoryCollegeId} ORDER BY level");
        $this->assertEquals(2, count($categoryCollegePath));
        $this->assertEquals($categoryListId, $categoryCollegePath[0]['path_id']);
        $this->assertEquals('0', $categoryCollegePath[0]['level']);
        $this->assertEquals($categoryCollegeId, $categoryCollegePath[1]['path_id']);
        $this->assertEquals('1', $categoryCollegePath[1]['level']);
        # endregion

        # region grade
        $categoryGrade = $this->selectFrom('category',"category_id, parent_id", "WHERE category_alternate_id = '1201211124143628'");
        $this->assertEquals(1, count($categoryGrade));
        $this->assertEquals($categoryCollegeId, $categoryGrade[0]['parent_id']);

        $categoryGradeId = $categoryGrade[0]['category_id'];

        $categoryGradeDescription = $this->selectFrom('category_description', 'name', "WHERE category_id = {$categoryGradeId}");
        $this->assertEquals('ALECRIM - 1 ANO EF - 2015', $categoryGradeDescription[0]['name']);

        $categoryGradeToStore = $this->selectFrom('category_to_store', "store_id", "WHERE category_id = {$categoryGradeId}");
        $this->assertEquals(0, (int)$categoryGradeToStore[0]['store_id']);

        $categoryGradePath = $this->selectFrom('category_path',"path_id, level", "WHERE category_id = {$categoryGradeId} ORDER BY level");
        $this->assertEquals(3, count($categoryGradePath));
        $this->assertEquals($categoryListId, $categoryGradePath[0]['path_id']);
        $this->assertEquals('0', $categoryGradePath[0]['level']);
        $this->assertEquals($categoryCollegeId, $categoryGradePath[1]['path_id']);
        $this->assertEquals('1', $categoryGradePath[1]['level']);
        $this->assertEquals($categoryGradeId, $categoryGradePath[2]['path_id']);
        $this->assertEquals('2', $categoryGradePath[2]['level']);
        # endregion

        # region items
        $itensRelationed = $this->selectFrom('product_to_category', 'product_id', "WHERE category_id = {$categoryGradeId}");
        $this->assertEquals(30, count($itensRelationed));

        $itens = $this->selectFrom(
            'product_school_list JOIN product USING (product_id)',
            'product.sku, product.model, product_school_list.quantity_list',
            "WHERE product_school_list.category_id = {$categoryGradeId} ORDER BY product_id"
        );



        $this->assertEquals('50411', $itens[0]['sku']);
        $this->assertEquals('050411', $itens[0]['model']);
        $this->assertEquals(1, $itens[0]['quantity_list']);

        $this->assertEquals('46769', $itens[1]['sku']);
        $this->assertEquals('046769', $itens[1]['model']);
        $this->assertEquals(1, $itens[1]['quantity_list']);

        $this->assertEquals('57236', $itens[2]['sku']);
        $this->assertEquals('057236', $itens[2]['model']);
        $this->assertEquals(1, $itens[2]['quantity_list']);

        $this->assertEquals('45344', $itens[3]['sku']);
        $this->assertEquals('9788576751045', $itens[3]['model']);
        $this->assertEquals(1, $itens[3]['quantity_list']);

        $this->assertEquals('75340', $itens[4]['sku']);
        $this->assertEquals('075340', $itens[4]['model']);
        $this->assertEquals(1, $itens[4]['quantity_list']);

        $this->assertEquals('45280', $itens[5]['sku']);
        $this->assertEquals('9788502073999', $itens[5]['model']);
        $this->assertEquals(1, $itens[5]['quantity_list']);

        $this->assertEquals('37967', $itens[6]['sku']);
        $this->assertEquals('7897664254657', $itens[6]['model']);
        $this->assertEquals(1, $itens[6]['quantity_list']);

        $this->assertEquals('73326', $itens[7]['sku']);
        $this->assertEquals('073326', $itens[7]['model']);
        $this->assertEquals(3, $itens[7]['quantity_list']);

        $this->assertEquals('65168', $itens[8]['sku']);
        $this->assertEquals('065168', $itens[8]['model']);
        $this->assertEquals(1, $itens[8]['quantity_list']);

        $this->assertEquals('74266', $itens[9]['sku']);
        $this->assertEquals('074266', $itens[9]['model']);
        $this->assertEquals(5, $itens[9]['quantity_list']);

        $this->assertEquals('68642', $itens[10]['sku']);
        $this->assertEquals('068642', $itens[10]['model']);
        $this->assertEquals(1, $itens[10]['quantity_list']);

        $this->assertEquals('49802', $itens[11]['sku']);
        $this->assertEquals('049802', $itens[11]['model']);
        $this->assertEquals(2, $itens[11]['quantity_list']);

        $this->assertEquals('71373', $itens[12]['sku']);
        $this->assertEquals('071373', $itens[12]['model']);
        $this->assertEquals(2, $itens[12]['quantity_list']);

        $this->assertEquals('71017', $itens[13]['sku']);
        $this->assertEquals('071017', $itens[13]['sku']);
        $this->assertEquals(1, $itens[13]['quantity_list']);

        $this->assertEquals('62124', $itens[14]['sku']);
        $this->assertEquals('062124', $itens[14]['model']);
        $this->assertEquals(1, $itens[14]['quantity_list']);

        $this->assertEquals('64405', $itens[15]['sku']);
        $this->assertEquals('064405', $itens[15]['model']);
        $this->assertEquals(1, $itens[15]['quantity_list']);

        $this->assertEquals('68645', $itens[16]['sku']);
        $this->assertEquals('068645', $itens[16]['model']);
        $this->assertEquals(8, $itens[16]['quantity_list']);

        $this->assertEquals('38674', $itens[17]['sku']);
        $this->assertEquals('7898017035466', $itens[17]['model']);
        $this->assertEquals(1, $itens[17]['quantity_list']);

        $this->assertEquals('56403', $itens[18]['sku']);
        $this->assertEquals('056403', $itens[18]['model']);
        $this->assertEquals(1, $itens[18]['quantity_list']);

        $this->assertEquals('69772', $itens[19]['sku']);
        $this->assertEquals('069772', $itens[19]['model']);
        $this->assertEquals(1, $itens[19]['quantity_list']);

        $this->assertEquals('8138', $itens[20]['sku']);
        $this->assertEquals('8765', $itens[20]['model']);
        $this->assertEquals(2, $itens[20]['quantity_list']);

        $this->assertEquals('41126', $itens[21]['sku']);
        $this->assertEquals('7898441790771', $itens[21]['model']);
        $this->assertEquals(2, $itens[21]['quantity_list']);

        $this->assertEquals('10885', $itens[22]['sku']);
        $this->assertEquals('11567', $itens[22]['model']);
        $this->assertEquals(2, $itens[22]['quantity_list']);

        $this->assertEquals('61966', $itens[23]['sku']);
        $this->assertEquals('061966', $itens[23]['model']);
        $this->assertEquals(1, $itens[23]['quantity_list']);

        $this->assertEquals('42114', $itens[24]['sku']);
        $this->assertEquals('7898924193631', $itens[24]['model']);
        $this->assertEquals(1, $itens[24]['quantity_list']);

        $this->assertEquals('61187', $itens[25]['sku']);
        $this->assertEquals('061187', $itens[25]['model']);
        $this->assertEquals(1, $itens[25]['quantity_list']);

        $this->assertEquals('36961', $itens[26]['sku']);
        $this->assertEquals('7897249567349', $itens[26]['model']);
        $this->assertEquals(1, $itens[26]['quantity_list']);

        $this->assertEquals('67660', $itens[27]['sku']);
        $this->assertEquals('067660', $itens[27]['model']);
        $this->assertEquals(1, $itens[27]['quantity_list']);

        $this->assertEquals('68666', $itens[28]['sku']);
        $this->assertEquals('068666', $itens[28]['model']);
        $this->assertEquals(1, $itens[28]['quantity_list']);

        $this->assertEquals('64496', $itens[29]['sku']);
        $this->assertEquals('064496', $itens[29]['model']);
        $this->assertEquals(1, (int)$itens[29]['quantity_list']);
        # endregion

    }

    /**
     * @param string Define what tables you should select
     * @param string    //  what fields of the table want
     * @param string    //  what are the parameters of the select
     * @return array Return an array
     * @throws Exception The parameter table, can't be empty
     */
    private function selectFrom($table, $fields = "*", $specify = "")
    {
        if (empty($table)) {
            throw new Exception('$table cannot be empty.');
        }

        return $this->connection->query("SELECT {$fields} FROM $table {$specify};")->fetchAll(PDO::FETCH_ASSOC);
    }
/*
    private function selectFromCategory($fields = "*", $specify = "")
    {
        return $this->connection->query("SELECT {$fields} FROM category {$specify};")->fetchAll(PDO::FETCH_ASSOC);
    }

    private function selectFromCategoryDescription($fields = "*", $specify = "")
    {
        return $this->connection->query("SELECT {$fields} FROM category_description {$specify};")->fetchAll(PDO::FETCH_ASSOC);
    }

    private function selectFromCategoryToStore($fields = "*", $specify = "")
    {
        return $this->connection->query("SELECT {$fields} FROM category_to_store {$specify};")->fetchAll(PDO::FETCH_ASSOC);
    }

    private function selectFromCategoryPath($fields = "*", $specify = "")
    {
        return $this->connection->query("SELECT {$fields} FROM category_path {$specify};")->fetchAll(PDO::FETCH_ASSOC);
    }

    private function selectFromProductToCategory($fields = "*", $specify = "")
    {
        return $this->connection->query("SELECT {$fields} FROM product_to_category {$specify};")->fetchAll(PDO::FETCH_ASSOC);
    }

    private function selectFromProductSchoolListJoinedProduct($fields = "*", $specify = "")
    {
        return $this->connection->query("SELECT {$fields} FROM product_school_list JOIN product USING (product_id) {$specify};")->fetchAll(PDO::FETCH_ASSOC);
    }*/

    private function getFileContents($filePath)
    {
        return file_get_contents($filePath);
    }

    public function __construct()
    {
        try {
            $this->connection = new PDO('mysql:host=' . DB_HOSTNAME . ';dbname=' . DB_DATABASE,
                DB_USERNAME, DB_PASSWORD
            );
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new ErrorException('Failed to connect to the database/start it > ' . $e->getMessage());
        }
    }

    public function __destruct()
    {
        $this->connection = null;
    }

}