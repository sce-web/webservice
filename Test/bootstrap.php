<?php
require_once("PhpUnitAutoLoader.php");
require_once("../Model/library.php");

PhpUnitAutoLoader::registerDirectory("../Model");

$dependecies = Spyc::YAMLLoad("source/Dependencies.yml");

$connection = new PDO('mysql:host=' . DB_HOSTNAME . ';dbname=' .  DB_DATABASE,
    DB_USERNAME, DB_PASSWORD);
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$truncates = array(
    'category',
    'category_description',
    'category_path',
    'category_to_store',
    'product_to_category',
    'product_school_list',
);

foreach ($truncates as $table) {
    $connection->query("TRUNCATE TABLE $table;");
}

foreach($dependecies as $table => $dataSet)
{
    $table = "`$table`";
    $connection->query("TRUNCATE TABLE $table;");
    $insertQuery = "INSERT INTO {$table} (`".implode(array_keys(reset($dataSet)), '`,`')."`) VALUES ";
    foreach ($dataSet as $data)
        $insertQuery .= "('".implode($data, "','")."'),";
    $insertQuery = rtrim($insertQuery, ',');
    $connection->query($insertQuery);
}

$connection = null;

