<?php

/**
 * Class PhpUnitAutoLoader
 */
class PhpUnitAutoLoader
{
    static private $classNames = array();

    public static function registerDirectory($dirName)
    {
        $di = new DirectoryIterator($dirName);
        foreach ($di as $file) {
            if ($file->isDir() && !$file->isLink() && !$file->isDot()) {
                self::registerDirectory($file->getPathname());
            } elseif (substr($file->getFilename(), -4) === '.php') {
                $className = substr($file->getFilename(), 0, -4);
                PhpUnitAutoLoader::registerClass($className, $file->getPathname());
            }
        }
    }

    public static function registerClass($className, $fileName)
    {
        PhpUnitAutoLoader::$classNames[$className] = $fileName;
    }

    public static function loadClass($className)
    {
        if (isset(PhpUnitAutoLoader::$classNames[$className])) {
            require_once(PhpUnitAutoLoader::$classNames[$className]);
        }
    }
}

spl_autoload_register(array("PhpUnitAutoLoader", "loadClass"));

require_once("../LocalConfig.php");
require_once("../Model/library.php");

define("WEBSERVICE_ROOT", "C:\\Users\\bernardo.SCE\\work\\webservice\\");

Logging::lfile(WEBSERVICE_ROOT . "error.log");

PhpUnitAutoLoader::registerDirectory("../Model/categoria");
PhpUnitAutoLoader::registerDirectory("../Model/pedido");
PhpUnitAutoLoader::registerDirectory("../Model/produto");
PhpUnitAutoLoader::registerDirectory("../Model");
PhpUnitAutoLoader::registerDirectory("../Connection");
PhpUnitAutoLoader::registerDirectory("../Lib");
