<?php
require_once('Lib/webservice/nusoap.php');

//$pedidoModel = new pedidoModel();
//$pedidoModel->conecta();
//$pedidoModel->emTeste($pedidoModel->nomeWS);

//Se estiver em modo teste não carrega o módulo de webservice
//if (!$pedidoModel->emTeste || true) {
$server = new soap_server();
$server->configureWSDL('server.CapturaPedido', 'urn:server.CapturaPedido');
$server->wsdl->schemaTargetNamespace = 'urn:server.CapturaPedido';
$server->register(
    'CapturaPedido',
    array('nropedido' => 'xsd:string'),
    array('return' => 'xsd:string'),
    'urn:server.CapturaPedido',
    'urn:server.CapturaPedido#CapturaPedido',
    'rpc',
    'encoded',
    'Captura os pedidos do site'
);
//} else {
//    $pedidoModel->nroPedido = 1812; //Pedido a ser testado
//    CapturaPedido($pedidoModel->nroPedido);
//}

function CapturaPedido($nropedido) {
    require_once("AutoLoader.php");
    return Webservice::CapturaPedido($nropedido);
    /*

    $pedidoModel = new pedidoModel();

    try {
        $pedidoModel->nroPedido = $nropedido;
        $conteudo = "";
        $conteudo = $pedidoModel->capturaPedido();

        if (!$pedidoModel->emTeste) {
            return
                    "<?xml version='1.0' encoding='utf-16'?>
                <WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
                    <Status>true</Status>
                    <Retorno>" . $conteudo . "</Retorno>
                    <Erro></Erro>
                </WsParam>";
        }
    } catch (Exception $e) {
        //if (!$pedidoModel->emTeste) {
            return
                    "<?xml version='1.0' encoding='utf-16'?>
                <WsParam xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
                    <Status>false</Status>
                    <Retorno>" . $conteudo . "</Retorno>
                    <Erro>" . $e->getMessage() . "</Erro>
                </WsParam>";
        //}
    }
    */
}

//if (!$pedidoModel->emTeste) {
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
//}

